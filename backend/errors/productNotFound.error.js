const productNotFound = async (res) => {
  return res.status(404).json({
    message: "Product Not Found!",
  });
};

module.exports = productNotFound;
