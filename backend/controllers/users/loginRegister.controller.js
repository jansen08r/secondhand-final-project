require("dotenv").config();

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const db = require("../../models");

const USERS_MODEL = db.tbl_users;

// Password Hash Salt
const salt = "ZpgSgcxATbQlKfe";

// Register User
async function registerUser(req, res) {
  try {
    const { username, email, password } = req.body;

    // Find User
    const user = await USERS_MODEL.findOne({ where: { email: email } });
    if (user) return res.status(422).json({ message: "User already exist!" });

    // encrypt password
    let hashPassword = bcrypt.hashSync(password + salt, 12);

    const dataReq = {
      username: username,
      email: email,
      password: hashPassword,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    await USERS_MODEL.create(dataReq).then((data) => {
      return res.status(201).json({
        message: "Register Success!",
        data: data,
      });
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Login User
async function loginUser(req, res) {
  try {
    const { email, password } = req.body;

    if (email !== "" || password !== "") {
      // Find User
      const user = await USERS_MODEL.findOne({
        where: { email: email },
      });
      if (!user) return invalid(res);

      // Match Password
      const matchPass = await bcrypt.compare(password + salt, user.password);
      if (!matchPass) return invalid(res);

      // Get Token
      const accessToken = authorize(user.id, user.username);

      return res.status(200).json({
        message: "Login Success!",
        token: accessToken,
      });
    }
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

function authorize(id, username) {
  const secretKey = process.env.JWT_SECRET_KEY;
  const expireKey = "2h";

  const tokenAccess = jwt.sign(
    {
      id: id,
      username: username,
    },
    secretKey,
    {
      algorithm: "HS256",
      expiresIn: expireKey,
    }
  );

  return tokenAccess;
}

function invalid(res) {
  res.status(422).json({ message: "Email/Password Invalid!" });
}

module.exports = {
  registerUser,
  loginUser,
};
