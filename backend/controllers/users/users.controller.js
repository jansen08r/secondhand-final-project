const db = require("../../models");
const { getTokenId } = require("../../middlewares/jwtAuth");
const cloudinaryUploadImage = require("../../utils/cloudinaryUploadImage");

const USERS_MODEL = db.tbl_users;

// Get User Profile using Token
async function getUserProfile(req, res) {
  try {
    const userId = getTokenId(req);

    const userData = await USERS_MODEL.findOne({
      where: { id: userId },
      attributes: { exclude: ["password"] },
    });

    res.status(200).json({
      message: "Get User Profile Success!",
      data: userData,
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

// Get User Profile by Id
async function getUserProfileById(req, res) {
  try {
    const userData = await USERS_MODEL.findOne({
      where: { id: req.params.id },
      attributes: { exclude: ["password", "email", "address"] },
    });

    res.status(200).json({
      message: "Get User Profile Success!",
      data: userData,
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

// Update user profile
async function updateUserProfile(req, res) {
  try {
    const { username, address, phoneNumber, provinceId, cityId } = req.body;
    const userId = getTokenId(req);

    const imageURL = await cloudinaryUploadImage(req, "fsw2-kel2/profile");

    const dataReq = {
      username: username,
      profileImage_url: imageURL,
      address: address,
      phoneNumber: phoneNumber,
      provinceId: provinceId,
      cityId: cityId,
      updatedAt: new Date(),
    };

    await USERS_MODEL.update(dataReq, { where: { id: userId } }).then(() => {
      res.status(201).json({
        message: "Update Profile Success!",
        data: dataReq,
      });
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

const getProfileById = async (id) => {
  const profile = await USERS_MODEL.findOne({
    where: { id: id },
  });

  return profile;
};

module.exports = {
  getUserProfile,
  getUserProfileById,
  updateUserProfile,
  getProfileById,
};
