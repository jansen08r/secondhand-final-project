const db = require("../../models");
const CATEGORIES_MODEL = db.tbl_categories;

async function getAllCategories(req, res) {
  await CATEGORIES_MODEL.findAll().then((data) => {
    res.status(200).json({ message: "Success!", data: data });
  });
}

async function getCategoryById(req, res) {
  await CATEGORIES_MODEL.findOne({
    where: { id: req.params.id },
  }).then((data) => {
    res.status(200).json({
      message: "Success!",
      data: data,
    });
  });
}

module.exports = {
  getAllCategories,
  getCategoryById,
};
