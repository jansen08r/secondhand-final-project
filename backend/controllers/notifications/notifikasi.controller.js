const db = require("../../models");
const { Op } = require("sequelize");
const { getTokenId } = require("../../middlewares/jwtAuth");
const { io } = require("../../bin/socket");
const NOTIFIKASI_MODEL = db.tbl_notification;

// Add New Notification
const addNotification = async (data) => {
  await NOTIFIKASI_MODEL.create(data);

  io.emit("notification", data);
};

const buyerUpdateNotification = async (req, data, id_transaksi, id_seller) => {
  const userId = getTokenId(req);

  await NOTIFIKASI_MODEL.update(data, {
    where: {
      id_transaksi: id_transaksi,
      id_buyer: userId,
      id_seller: id_seller,
    },
  }).then((data) => {
    io.emit("notification", data);
  });
};

const sellerUpdateNotification = async (req, data, id_transaksi, id_buyer) => {
  const userId = getTokenId(req);

  await NOTIFIKASI_MODEL.update(data, {
    where: {
      id_transaksi: id_transaksi,
      id_buyer: id_buyer,
      id_seller: userId,
    },
  }).then((data) => {
    io.emit("notification", data);
  });
};

// Get User Notification
async function getUserNotification(req, res) {
  try {
    const userId = getTokenId(req);

    await NOTIFIKASI_MODEL.findAll({
      where: {
        [Op.or]: [{ id_seller: userId }, { id_buyer: userId }],
      },
    }).then((data) => {
      res.status(200).json({
        message: "Success!",
        data: data,
      });
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

module.exports = {
  addNotification,
  buyerUpdateNotification,
  sellerUpdateNotification,
  getUserNotification,
};
