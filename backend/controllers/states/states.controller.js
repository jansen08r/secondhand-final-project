const axios = require("axios");

async function getAllProvince(req, res) {
  await axios
    .get("http://dev.farizdotid.com/api/daerahindonesia/provinsi")
    .then((data) => {
      res.status(200).json({
        message: "Success!",
        data: data.data.provinsi,
      });
    });
}

async function getProvince(req, res) {
  const provinceId = req.params.id;

  await axios
    .get(`http://dev.farizdotid.com/api/daerahindonesia/provinsi/${provinceId}`)
    .then((data) => {
      res.status(200).json({
        message: "Success!",
        data: data.data,
      });
    });
}

async function getCities(req, res) {
  const idProvinsi = req.params.id;

  await axios
    .get(
      `https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${idProvinsi}`
    )
    .then((data) => {
      res.status(200).json({
        message: "Success!",
        data: data.data,
      });
    });
}

async function getCity(req, res) {
  const idCity = req.params.id;

  await axios
    .get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${idCity}`)
    .then((data) => {
      res.status(200).json({
        message: "Success!",
        data: data.data,
      });
    });
}

module.exports = {
  getAllProvince,
  getProvince,
  getCities,
  getCity,
};
