const productNotFound = require("../../errors/productNotFound.error");
const { getTokenId } = require("../../middlewares/jwtAuth");
const db = require("../../models");
const cloudinaryUpload = require("../../utils/cloudinaryUploadImage");
const { sendEmailNotification } = require("../../utils/nodemailer");
const { addNotification } = require("../notifications/notifikasi.controller");
const { getProfileById } = require("../users/users.controller");
const PRODUCTS_MODEL = db.tbl_products;
const PRODUCTSIMAGE_MODEL = db.tbl_productsImage;

async function getAllProducts(req, res) {
  try {
    await PRODUCTS_MODEL.findAll().then((data) =>
      res.status(200).json({
        message: "Success!",
        data: data,
      })
    );
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

async function createProduct(req, res) {
  try {
    const { name, price, description, id_category } = req.body;
    let productImageURL;

    // Get user id from token
    const userId = getTokenId(req);

    // Check user profile
    const profile = await getProfileById(userId);
    if (
      !profile.profileImage_url ||
      !profile.address ||
      !profile.phoneNumber ||
      !profile.provinceId ||
      !profile.cityId
    ) {
      res.status(401).json({
        message: "Please complete your profile!",
      });

      return;
    }

    // Check product images amount
    const productsAmount = await PRODUCTS_MODEL.count({
      where: { id_owner: userId },
    });

    if (productsAmount >= 4) {
      res.status(500).json({
        message: "Products sold already reach maximum of 4 products!",
      });

      return;
    }

    // Cloudinary Upload Image
    productImageURL = await cloudinaryUpload(req, "fsw2-kel2/products");

    const dataReqProduct = {
      name: name,
      price: price,
      description: description,
      isSold: false,
      id_category: id_category,
      id_owner: userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    const findProduct = await findOneProductByDatas(
      name,
      price,
      description,
      id_category,
      userId
    );

    if (findProduct)
      return res.status(422).json({ message: "Product Already Exist!" });

    await PRODUCTS_MODEL.create(dataReqProduct);

    let dataReqProductImage;

    const product = await findOneProductByDatas(
      name,
      price,
      description,
      id_category,
      userId
    );

    // Insert image to product image table
    if (productImageURL !== null) {
      const productId = product.id;

      dataReqProductImage = {
        imageURL: productImageURL,
        id_products: productId,
        id_owner: userId,
        createdAt: new Date(),
        updatedAt: new Date(),
      };

      await PRODUCTSIMAGE_MODEL.create(dataReqProductImage);
    }

    // Send Notification
    await addNotification({
      id_seller: userId,
      id_product: product.id,
      status: "Produk berhasil ditambahkan!",
    });

    // Send Email
    const to = profile.email;
    const subject = "Berhasil menambahkan produk!";
    const text = `Produk dengan nama ${product.name} berhasil ditambahkan ke daftar jual!`;
    sendEmailNotification(to, subject, text);

    res.status(201).json({
      message: "Success Create Product!",
      data: dataReqProduct,
      productImage: dataReqProductImage,
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

async function updateProduct(req, res) {
  try {
    const { name, price, description, id_category } = req.body;

    const findProduct = await findOneProductById(req.params.id);

    if (findProduct === null) {
      productNotFound(res);

      return;
    }

    const dataReq = {
      name: name,
      price: price,
      description: description,
      id_category: id_category,
      updatedAt: new Date(),
    };

    await PRODUCTS_MODEL.update(dataReq, {
      where: {
        id: req.params.id,
      },
    }).then(() => {
      res.status(201).json({
        message: "Success!",
        data: dataReq,
      });
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

async function deleteProduct(req, res) {
  try {
    // Find Product
    const product = await findOneProductById(req.params.id);

    if (product === null) {
      productNotFound(res);

      return;
    }

    await PRODUCTSIMAGE_MODEL.destroy({
      where: {
        id_products: req.params.id,
      },
    });

    await PRODUCTS_MODEL.destroy({
      where: { id: req.params.id },
    });

    const userId = getTokenId(req);

    await addNotification({
      id_seller: userId,
      id_product: product.id,
      status: "Produk berhasil dihapus!",
    });

    // Send Email
    const profile = await getProfileById(userId);

    const to = profile.email;
    const subject = "Berhasil menghapus produk!";
    const text = `Produk dengan nama ${product.name} berhasil dihapus!`;
    sendEmailNotification(to, subject, text);

    res.status(200).json({
      message: "Product deleted successfully!",
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

async function getProductDetail(req, res) {
  try {
    // Find Product
    const findProduct = await findOneProductById(req.params.id);

    if (findProduct === null) {
      productNotFound(res);

      return;
    }

    const product = await PRODUCTS_MODEL.findAll({
      where: {
        id: req.params.id,
      },
    });

    await PRODUCTSIMAGE_MODEL.findAll({
      where: {
        id_products: req.params.id,
      },
    }).then((images) => {
      res.status(200).json({
        message: "Success!",
        data: product,
        images: images,
      });
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

async function getProductSold(req, res) {
  try {
    // Get user id from token
    const userId = getTokenId(req);

    await PRODUCTS_MODEL.findAll({
      where: {
        isSold: true,
        id_owner: userId,
      },
    }).then((data) =>
      res.status(200).json({
        message: "Success!",
        data: data,
      })
    );
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

const findOneProductByDatas = async (
  name,
  price,
  description,
  id_category,
  userId
) => {
  const product = await PRODUCTS_MODEL.findOne({
    where: {
      name: name,
      price: price,
      description: description,
      id_category: id_category,
      id_owner: userId,
    },
  });

  return product;
};

const findOneProductById = async (id) => {
  const Product = await PRODUCTS_MODEL.findOne({
    where: { id: id },
  });

  return Product;
};

module.exports = {
  getAllProducts,
  getProductSold,
  createProduct,
  updateProduct,
  deleteProduct,
  getProductDetail,
  findOneProductById,
};
