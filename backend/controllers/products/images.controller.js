const { getTokenId } = require("../../middlewares/jwtAuth");
const { findOneProductById } = require("./products.controller");
const cloudinaryUpload = require("../../utils/cloudinaryUploadImage");

const db = require("../../models");
const productNotFound = require("../../errors/productNotFound.error");
const PRODUCTSIMAGE_MODEL = db.tbl_productsImage;

// Upload an image
async function uploadProductImage(req, res) {
  try {
    // Find Product
    const findProduct = findOneProductById(req.params.id);

    if (findProduct === null) {
      productNotFound(res);

      return;
    }

    // Get user id from token
    const userId = getTokenId(req);

    // Check product images amount
    const imagesAmount = await PRODUCTSIMAGE_MODEL.count({
      where: { id_products: req.params.id },
    });

    if (imagesAmount >= 4) {
      res.status(500).json({
        message: "Product images already reach maximum of 4 pictures!",
      });

      return;
    }

    // Upload Image to Cloudinary
    const productImageURL = await cloudinaryUpload(req, "fsw2-kel2/products");

    const dataReq = {
      imageURL: productImageURL,
      id_products: req.params.id,
      id_owner: userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    await PRODUCTSIMAGE_MODEL.create(dataReq).then((data) => {
      res.status(201).json({
        message: "Success Upload Product!",
        data: data,
      });
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Delete an image
async function deleteProductImage(req, res) {
  try {
    // Find Product
    const findProduct = await PRODUCTSIMAGE_MODEL.findOne({
      where: { id: req.params.id },
    });

    if (findProduct === null) {
      res.status(500).json({
        message: "Image Not Found!",
      });

      return;
    }

    await PRODUCTSIMAGE_MODEL.destroy({
      where: { id: req.params.id },
    }).then(() => {
      res.status(200).json({
        message: "Delete Success!",
      });
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Get all images from database
async function getAllImages(req, res) {
  try {
    await PRODUCTSIMAGE_MODEL.findAll().then((data) => {
      res.status(200).json({
        message: "Success!",
        images: data,
      });
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

// Get all images of a product
async function getProductImages(req, res) {
  try {
    await PRODUCTSIMAGE_MODEL.findAll({
      where: { id_products: req.params.id },
    }).then((data) => {
      res.status(200).json({
        message: "Success!",
        productImages: data,
      });
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

module.exports = {
  uploadProductImage,
  deleteProductImage,
  getAllImages,
  getProductImages,
};
