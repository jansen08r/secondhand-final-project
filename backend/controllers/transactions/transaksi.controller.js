const db = require("../../models");
const { Op } = require("sequelize");
const { getTokenId } = require("../../middlewares/jwtAuth");
const { findOneProductById } = require("../products/products.controller");
const {
  addNotification,
  buyerUpdateNotification,
  sellerUpdateNotification,
} = require("../notifications/notifikasi.controller");
const { getProfileById } = require("../users/users.controller");
const productNotFound = require("../../errors/productNotFound.error");
const { sendEmailNotification } = require("../../utils/nodemailer");
const TRANSAKSI_MODEL = db.tbl_transactions;
const PRODUCTS_MODEL = db.tbl_products;

// Get User Transaction
async function getUserTransaksi(req, res) {
  try {
    const userId = getTokenId(req);

    await TRANSAKSI_MODEL.findAll({
      where: {
        [Op.or]: [{ id_seller: userId }, { id_buyer: userId }],
      },
    }).then((data) =>
      res.status(200).json({
        message: "Success!",
        data: data,
      })
    );
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

// Get a transaction detail
async function getTransaksiDetail(req, res) {
  try {
    const transaksi = await TRANSAKSI_MODEL.findOne({
      where: {
        id: req.params.id,
      },
    });

    if (!transaksi) {
      res.status(404).json({
        message: "Transaction Not Found!",
      });

      return;
    }

    const product = await findOneProductById(transaksi.id_product);

    res.status(200).json({
      message: "Success!",
      transaksi: transaksi,
      product: product,
    });
  } catch (error) {
    res.status(404).json({
      message: error.message,
    });
  }
}

// Buyer add transaksi
async function addTransaksi(req, res) {
  try {
    let dataTransaksi;

    // Find if product exist
    const product = await findOneProductById(req.params.id);

    if (product === null) {
      productNotFound(res);

      return;
    }

    // Check user id
    const userId = getTokenId(req);

    const profile = await getProfileById(userId);
    if (
      !profile.profileImage_url ||
      !profile.address ||
      !profile.phoneNumber ||
      !profile.provinceId ||
      !profile.cityId
    ) {
      res.status(401).json({
        message: "Please complete your profile!",
      });

      return;
    }

    if (product.id_owner === userId) {
      res.status(401).json({
        message: "Cannot buy your own item!",
      });

      return;
    }

    const dataReq = {
      id_product: req.params.id,
      id_seller: product.id_owner,
      id_buyer: userId,
      bargainPrice: req.body.bargainPrice,
      sellerResponse: false,
      isCompleted: false,
      createdAt: new Date(),
      updatedAt: new Date(),
    };

    // Add Transaksi
    await TRANSAKSI_MODEL.create(dataReq).then((data) => {
      dataTransaksi = data;
    });

    // Send to Notifications
    const notif = await addNotification({
      id_transaksi: dataTransaksi.id,
      id_seller: dataTransaksi.id_seller,
      id_buyer: dataTransaksi.id_buyer,
      id_product: product.id,
      bargainPrice: dataTransaksi.bargainPrice,
      status: "Penawaran produk",
    });

    // Send Email
    const to = profile.email;
    const subject = "Berhasil menawar produk";
    const text = `Produk dengan nama ${product.name} berhasil ditawar! Silahkan menunggu jawaban dari penjual!`;
    sendEmailNotification(to, subject, text);

    res.status(200).json({
      messages: "Success!",
      dataTransaksi: dataTransaksi,
      product: product,
      notification: notif,
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Seller Update Response Transaksi ditolak
async function sellerRejectTransaksi(req, res) {
  try {
    const dataReq = {
      sellerResponse: true,
      updatedAt: new Date(),
    };

    // Update bargain price
    await TRANSAKSI_MODEL.update(dataReq, {
      where: { id: req.params.id },
    });

    const transaksi = await TRANSAKSI_MODEL.findOne({
      where: { id: req.params.id },
    });

    // Send to notifications
    const notifDataReq = {
      status: "Penawaran Produk Ditolak!",
      updatedAt: new Date(),
    };

    const notif = await sellerUpdateNotification(
      req,
      notifDataReq,
      req.params.id,
      transaksi.id_buyer
    );

    // Send Email
    const profile = await getProfileById(transaksi.id_buyer);
    const product = await findOneProductById(transaksi.id_product);

    const to = profile.email;
    const subject = "Penjual menolak tawaran!";
    const text = `Tawaran anda ke produk dengan nama ${product.name} ditolak oleh penjual!`;
    sendEmailNotification(to, subject, text);

    await res.status(200).json({
      message: "Success!",
      data: dataReq,
      notification: notif,
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Seller Update Response Transaksi diterima
async function sellerAcceptTransaksi(req, res) {
  try {
    const dataReq = {
      isBargainAccepted: true,
      updatedAt: new Date(),
    };

    // Update bargain price
    await TRANSAKSI_MODEL.update(dataReq, {
      where: { id: req.params.id },
    });

    const transaksi = await TRANSAKSI_MODEL.findOne({
      where: { id: req.params.id },
    });

    // Send to notifications
    const notifDataReq = {
      status: "Penawaran Produk Diterima!",
      message: "Kamu akan segera dihubungi penjual via whatsapp",
      updatedAt: new Date(),
    };

    const notif = await sellerUpdateNotification(
      req,
      notifDataReq,
      req.params.id,
      transaksi.id_buyer
    );

    // Send Email
    const profile = await getProfileById(transaksi.id_buyer);
    const product = await findOneProductById(transaksi.id_product);

    const to = profile.email;
    const subject = "Penjual menerima tawaran!";
    const text = `Selamat! Tawaran anda ke produk dengan nama ${product.name} diterima oleh penjual!`;
    sendEmailNotification(to, subject, text);

    await res.status(200).json({
      message: "Success!",
      data: dataReq,
      notification: notif,
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Buyer Update Transaksi
async function buyerUpdateTransaksi(req, res) {
  try {
    const dataReq = {
      bargainPrice: req.body.bargainPrice,
      sellerResponse: false,
      updatedAt: new Date(),
    };

    // Update bargain price
    await TRANSAKSI_MODEL.update(dataReq, {
      where: { id: req.params.id },
    });

    const transaksi = await TRANSAKSI_MODEL.findOne({
      where: { id: req.params.id },
    });

    const notifDataReq = {
      status: "Penawaran Produk!",
      bargainPrice: req.body.bargainPrice,
      updatedAt: new Date(),
    };

    // Send to notifications
    const notif = await buyerUpdateNotification(
      req,
      notifDataReq,
      req.params.id,
      transaksi.id_seller
    );

    // Send Email
    const profile = await getProfileById(transaksi.id_seller);
    const profileBuy = await getProfileById(transaksi.id_buyer);
    const product = await findOneProductById(transaksi.id_product);

    const to = profile.email;
    const subject = "Pembeli menawar kembali!";
    const text = `${profileBuy.username} telah menawar kembali produkmu yang bernama ${product.name}!`;
    sendEmailNotification(to, subject, text);

    await res.status(200).json({
      message: "Success!",
      data: dataReq,
      notification: notif,
    });
  } catch (error) {
    res.status(422).json({
      message: error.message,
    });
  }
}

// Transaksi Final
async function transactionFinal(req, res) {
  try {
    const transaksiVal = req.body.transaksi;

    const transaksi = await TRANSAKSI_MODEL.findOne({
      where: { id: req.params.id },
    });

    const produk = await findOneProductById(transaksi.id_product);
    const profileBuy = await getProfileById(transaksi.id_buyer);

    if (transaksiVal === "accepted") {
      // Update Notifications
      const notifDataReq = {
        status: "Produk Berhasil Dijual!",
        message: "Terima kasih telah membeli produk ini!",
        updatedAt: new Date(),
      };

      const notif = await sellerUpdateNotification(
        req,
        notifDataReq,
        req.params.id,
        transaksi.id_buyer
      );

      // Send Email
      const to = profileBuy.email;
      const subject = "Pembelian berhasil!";
      const text = `Selamat! Anda berhasil membeli produk ${produk.name}!`;
      sendEmailNotification(to, subject, text);

      // Updata transaction complete status
      const transactionDataReq = {
        isCompleted: true,
        updatedAt: new Date(),
      };

      const transactionUpdate = await TRANSAKSI_MODEL.update(
        transactionDataReq,
        {
          where: { id: req.params.id },
        }
      );

      // Update product isSold === true
      const dataReq = {
        isSold: true,
        updatedAt: new Date(),
      };

      const productUpdate = await PRODUCTS_MODEL.update(dataReq, {
        where: {
          id: produk.id,
        },
      });

      res.status(200).json({
        message: "Transaksi Berhasil!",
        notification: notif,
        transaction: transactionUpdate,
        product: productUpdate,
      });
    } else {
      // Update Notifications
      const notifDataReq = {
        status: "Transaksi Dibatalkan!",
        message: "Terima kasih telah mencoba untuk membeli produk ini!",
        updatedAt: new Date(),
      };

      const notif = await sellerUpdateNotification(
        req,
        notifDataReq,
        req.params.id,
        transaksi.id_buyer
      );

      // Send Email
      const to = profileBuy.email;
      const subject = "Pembelian gagal!";
      const text = `Maaf! Pembelian produk ${produk.name} gagal! Terima kasih telah mencoba untuk membeli produk ini!`;
      sendEmailNotification(to, subject, text);

      // Updata transaction complete status
      const transactionDataReq = {
        isCompleted: true,
        updatedAt: new Date(),
      };

      const transactionUpdate = await TRANSAKSI_MODEL.update(
        transactionDataReq,
        {
          where: { id: req.params.id },
        }
      );

      res.status(200).json({
        message: "Transaksi Dibatalkan!",
        notification: notif,
        transaction: transactionUpdate,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
}

module.exports = {
  getUserTransaksi,
  getTransaksiDetail,
  addTransaksi,
  buyerUpdateTransaksi,
  sellerRejectTransaksi,
  sellerAcceptTransaksi,
  transactionFinal,
};
