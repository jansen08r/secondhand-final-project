const db = require("../models");
const { getTokenId } = require("./jwtAuth");
const PRODUCTS_MODEL = db.tbl_products;
const PRODUCTSIMAGE_MODEL = db.tbl_productsImage;

const checkProductOwner = async (req, res, next) => {
  try {
    const userId = getTokenId(req);

    const product = await PRODUCTS_MODEL.findOne({
      where: { id: req.params.id },
    });

    if (product.id_owner !== userId) {
      res.status(401).json({
        message: "Not Authorized!",
      });
    } else {
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

const checkImageOwner = async (req, res, next) => {
  try {
    const userId = getTokenId(req);

    const productImage = await PRODUCTSIMAGE_MODEL.findOne({
      where: { id: req.params.id },
    });

    if (productImage.id_owner !== userId) {
      res.status(401).json({
        message: "Not Authorized!",
      });
    } else {
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

module.exports = {
  checkImageOwner,
  checkProductOwner,
};
