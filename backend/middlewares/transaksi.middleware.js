const { Op } = require("sequelize");
const { getTokenId } = require("./jwtAuth");

const db = require("../models");
const TRANSAKSI_MODEL = db.tbl_transactions;

// Check transaction owner
const checkBuyerSellerAuth = async (req, res, next) => {
  try {
    const userId = getTokenId(req);

    const transaction = await TRANSAKSI_MODEL.findOne({
      where: {
        [Op.or]: [{ id_seller: userId }, { id_buyer: userId }],
      },
    });

    if (!transaction) {
      res.status(404).json({
        message: "Daftar kosong!",
      });
    } else {
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

// Check transaction auth, buyer only
const checkTransactionBuyerAuth = async (req, res, next) => {
  try {
    const userId = getTokenId(req);

    const transaction = await TRANSAKSI_MODEL.findOne({
      where: {
        id_buyer: userId,
      },
    });

    if (!transaction) {
      res.status(401).json({
        message: "Not Authorized!",
      });
    } else {
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

// Check transaction auth, seller only
const checkTransactionSellerAuth = async (req, res, next) => {
  try {
    const userId = getTokenId(req);

    const transaction = await TRANSAKSI_MODEL.findOne({
      where: {
        id_seller: userId,
      },
    });

    if (!transaction) {
      res.status(401).json({
        message: "Not Authorized!",
      });
    } else {
      next();
    }
  } catch (error) {
    res.status(500).json({
      message: error.message,
    });
  }
};

module.exports = {
  checkBuyerSellerAuth,
  checkTransactionBuyerAuth,
  checkTransactionSellerAuth,
};
