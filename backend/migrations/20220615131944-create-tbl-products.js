"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("tbl_products", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      price: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      description: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      isSold: {
        allowNull: true,
        type: Sequelize.BOOLEAN,
      },
      id_category: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_categories",
          key: "id",
        },
        foreignKey: true,
      },
      id_owner: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_users",
          key: "id",
        },
        foreignKey: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("tbl_products");
  },
};
