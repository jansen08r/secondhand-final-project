"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("tbl_transactions", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      id_product: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_products",
          key: "id",
        },
        foreignKey: true,
      },
      id_seller: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_users",
          key: "id",
        },
        foreignKey: true,
      },
      id_buyer: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_users",
          key: "id",
        },
        foreignKey: true,
      },
      bargainPrice: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      sellerResponse: {
        allowNull: true,
        type: Sequelize.BOOLEAN,
      },
      isBargainAccepted: {
        allowNull: true,
        type: Sequelize.BOOLEAN,
      },
      isCompleted: {
        allowNull: true,
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("tbl_transactions");
  },
};
