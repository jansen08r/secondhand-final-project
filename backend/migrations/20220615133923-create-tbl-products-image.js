"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("tbl_productsImages", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      imageURL: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      id_products: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_products",
          key: "id",
        },
        foreignKey: true,
      },
      id_owner: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_users",
          key: "id",
        },
        foreignKey: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("tbl_productsImages");
  },
};
