"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("tbl_notifications", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      id_transaksi: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_transactions",
          key: "id",
        },
        foreignKey: true,
      },

      id_seller: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_users",
          key: "id",
        },
        foreignKey: true,
      },
      id_buyer: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_users",
          key: "id",
        },
        foreignKey: true,
      },
      id_product: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: "tbl_products",
          key: "id",
        },
        foreignKey: true,
      },
      bargainPrice: {
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      status: {
        allowNull: false,
        type: Sequelize.TEXT,
      },
      message: {
        allowNull: true,
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("tbl_notifications");
  },
};
