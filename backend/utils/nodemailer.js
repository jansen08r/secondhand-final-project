require("dotenv").config();

const nodemailer = require("nodemailer");
const { google } = require("googleapis");

const REDIRECT_URI = "https://developers.google.com/oauthplayground";

const OAuth2Client = new google.auth.OAuth2(
  process.env.GMAIL_CLIENT_ID,
  process.env.GMAIL_CLIENT_SECRET,
  REDIRECT_URI
);

OAuth2Client.setCredentials({ refresh_token: process.env.GMAIL_REFRESH_TOKEN });

const sendEmailNotification = async (to, subject, text) => {
  try {
    const accessToken = await OAuth2Client.getAccessToken();

    const transporter = nodemailer.createTransport({
      service: "gmail",
      auth: {
        type: "OAuth2",
        user: "secondHand.kel2.fsw2@gmail.com",
        clientId: process.env.GMAIL_CLIENT_ID,
        clientSecret: process.env.GMAIL_CLIENT_SECRET,
        refreshToken: process.env.GMAIL_REFRESH_TOKEN,
        accessToken: accessToken,
      },
    });

    const mailData = {
      from: "secondHand.kel2.fsw2@gmail.com",
      to: to,
      subject: `SecondHand: ${subject}`,
      text: text,
    };

    const result = await transporter.sendMail(mailData);

    return result;
  } catch (error) {
    return error;
  }
};

module.exports = {
  sendEmailNotification,
};
