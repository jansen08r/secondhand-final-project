Step:
- 1. npm install
- 2. sequelize db:create
- 3. sequelize db:migrate
- 4. sequelize db:seed:all
- 5. npm run dev

Server run Start:
`npm run start`