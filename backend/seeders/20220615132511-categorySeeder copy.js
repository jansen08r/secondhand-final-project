"use strict";

const category = ["Aksesoris", "Elektronik", "Hobi", "Kendaraan", "Pakaian"];

module.exports = {
  async up(queryInterface, Sequelize) {
    const data = [];

    category.forEach((cat) => {
      data.push({
        category: cat,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    });

    await queryInterface.bulkInsert("tbl_categories", data, {});
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("tbl_categories", null, {});
  },
};
