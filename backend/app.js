var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");

var usersRouter = require("./routes/users");
var productsRouter = require("./routes/products");
var transaksiRouter = require("./routes/transaksi");
var notifikasiRouter = require("./routes/notifications");
var dbStatesRouter = require("./routes/dbStates");
var categoriesRouter = require("./routes/categories");

var app = express();

// CORS Settings
const corsOptions = {
  allowedHeaders: ["Content-Type", "Authorization"],
};
app.use(cors(corsOptions));
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE");
  next();
});

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/users", usersRouter);
app.use("/api/products", productsRouter);
app.use("/api/transactions", transaksiRouter);
app.use("/api/notifications", notifikasiRouter);
app.use("/api/states", dbStatesRouter);
app.use("/api/categories", categoriesRouter);

module.exports = app;
