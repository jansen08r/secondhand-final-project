"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tbl_notification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tbl_transactions, {
        foreignKey: "id_transaksi",
      });
      this.belongsTo(models.tbl_users, {
        foreignKey: "id_seller",
      });
      this.belongsTo(models.tbl_users, {
        foreignKey: "id_buyer",
      });
      this.belongsTo(models.tbl_products, {
        foreignKey: "id_product",
      });
    }
  }
  tbl_notification.init(
    {
      id_transaksi: DataTypes.INTEGER,
      id_seller: DataTypes.INTEGER,
      id_buyer: DataTypes.INTEGER,
      id_product: DataTypes.INTEGER,
      bargainPrice: DataTypes.INTEGER,
      status: DataTypes.TEXT,
      message: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "tbl_notification",
    }
  );
  return tbl_notification;
};
