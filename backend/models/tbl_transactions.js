"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tbl_transactions extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tbl_users, {
        foreignKey: "id_seller",
      });
      this.belongsTo(models.tbl_users, {
        foreignKey: "id_buyer",
      });
      this.belongsTo(models.tbl_products, {
        foreignKey: "id_product",
      });
    }
  }
  tbl_transactions.init(
    {
      id_product: DataTypes.INTEGER,
      id_seller: DataTypes.INTEGER,
      id_buyer: DataTypes.INTEGER,
      bargainPrice: DataTypes.INTEGER,
      isBargainAccepted: DataTypes.BOOLEAN,
      sellerResponse: DataTypes.BOOLEAN,
      isCompleted: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "tbl_transactions",
    }
  );
  return tbl_transactions;
};
