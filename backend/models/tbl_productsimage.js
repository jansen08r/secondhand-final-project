"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tbl_productsImage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tbl_users, {
        foreignKey: "id_owner",
      });
      this.belongsTo(models.tbl_products, {
        foreignKey: "id_products",
      });
    }
  }
  tbl_productsImage.init(
    {
      imageURL: DataTypes.TEXT,
      id_products: DataTypes.INTEGER,
      id_owner: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "tbl_productsImage",
    }
  );
  return tbl_productsImage;
};
