"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tbl_users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // this.belongsTo(models.tbl_city, {
      //   foreignKey: "id_city",
      // });
    }
  }
  tbl_users.init(
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      profileImage_url: DataTypes.STRING,
      address: DataTypes.TEXT,
      phoneNumber: DataTypes.TEXT,
      provinceId: DataTypes.INTEGER,
      cityId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "tbl_users",
    }
  );
  return tbl_users;
};
