"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tbl_products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.tbl_users, {
        foreignKey: "id_owner",
      });
      this.belongsTo(models.tbl_categories, {
        foreignKey: "id_category",
      });
    }
  }
  tbl_products.init(
    {
      name: DataTypes.STRING,
      price: DataTypes.INTEGER,
      description: DataTypes.TEXT,
      isSold: DataTypes.BOOLEAN,
      id_category: DataTypes.INTEGER,
      id_owner: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "tbl_products",
    }
  );
  return tbl_products;
};
