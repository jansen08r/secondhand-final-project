"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class tbl_categories extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  tbl_categories.init(
    {
      category: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "tbl_categories",
    }
  );
  return tbl_categories;
};
