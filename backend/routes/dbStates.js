var express = require("express");
const {
  getAllProvince,
  getCities,
  getProvince,
  getCity,
} = require("../controllers/states/states.controller");
var router = express.Router();

// Get Provinsi
router.get("/", getAllProvince);
router.get("/:id", getProvince);

// Get Kabupaten/Kota
router.get("/:id/city", getCities);
router.get("/city/:id", getCity);

module.exports = router;
