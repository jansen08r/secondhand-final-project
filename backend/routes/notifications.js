var express = require("express");
const {
  getUserNotification,
} = require("../controllers/notifications/notifikasi.controller");
const { jwtAuth } = require("../middlewares/jwtAuth");
var router = express.Router();

// Get
router.get("/", jwtAuth, getUserNotification);

module.exports = router;
