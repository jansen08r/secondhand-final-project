var express = require("express");
const {
  getAllCategories,
  getCategoryById,
} = require("../controllers/categories/categories.controller");
var router = express.Router();

// Get
router.get("/", getAllCategories); // Get all categories
router.get("/:id", getCategoryById); // Get one category by id

module.exports = router;
