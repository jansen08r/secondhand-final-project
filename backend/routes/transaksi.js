var express = require("express");
var router = express.Router();
const { jwtAuth } = require("../middlewares/jwtAuth");
const {
  getUserTransaksi,
  addTransaksi,
  getTransaksiDetail,
  buyerUpdateTransaksi,
  sellerRejectTransaksi,
  sellerAcceptTransaksi,
  transactionFinal,
} = require("../controllers/transactions/transaksi.controller");
const {
  checkBuyerSellerAuth,
  checkTransactionBuyerAuth,
  checkTransactionSellerAuth,
} = require("../middlewares/transaksi.middleware");

// Get
router.get("/", [jwtAuth, checkBuyerSellerAuth], getUserTransaksi); //List semua transaksi
router.get("/:id", [jwtAuth, checkBuyerSellerAuth], getTransaksiDetail); //Get transaction detail

// Post
router.post("/:id/addNew", jwtAuth, addTransaksi); //Add new transaction

// Put
router.put(
  "/:id/update/buyer",
  [jwtAuth, checkTransactionBuyerAuth],
  buyerUpdateTransaksi
); //Buyer update transaksi bargain price
router.put(
  "/:id/update/seller-reject",
  [jwtAuth, checkTransactionSellerAuth],
  sellerRejectTransaksi
); // Seller reject transaction
router.put(
  "/:id/update/seller-accept",
  [jwtAuth, checkTransactionSellerAuth],
  sellerAcceptTransaksi
); // Seller reject transaction
router.put(
  "/:id/complete",
  [jwtAuth, checkTransactionSellerAuth],
  transactionFinal
); // Complete transaction

module.exports = router;
