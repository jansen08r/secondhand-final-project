var express = require("express");
var router = express.Router();
const {
  registerUser,
  loginUser,
} = require("../controllers/users/loginRegister.controller");
const {
  getUserProfile,
  updateUserProfile,
  getUserProfileById,
} = require("../controllers/users/users.controller");
const { jwtAuth } = require("../middlewares/jwtAuth");
const fileCloudinaryMiddlware = require("../middlewares/cloudinary/cloudinary.middleware");

/* Get */
router.get("/profile", jwtAuth, getUserProfile); // Get User Profile using token
router.get("/:id/profile", getUserProfileById); // Get User Profile by Id

/* Post */
router.post("/register", registerUser); // Register User
router.post("/login", loginUser); // Login User

/* Put */
router.put(
  "/profile/update",
  [jwtAuth, fileCloudinaryMiddlware.single("image")],
  updateUserProfile
);

module.exports = router;
