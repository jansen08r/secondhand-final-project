var express = require("express");
var router = express.Router();
const {
  createProduct,
  updateProduct,
  getAllProducts,
  getProductDetail,
  deleteProduct,
  getProductSold,
} = require("../controllers/products/products.controller");
const { jwtAuth } = require("../middlewares/jwtAuth");
const fileCloudinaryMiddlware = require("../middlewares/cloudinary/cloudinary.middleware");
const {
  checkProductOwner,
  checkImageOwner,
} = require("../middlewares/users.middleware");
const {
  uploadProductImage,
  deleteProductImage,
  getAllImages,
  getProductImages,
} = require("../controllers/products/images.controller");

/* Get */
router.get("/", getAllProducts); // Get all products
router.get("/:id/detail", getProductDetail); // Get a product detail
router.get("/images", getAllImages); // Get all images
router.get("/:id/images", getProductImages); // Get a product images
router.get("/sold", jwtAuth, getProductSold); // Get user sold products

/* Post */
router.post(
  "/create",
  [jwtAuth, fileCloudinaryMiddlware.single("image")],
  createProduct
); // Create new products
router.post(
  "/:id/uploadImage",
  [jwtAuth, checkProductOwner, fileCloudinaryMiddlware.single("image")],
  uploadProductImage
); // Upload image product

/* Put */
router.put("/:id/update", [jwtAuth, checkProductOwner], updateProduct); // Update Product

/* Delete */
router.delete("/:id/delete", [jwtAuth, checkProductOwner], deleteProduct); // Delete a product
router.delete(
  "/image/:id/delete",
  [jwtAuth, checkImageOwner],
  deleteProductImage
); // Delete product image

module.exports = router;
