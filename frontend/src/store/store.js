import { createStore, compose, applyMiddleware } from 'redux'
import { composeWithDevTools } from "redux-devtools-extension";

import reducer from './reducers' // ngambang gini otomatis ngarah ke /index
import thunk from 'redux-thunk';

// const store = createStore(reducer)
// const store = createStore(reducer, compose(applyMiddleware(thunk)))

const composed = composeWithDevTools(applyMiddleware(thunk));
const store = createStore(reducer, composed);



export default store