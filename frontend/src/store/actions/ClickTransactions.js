
export const CLICK_TRANSACTIONS = "CLICK_TRANSACTIONS";

export const ClickTransactions = (data) => async (dispatch) => {
    try{
        dispatch({
            type: "CLICK_TRANSACTIONS",
            payload: {
                data
            }
        })
    }
    catch(err){
        console.log(err);
    }
   
}