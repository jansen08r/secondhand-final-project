import axios from "axios"

export const SEE_DETAIL_PRODUCT = "SEE_DETAIL_PRODUCT";

export const seeDetailProduct = (id) => async (dispatch) => {
    try{
        const {data} = await axios.get(`http://localhost:8000/api/products/${id}/detail`);
        console.log(data, "ini hasil axios dari thunk");
        dispatch({
            type: "SEE_DETAIL_PRODUCT",
            payload: {
                data
            }
        })
    }
    catch(err){
        console.log(err);
    }
   
}