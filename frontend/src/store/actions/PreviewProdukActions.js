
export const PREVIEW_PRODUCT = "PREVIEW_PRODUCT";

export const previewProduct = (data) => async (dispatch) => {
    try{
        dispatch({
            type: "PREVIEW_PRODUCT",
            payload: {
                data
            }
        })
    }
    catch(err){
        console.log(err);
    }
   
}