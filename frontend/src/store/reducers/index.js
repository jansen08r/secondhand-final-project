import { combineReducers } from 'redux'
import productDetailReducer from './ProductDetailReducers'
import previewProductReducer from './PreviewProdukReducers'
import ClickTransactionsReducers from './ClickTransactionsReducers'


const reducer = combineReducers({
    productDetail: productDetailReducer,
    previewProduct: previewProductReducer,
    ClickTransactions : ClickTransactionsReducers
})

export default reducer