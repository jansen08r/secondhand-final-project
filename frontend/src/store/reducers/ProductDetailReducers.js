import { SEE_DETAIL_PRODUCT } from "../actions";

export default function productDetailReducer(state=[], action){
    switch(action.type){
        case SEE_DETAIL_PRODUCT:
            console.log(action.payload.data, "ini action.payload.data SEE_DETAIL_PRODUCT");
            return [action.payload.data]

        default:
            return state;
    }
}