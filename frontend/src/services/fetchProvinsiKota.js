import axios from "axios";

export function fetchProvinsi() {
    return axios.get('https://dev.farizdotid.com/api/daerahindonesia/provinsi', {
      headers: {"Access-Control-Allow-Origin": "*"}
    });
  }

export function fetchKota(id) {
  return axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=${id}`,{
    headers: {"Access-Control-Allow-Origin": "*"}
  });
}