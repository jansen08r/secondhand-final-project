import axios from "axios";

export function updateInformasiProdukService(formData){
    let token = localStorage.getItem('token');

    return axios.post('http://localhost:8000/api/products/create',formData, {
        headers: {
        'Authorization':`${token}`, // Bearer nya udah di BE jadi di FE gausah tambah bearer
        'content-type': 'multipart/form-data'
        }
    })
    
    // return axios.post('http://localhost:3000/cards', data)
}