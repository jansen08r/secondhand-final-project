import axios from "axios";

export function updateInformasiAkunService(data){
    let token = localStorage.getItem('token');

    return axios.put('http://localhost:8000/api/users/profile/update',data,{
        headers: {
            'Authorization':`${token}` // Bearer nya udah di BE jadi di FE gausah tambah bearer
        }
    })
}
