import axios from "axios";

export function loginService(data){
    return axios.post('http://localhost:8000/api/users/login',data)
}

export function registerService(data) {
    return axios.post('http://localhost:8000/api/users/register',data)
}