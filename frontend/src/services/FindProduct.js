import axios from "axios";

export async function findProductService(id){
    return await axios.get(`http://localhost:8000/api/products/${id}/detail`)
}

