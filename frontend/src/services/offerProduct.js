import axios from "axios";

const token = localStorage.getItem('token')

console.log(token);

export async function offerProductService(id, data){
    return await axios.post(`http://localhost:8000/api/transactions/${id}/addNew`,
    data,
    {
        headers: {
            Authorization: token
        }
    })
}

