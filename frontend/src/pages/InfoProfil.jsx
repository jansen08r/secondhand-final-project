import React, {useRef, useState, useEffect} from 'react';
import { Link, useNavigate } from "react-router-dom";
import fi_camera from '../images/fi_camera.png';
import fi_arrowleft from '../images/fi_arrowleft.png';
import logoKotak from '../images/logoKotak.png';
import { loginService } from '../services/loginRegister';
import { updateInformasiAkunService } from '../services/updateInformasiAkun';
import { fetchKota, fetchProvinsi } from '../services/fetchProvinsiKota';
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';
import './styles.css';
import axios from 'axios';

import { MdUploadFile } from 'react-icons/md';

const InfoProfil = () => {
    const navigate = useNavigate();

    const [username, setUsername] = useState('');
    const [cityId, setCityId] = useState('');
    const [provinceId, setProvinceId] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [listProvince, setListProvince] = useState('');
    const [listCity, setListCity] = useState('');
    const [email, setEmail] = useState('')

    const [namaSendiri, setNamaSendiri] = useState('')
    const [emailSendiri, setEmailSendiri] = useState('')
    const [gambarSendiri, setGambarSendiri] = useState('')
    const [addressSendiri, setAddressSendiri] = useState('')
    const [nomorSendiri, setNomorSendiri] = useState('')

    const [login, setLogin] = useState(false)

    const [file, setFile] = useState();
    const [fileUrl, setFileUrl] = useState('');

    const fileInputRef = useRef();

    const handleNameInput = (event) => {
        setUsername(event.target.value)
    }

    const handleCityInput = (event) => {
        setCityId(event.target.value)
        console.log(event.target.value);
    }

    const handleProvinceInput = (e) => {
        setProvinceId(e.target.value)
    }

    const handleAddressInput = (event) => {
        setAddress(event.target.value)
    }

    const handleNumberInput = (event) => {
        setPhoneNumber(event.target.value)
    }

    const handleGambar = (e) => {
        e.preventDefault()
        const file = e.target.files[0];
        setFile(e.target.files[0]);

        const reader = new FileReader();
        reader.onloadend = () => {
            console.log(reader.result);
            setFileUrl(reader.result)
        };
        reader.readAsDataURL(file);
        
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        let formData = new FormData();

        formData.append('username', username)
        formData.append('email', email)
        formData.append('image', file)
        formData.append('address', address)
        formData.append('phoneNumber', phoneNumber)
        formData.append('provinceId', provinceId)
        formData.append('cityId', cityId)

        console.log(...formData, "ini formDataaa");

        updateInformasiAkunService(formData).then((res) => {
            console.log("berhasil", res.data);
            // localStorage.setItem('token', res.data.token)
            setLogin(true)
            // navigate('/')
            toast.success('Info profil berhasil diupdate!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }).catch((err) => {
            // window.location.reload(false); // ini nge refresh kalo gagal
            console.log(err);
            toast.error('Info profil gagal diupdate!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        })
    }

    // List Provinsi
    useEffect(() => {
        async function fetchMyApi() {
            let res = await fetchProvinsi();
            setListProvince(...listProvince, res.data);
        }

        fetchMyApi();
    }, []);

    useEffect(() => {
        async function fetchMyApi() {
                setLogin(true);
                let {data} = await axios.get(`http://localhost:8000/api/users/profile`,{
                    headers: {
                        Authorization: `${localStorage.getItem('token')}`
                    }
                })
                setNamaSendiri(data.data.username) 
                setEmailSendiri(data.data.email)
                setAddressSendiri(data.data.address)
                setNomorSendiri(data.data.phoneNumber)
                setGambarSendiri(data.data.profileImage_url)
                let res = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${data.data.cityId}`)
                // setKotaSendiri(res.data.nama)
        }
        fetchMyApi();
    }, [])

    // List Kota ( biasa disebut callback )
    // dia langsung jalanin fungsi yang callbacknya didengerin sama useEffect, keren sih, meskipun lebih panjang kalo pake functional component , sumber : On Ben Hare's answer, If someone wants to achieve the same using React Hooks I have added sample code below.
    useEffect(() => {  // this hook will get called everytime when myArr has changed
        // perform some action which will get fired everytime when myArr gets updated

        async function fetchMyApi() {
            let res = await fetchKota(provinceId); // isi provinsi itu id
            setListCity(res.data);
        }
        fetchMyApi();
    }, [provinceId]);


    return (
        <div class='container-fluid px-0 '>

            <nav class="navbar navbar-expand d-none d-md-block navbar-light bg-light shadow-sm">
                    <div className="d-flex w-100 justify-content-between">
                        <Link to='/' style={{textDecoration: "none", color: "purple"}}> 
                                <img src={logoKotak} alt="" className='ps-5 d-none d-md-block'/>
                        </Link> 
                        <div class=" position-absolute top-50 start-50 translate-middle d-none d-md-block h6" >
                                Lengkapi Info Akun
                        </div>
                    </div>
            </nav>
            

            <div className='d-flex justify-content-center'>
                <form class=' w-100'>
                    <div className='d-flex align-items-start position-relative mt-5 mb-5 pb-4'>
                            
                            <Link to='/' style={{textDecoration: "none", color: "purple"}}> 
                                <img src={fi_arrowleft} alt="" class='ps-3'/>
                            </Link> 
                            
                            <div className='position-absolute top-0 start-50 translate-middle-x w-75 px-3'>
                                <div class="d-flex justify-content-center pb-4 h6 d-block d-md-none" >
                                    Lengkapi Info Akun
                                </div>
        
                                {file !== undefined ? 
                                <>
                                    <div class="border-radius-13px d-flex flex-column align-items-center" style={{}}>
                                        <img src={fileUrl} alt="" style={{width: "110px", height: "110px", objectFit: "cover", borderRadius: "13px"}}/>
                                        {/* <button style={{border: "none", borderRadius: "12px", height: "96px", width: "96px", background: "#E2D4F0;"}} 
                                        onClick={() => fileInputRef.current.click()}>
                                            <BsCamera size={28}/>
                                        </button> */}
                                        {/* <button style={{border: "none", borderRadius: "12px"}} 
                                        onClick={() => fileInputRef.current.click()}>
                                            Ganti Gambar
                                        </button> */}
                                        <input accept='image/*' type="file" id="files" style={{display: "none"}} name="upload_gambar" onChange={(e) => handleGambar(e)}/>
                                        <label className="d-flex justify-content-center" for="files" style={{ marginTop: "20px", backgroundColor: "#7126B5", color: "white", borderRadius: "16px", fontSize: "12px", border: "none", width: "120px", padding: "10px 0 10px 0", cursor: "pointer"}}><MdUploadFile size={18} style={{float: "left"}}/>Ganti Gambar</label>
                                    </div>
                                </>
                                :
                                <div className='d-flex flex-column align-items-center'>
                                    
                                    <input accept='image/*' type="file" id="files1" style={{display: "none"}} name="upload_gambar" onChange={(e) => handleGambar(e)} />
                                    <label for="files1" style={{cursor: "pointer"}}><div class="square mx-auto border-radius-13px d-flex justify-content-center align-items-center" style={{backgroundColor: '#E2D4F0'}}>
                                        {
                                            gambarSendiri !== null ?
                                            <img src={gambarSendiri} alt="" style={{width: "100%", height: "100%", objectFit: "cover", borderRadius: "13px"}} />
                                            :
                                            <img src={fi_camera} alt="" class=''/>

                                        }
                                    </div>
                                    </label>
                                </div>
                                }
                                <div className=" mx-auto pt-3">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Nama*</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder={namaSendiri !== null ? namaSendiri : 'John Doe' } 
                                         
                                        onChange={(e) => handleNameInput(e)}/>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Email</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Email'  
                                        value={emailSendiri !== null ? emailSendiri : '' } 
                                        onChange={(e) => setEmail(e.target.value)}/>
                                    </div>

                                    {listProvince.length === 0 ? 
                                        <>
                                            <div class="mb-3">
                                                <label for="exampleInputPassword1" class="form-label">Provinsi*</label>
                                                <select class="form-select border-radius-13px" aria-label="Default select example"  onChange={(e) => handleProvinceInput(e)}>
                                                    <option value="" disabled selected hidden>Pilih Provinsi</option>
                                                </select>
                                            </div>
                                        </>
                                        :
                                        <div class="mb-3">
                                            <label for="exampleInputPassword1" class="form-label">Provinsi*</label>
                                            <select class="form-select border-radius-13px" aria-label="Default select example"  onChange={(e) => handleProvinceInput(e)}>
                                                <option value="" disabled selected hidden>Pilih Provinsi</option>
                                                {
                                                    listProvince.length !== 0 ?
                                                    <>
                                                        {listProvince.map((provinsi, index) => <option value={provinsi.id} key={index}>{provinsi.name}</option> )}
                                                    </>
                                                    :
                                                    <></>
                                                }
                                            </select>
                                        </div>
                                    }
                                    {listCity.length === 0 ? 
                                        
                                        <div class="mb-3">
                                            <label for="exampleInputPassword1" class="form-label">Kota*</label>
                                            <select class="form-select border-radius-13px" aria-label="Default select example"  onChange={(e) => handleCityInput(e)}>
                                                <option value="" disabled selected >Pilih Dulu Provinsi</option>
                                            </select>
                                        </div>
                                        :
                                        <div class="mb-3">
                                            <label for="exampleInputPassword1" class="form-label">Kota*</label>
                                            <select class="form-select border-radius-13px" aria-label="Default select example"  onChange={(e) => handleCityInput(e)}>
                                                <option value="" disabled selected hidden>Pilih Kota</option>
                                                {listCity.map((kota, index) => <option value={kota.id} key={index}>{kota.name}</option> )}
                                            </select>
                                        </div>

                                    }
                                    
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Alamat*</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder={addressSendiri !== null ? addressSendiri : 'Jalan Hiu 123' }  
                                        
                                        onChange={(e) => handleAddressInput(e)}></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">No Handphone*</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" 
                                        
                                        placeholder={nomorSendiri !== null ? nomorSendiri : 'Contoh : 6281344881956' }  onChange={(e) => handleNumberInput(e)}/>
                                    </div>
                                    <button type="submit" class="btn w-100 mb-4" style={{backgroundColor: '#7126B5', color:'white', borderRadius: "16px"}} onClick={(e) => handleSubmit(e)} >Simpan</button>
                                </div>
                            </div>
                    </div>
                    
                </form>
            </div>
        </div>
    );
}

export default InfoProfil;
