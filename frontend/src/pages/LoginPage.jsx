import React,{ useState } from 'react';
import loginAtauRegister from '../images/loginAtauRegister.png';
import "./styles.css"
import { Link, useNavigate } from "react-router-dom";
import { loginService } from '../services/loginRegister';
import fi_eye from '../images/fi_eye.png'
import fi_arrowleft from '../images/fi_arrowleft.png'
import { useEffect } from 'react';
import 'react-toastify/dist/ReactToastify.css';

import { ToastContainer, toast } from 'react-toastify'; // toast 

const LoginPage = () => {
    const navigate = useNavigate();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [login, setLogin] = useState(false)
    const [mata, setMata] = useState(false)
 
    const handleEmailInput = (event) => {
        setEmail(event.target.value)
        console.log(event.target.value);
    }

    const handlePasswordInput = (event) => {
        setPassword(event.target.value)
        console.log(event.target.value);
    }

    const handleMata = () => {
        mata ? setMata(false) : setMata(true)
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log({
            email: email,
            password: password
        });

        let loginData = {
            email: email,
            password: password
        }

        loginService(loginData).then((res) => {
            toast.success('Login Berhasil!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            // console.log("berhasil", res.data);
            localStorage.setItem('token', res.data.token) // buat taruh di local storage
            setLogin(true)
            navigate('/')
        }).catch((err) => {
            console.log(err);
            toast.error('Username atau password salah!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        })
    }

    const handleLogout = () => {
        // localStorage.clear()
        localStorage.removeItem("token")
        setLogin(false)
    }


    useEffect(() => {
        let token = localStorage.getItem('token')
        if(token){
            navigate('/')
            // navigate(-1)
        }
    },[])

    return (
        <div>
            
            {/* Container-fluid jangan dikasih px-0 mx-0 nanti bakal ada kayak tambahan scroll kebawah sama kekanan entah dari mana .. */}
            <div className="container-fluid ">
                <div className="row">
                    <div className='col-md-6 d-none d-sm-block px-0 mx-0'>
                            <img src={loginAtauRegister} className="" alt="loginAtauRegister" style={{width : '100%', height:'100vh', objectFit: "cover"}} />
                    </div>
                    <div className='col-md-6 px-0 mx-0 d-flex justify-content-center align-items-md-center min-vh-100 coba-Rel' >
                        <div class="w-75 ">
                            <button onClick={() => navigate(-1)} className='coba-Abs1 d-block d-md-none border-0 px-0'>
                                <img src={fi_arrowleft} alt=""/>
                            </button>
                            <h1>Masuk</h1>
                            <form >
                                <div class="mb-3">
                                    <label for="exampleInputEmail1" class="form-label" >Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contoh: johndee@gmail.com" onChange={(e) => handleEmailInput(e)} />
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label" >Password</label>
                                    <div className="position-relative">
                                        <img src={fi_eye} alt="" className='position-absolute top-50 end-0 translate-middle-y pe-3' onClick={() => handleMata()}/>
                                        {mata ? 
                                        <input type='text' class="form-control" id="exampleInputPassword1" placeholder="Masukkan password" onChange={(e) => handlePasswordInput(e)} />
                                        :
                                        <input type='password' class="form-control" id="exampleInputPassword1" placeholder="Masukkan password" onChange={(e) => handlePasswordInput(e)} /> 
                                    }
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary w-100 border-0" style={{backgroundColor: '#7126B5', color:'white', borderRadius: "16px"}}onClick={(e) => handleSubmit(e)} >Masuk</button>
                            </form>
                            <div className=" coba-Abs mb-4 mb-md-0 w-100">
                                <div className='mt-4 text-center w-100' >
                                    Belum punya akun? <Link to="/register" style={{textDecoration: "none", color: "#7126B5"}}> <b>Daftar di sini</b></Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
        </div>
    );
}

export default LoginPage;
