import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import styled from 'styled-components';
import { useParams, useNavigate } from "react-router-dom";
import Navbar from '../components/Navbar';
import Modals from '../components/Modal/Modal';
import ModalsResponsive from '../components/Modal/ModalResponsive';
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';

import imageClock from '../images/gambarJam1.png'
import imagePerson from '../images/person.png'

import '../assets/css/detail-product.style.css'
import axios from 'axios';
import { offerProductService } from '../services/offerProduct';

import { MdFavorite } from "react-icons/md";
import { BsClock } from 'react-icons/bs';

const Wrapper = styled.div`
    display: none;

    @media only screen and (max-width: 576px){
        display: block;

        .separator{
            border-bottom: 3px solid #000000;
            margin: 20px 125px;
            border-radius: 16px;
        }
        
        .cardUi-backdrop{
            position: absolute;
            top: 0;
            width: 100%;
            height: 100%;
            background: red;
            z-index: 9999;
            background: rgba(0,0,0,0.6)
        }
        
        .cardUi{
            display: block;
            width: 100%;
            position: absolute;
            bottom: 0;
            padding: 10px 15px;
            background-color: #fff;
            box-shadow: 0px 10px 15px -3px rgba(0,0,0,0.1);
            border-radius: 16px 16px 0 0;
            z-index: 9999;
        }
    }
`

const DetailProduct = () => {
    const { id } = useParams();
    const navigate = useNavigate()

    const [showModalMobile, setShowModalMobile] = useState(false)
    const [barang, setBarang] = useState([])
    const [login, setLogin] = useState(false)
    const [namaPenjual, setNamaPenjual] = useState('')
    const [kotaPenjual, setKotaPenjual] = useState('')
    const [gambarProfil, setGambarProfil] = useState('')
    const [namaSendiri, setNamaSendiri] = useState('')
    const [kotaSendiri, setKotaSendiri] = useState('')
    const [gambarSendiri, setGambarSendiri] = useState('')
    const [userIdSendiri, setUserIdSendiri] = useState('')
    const [kategori, setKategori] = useState('')

    const [userId, setUserId] = useState('')
    const [harga, setHarga] = useState('')
    const [transactions, setTransactions] = useState([])
    const [tawar, setTawar] = useState('')
    console.log(namaPenjual, userId, "ini nama dan id penjual ");

    const ClickTransactions = useSelector((state) => state.ClickTransactions)

    const handleShowModalMobile = () => {
        setShowModalMobile(!showModalMobile)
    }

    const handleHarga = e => {
        setHarga(e.target.value)
    }

    const handleSubmit = () => {
        const data = {
            bargainPrice: harga
        }

        offerProductService(barang.data[0].id, data).then(res => {
            toast.success('Harga tawarmu berhasil dikirim ke penjual!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            // alert("Tawaranmu berhasil dikirm ke penjual")
        })
    }

    const handleWishlist = async (id) => {
        let {data} = await axios.get('http://localhost:8000/api/users/profile',{
          headers:{
            Authorization : `${localStorage.getItem("token")}`
          }
        })
  
        let wishlist = JSON.parse(localStorage.getItem(`wishlist${data.data.id}`));
        if(wishlist === null){ 
          let arr = []
          arr.push(id)
          localStorage.setItem(`wishlist${data.data.id}`, JSON.stringify(arr))
        }
  
        wishlist.push(id)
        localStorage.setItem(`wishlist${data.data.id}`, JSON.stringify(wishlist))
        toast.success('Telah ditambahkan ke wishlist!', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
    }

    const handleTawar = (e) => {
        setTawar(e.target.value)
        // console.log(e.target.value);
    }

    const handleSubmit1 = async(idTransaksi) => {
        
        const bargainPrice = {
            bargainPrice : tawar
        }

        let {data} = axios.put(`http://localhost:8000/api/transactions/${idTransaksi}/update/buyer`,bargainPrice,{
            headers:{
                Authorization: `${localStorage.getItem('token')}`
            }
        })
        alert('Harga sudah diupdate')
        
    }

    // console.log(barang, "ini Barang");

    // cari sendiri
    useEffect(() => {
        async function fetchMyApi() {
                setLogin(true);
                let {data} = await axios.get(`http://localhost:8000/api/users/profile`,{
                    headers: {
                        Authorization: `${localStorage.getItem('token')}`
                    }
                })
                setNamaSendiri(data.data.username) 
                setUserIdSendiri(data.data.id)
                setGambarSendiri(data.data.profileImage_url)
                // console.log(data.data.username, "ini namaaaaaaaaaa")
                let res = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${data.data.cityId}`)
                setKotaSendiri(res.data.nama)

        }
        fetchMyApi();
    }, [])

    // cari penjual
    useEffect(() => {
        async function fetchMyApi() {
                
                let {data} = await axios.get(`http://localhost:8000/api/users/${barang.data[0].id_owner}/profile`,{
                    headers: {
                        Authorization: `${localStorage.getItem('token')}`
                    }
                })
                setNamaPenjual(data.data.username) 
                setUserId(data.data.id)
                setGambarProfil(data.data.profileImage_url)
                console.log(data.data.username, "ini namaaaaaaaaaa")
                let res = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${data.data.cityId}`)
                setKotaPenjual(res.data.nama)

        }
        fetchMyApi();
    }, [barang])

    useEffect(() => {
        async function fetchMyApi(){
            let {data} = await axios.get(`http://localhost:8000/api/products/${id}/detail`)
            setBarang(...barang, data)
            // console.log(data, "ini useEffect");
        }
        fetchMyApi()
    },[])

    useEffect(() => {
        async function fetchMyApi(){
            let token = await localStorage.getItem('token');
            let {data} = await axios.get(`http://localhost:8000/api/transactions/`,{
                headers: {
                    Authorization: token
                }
            })
            // console.log(data.data, "ini hasil dari ambil user Transactions");
            setTransactions(...transactions, data.data)
        }
        fetchMyApi()
    },[])

    useEffect(() => {
        const fetchMyApi = async() => {
          // const arrKosong1 = []
    
          let res = await axios.get(`http://localhost:8000/api/categories/${barang.data[0].id_category}`,{
            headers: {
                Authorization: `${localStorage.getItem('token')}`
            }
          })
          setKategori(res.data.data.category)
          // arrKosong1.push(res.data.data.category)
        }
      
        fetchMyApi()
      }, [barang])

      console.log(transactions, "ini transactions dasdada");
  
    return (
        <>
        {barang ? 
        
        <div>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <div className="d-none d-lg-block">
                <Navbar login={login} setLogin={setLogin} />
            </div>
            <div className="page-detailproduct">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="row">
                                <div className="col-12 px-0">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
                                        <div class="carousel-indicators">
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                        </div>
                                        <div class="carousel-inner">
                                            {barang.length !== 0 ? 
                                                <>
                                                    {
                                                        barang.images.length !== 0 ?
                                                        <>
                                                            <div class="carousel-item active">
                                                                <img src={barang.images[0].imageURL} class="d-block w-100" alt="..." style={{ width:"600px", height:"436px"}}/>
                                                            </div>
                                                            {  barang.images.length > 1 ? 
                                                                <>
                                                                    {barang.images.slice(1).map((item) => {
                                                                        return  <div class="carousel-item">
                                                                                    <img src={item.imageURL} class="d-block w-100" alt="..." style={{ width:"600px", height:"436px"}}/>
                                                                                </div>
                                                                    })} 
                                                                </>
                                                                :
                                                                <></>
                                                            }   
                                                        
                                                        </>
                                                        :
                                                        <></>
                                                        
                                                    }
                                                    
                                                </>
                                                :
                                                <></>
                                            }
                                            
                                        </div>

                                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                </div>
                                <div className="col-12 d-block d-md-none">
                                    <div className="card mt-3" style={{borderRadius: "16px"}}>
                                        <div className="card-body">
                                            {barang.length !== 0 ? 
                                            <><h3 className="prduct-name fw-bold">{barang.data[0].name} </h3>
                                            <p className="product-category text-muted">{kategori}</p>
                                            <p className="product-price">Rp. {barang.data[0].price}</p>
                                            </> : <></>
                                            }
                                        </div>
                                    </div>
                                    <div className="card mt-3" style={{borderRadius: "16px"}}>
                                        <div className="card-body" >
                                            <div className="row">
                                                <div className="col-3">
                                                    <img src={gambarProfil} alt="" style={{width: "48px", height: "48px", borderRadius: "12px"}}/>
                                                </div>
                                                <div className="col-9">
                                                    <h5 className="seller-name fw-bold" style={{fontSize: "14px"}}>{namaPenjual}</h5>
                                                    <p className="city" style={{fontSize: "12px"} }>{kotaPenjual}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12 pb-5">
                                {/* { barang.length !== 0 ? <>
                                    <div className="card mt-3 mt-md-5 mb-4" style={{ borderRadius: "16px" }}>
                                        <div className="card-body">
                                            <div className="detailproduct-title fw-bold" style={{fontSize: "16px"}}>Deskripsi</div>
                                            
                                            <div className="detailproduct-description mt-3"style={{fontSize: "14px"}}>
                                                {barang.data[0].description}
                                            </div> 
                                        </div> 
                                    </div>
                                    <div className='d-block d-md-none'>
                                    <ModalsResponsive name={barang.data[0].name} price={barang.data[0].price} image={barang.images[0].imageURL} onChange={value => handleHarga(value)} onClick={() => handleSubmit()}/>
                                    <button type="button" className="d-blok w-100 rounded-pill mt-2 mb-2" onClick={() => handleWishlist(id)} style={{ backgroundColor: "#F94449", color: "white", borderRadius: "16px", fontSize: "14px", border: "none" }}>Add to Wishlist <MdFavorite size={18} style={{float: "right", margin: "2px 6px 0 0 "}}/></button>
                                    </div>
                                    
                                </> : <></>
                                } */}
                                {barang.length !== 0 ? <>
                                <div className="card mt-3 mt-md-5 mb-4" style={{ borderRadius: "16px" }}>
                                    <div className="card-body">
                                        <div className="detailproduct-title fw-bold" style={{fontSize: "16px"}}>Deskripsi</div>
                                        
                                        <div className="detailproduct-description mt-3"style={{fontSize: "14px"}}>
                                            {barang.data[0].description}
                                        </div> 
                                    </div> 
                                </div>
                                <div className='d-block d-md-none'>
                                    {
                                        // baru ngeh karena filter dia kan ngembaliin array, jadi bisa dicek aja apakah hasil filter array nya lebih dari 0, 
                                        // masih bingung ? sumber : https://stackoverflow.com/questions/8217419/how-to-determine-if-javascript-array-contains-an-object-with-an-attribute-that-e#:~:text=No%20need%20to%20reinvent%20the%20wheel%20loop%2C%20at%20least%20not%20explicitly%20(using%20arrow%20functions%2C%20modern%20browsers%20only)%3A
                                        // transactions.filter(i => i.id_product == id).length > 0  ? 

                                        // transactions.filter(i => i.id_product == id).length > 0  ? 
                                        barang.data[0].id_owner === userIdSendiri ?

                                        <>  
                                            {
                                            // barang.data[0].id_owner === userId ?
                                            transactions.filter(i => i.id_product == id).length > 0 &&  transactions.filter(i => i.id_seller == userIdSendiri).length === 0 ? 

                                                <>
                                                    {/* <div>Produk Anda sendiri, silahkan menuju daftar Jual dan Edit jika barang jika diperlukan.</div>
                                                    <div>Anda tidak dapat menego barang milik anda sendiri.</div> */}
                                                    <button type="button" class="btn btn-secondary mb-3 w-100 rounded-pill" style={{borderRadius: "16px", fontSize: "14px"}} disabled>Menunggu Respon Penjual <BsClock size={16} style={{marginTop: "2px", float: "right"}}/></button>
                                                    <button type="button" class="btn btn-outline-success w-100 rounded-pill" onClick={() => navigate('/product-sell#wishlist1') } style={{fontSize : "14px"}}>Update Harga</button>
                                                    <div className='d-flex justify-content-center'></div>
                                                    
                                                </>
                                                :
                                                <>
                                                    <div className='d-flex justify-content-center '>
                                                        <button type="button" class="btn btn-success w-100 rounded-pill" onClick={() => navigate('/product-sell#product')}>Edit Produk</button>
                                                    </div>
                                                </>                                            
                                            }
                                            
                                        </>
                                        :
                                        <>
                                            
                                            {
                                            // barang.data[0].id_owner === userId ?
                                            transactions.filter(i => i.id_product == id).length > 0 &&  transactions.filter(i => i.id_seller == userIdSendiri).length === 0 ? 

                                                <>
                                                    {/* <div>Produk Anda sendiri, silahkan menuju daftar Jual dan Edit jika barang jika diperlukan.</div>
                                                    <div>Anda tidak dapat menego barang milik anda sendiri.</div> */}
                                                    <button type="button" class="btn btn-secondary mb-3 w-100 rounded-pill" style={{borderRadius: "16px", fontSize: "14px"}} disabled>Menunggu Respon Penjual <BsClock size={16} style={{marginTop: "2px", float: "right"}}/></button>
                                                    <button type="button" class="btn btn-outline-success w-100 rounded-pill" onClick={() => navigate('/product-sell#wishlist1') } style={{fontSize : "14px"}}>Update Harga</button>
                                                    <div className='d-flex justify-content-center'></div>
                                                    
                                                </>
                                                :
                                                <>
                                                        {/* BARANG HARUS ADA IMAGE ! ATAU GA TERPAKSA DITUNGGU DULU PAKE TERNARY*/}
                                                        {barang.images.length !== 0 ?
                                                            <>
                                                            <ModalsResponsive name={barang.data[0].name} price={barang.data[0].price} image={barang.images[0].imageURL} onChange={value => handleHarga(value)} onClick={() => handleSubmit()}/>
                                                            <button type="button" className="d-blok w-100 rounded-pill mt-2" onClick={() => handleWishlist(id)} style={{ backgroundColor: "#F94449", color: "white", borderRadius: "16px", fontSize: "14px", border: "none" }}>Add to Wishlist <MdFavorite size={18} style={{float: "right", margin: "2px 6px 0 0 "}}/></button>
                                                            </>
                                                            :
                                                            <></>
                                                        }
                                                </>                                            
                                            }
                                        </>
                                    }
                                    
                                </div>
                                </>:
                                <></>
                                }   
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 d-none d-md-block ps-4">
                            <div className="card shadow" style={{ borderRadius: "16px" }}>
                                {barang.length !== 0 ? 

                                <div className="card-body">
                                    <h3 className="prduct-name fw-bold" style={{ fontSize: "18px"}}>{barang.data[0].name}</h3>
                                    <p className="product-category text-muted" style={{ fontSize: "14px"}}>{kategori}</p>
                                    <p className="product-price" style={{ fontSize: "18px"}}>Rp. {barang.data[0].price}</p>
                                    {
                                        // baru ngeh karena filter dia kan ngembaliin array, jadi bisa dicek aja apakah hasil filter array nya lebih dari 0, 
                                        // masih bingung ? sumber : https://stackoverflow.com/questions/8217419/how-to-determine-if-javascript-array-contains-an-object-with-an-attribute-that-e#:~:text=No%20need%20to%20reinvent%20the%20wheel%20loop%2C%20at%20least%20not%20explicitly%20(using%20arrow%20functions%2C%20modern%20browsers%20only)%3A
                                        // transactions.filter(i => i.id_product == id).length > 0  ? 

                                        // transactions.filter(i => i.id_product == id).length > 0  ? 
                                        barang.data[0].id_owner === userIdSendiri ?

                                        <>  
                                            {
                                            // barang.data[0].id_owner === userId ?
                                            transactions.filter(i => i.id_product == id).length > 0  &&  transactions.filter(i => i.id_seller == userIdSendiri).length === 0 ? 

                                                <>
                                                    {/* <div>Produk Anda sendiri, silahkan menuju daftar Jual dan Edit jika barang jika diperlukan.</div>
                                                    <div>Anda tidak dapat menego barang milik anda sendiri.</div> */}
                                                    <button type="button" class="btn btn-secondary mb-3 w-100 rounded-pill" style={{borderRadius: "16px", fontSize: "14px"}} disabled>Menunggu Respon Penjual <BsClock size={16} style={{marginTop: "2px", float: "right"}}/></button>
                                                    <button type="button" class="btn btn-outline-success w-100 rounded-pill" onClick={() => navigate('/product-sell#wishlist1') } style={{fontSize : "14px"}}>Update Harga</button>
                                                    <div className='d-flex justify-content-center'></div>
                                                    
                                                </>
                                                :
                                                <>
                                                    <div className='d-flex justify-content-center '>
                                                        <button type="button" class="btn btn-success w-100 rounded-pill" onClick={() => navigate('/product-sell#product')}>Edit Produk</button>
                                                    </div>
                                                </>                                            
                                            }
                                            
                                        </>
                                        :
                                        <>
                                            
                                            {
                                            // barang.data[0].id_owner === userId ?
                                            transactions.filter(i => i.id_product == id).length > 0  &&  transactions.filter(i => i.id_seller == userIdSendiri).length === 0 ? 

                                                <>
                                                    {/* <div>Produk Anda sendiri, silahkan menuju daftar Jual dan Edit jika barang jika diperlukan.</div>
                                                    <div>Anda tidak dapat menego barang milik anda sendiri.</div> */}
                                                    <button type="button" class="btn btn-secondary mb-3 w-100 rounded-pill" style={{borderRadius: "16px", fontSize: "14px"}} disabled>Menunggu Respon Penjual <BsClock size={16} style={{marginTop: "2px", float: "right"}}/></button>
                                                    <button type="button" class="btn btn-outline-success w-100 rounded-pill" onClick={() => navigate('/product-sell#wishlist1') } style={{fontSize : "14px"}}>Update Harga</button>
                                                    <div className='d-flex justify-content-center'></div>
                                                    
                                                </>
                                                :
                                                <>
                                                        {/* BARANG HARUS ADA IMAGE ! ATAU GA TERPAKSA DITUNGGU DULU PAKE TERNARY*/}
                                                        {barang.images.length !== 0 ?
                                                            <>
                                                            <Modals name={barang.data[0].name} price={barang.data[0].price} image={barang.images[0].imageURL} onChange={value => handleHarga(value)} onClick={() => handleSubmit()}/>
                                                            <button type="button" className="d-blok w-100 rounded-pill mt-2" onClick={() => handleWishlist(id)} style={{ backgroundColor: "#F94449", color: "white", borderRadius: "16px", fontSize: "14px", border: "none" }}>Add to Wishlist <MdFavorite size={18} style={{float: "right", margin: "2px 6px 0 0 "}}/></button>
                                                            </>
                                                            :
                                                            <></>
                                                        }
                                                </>                                            
                                            }
                                        </>
                                    }
                                    
                                </div>
                                :
                                <></>
                                }   
                            </div>
                            <div className="card shadow mt-4" style={{ borderRadius: "16px" }}>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-2">
                                            <img src={gambarProfil} alt="" style={{width: "48px", height: "48px", borderRadius: "12px"}}/>
                                        </div>
                                        <div className="col-10">
                                            <h5 className="seller-name fw-bold" style={{fontSize: "16px"}}>{namaPenjual}</h5>
                                            <p className="city" style={{fontSize: "12px"}}>{kotaPenjual}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            {
                showModalMobile === false
                ? <></>
                :(
                    <Wrapper>
                        <div class="cardUi-backdrop">
                            <div class="cardUi">
                                <div class="cardUi-body">
                                    <div class="separator" onClick={handleShowModalMobile}></div>
                                    <h5>Masukkan Harga Tawaranmu</h5>
                                    <p>Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.</p>
                                    <div class="card border-0">
                                        <div class="card-body">
                                            <div class="row shadow py-3 mb-4">
                                                <div class="col-2">
                                                    <img src={imagePerson} alt="profile-seller" />
                                                </div>
                                                <div class="col-10">
                                                    <h4 class="seller-name m-0">Nama Penjual</h4>
                                                    <p class="city m-0">250000</p>
                                                </div>
                                            </div>
                                            <label for="exampleInputEmail1" class="form-label"><b>Harga Tawar</b></label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Rp.' onChange={(e) => handleHarga(e)} />
                                            <button type="button" className="btn btn-primary d-blok w-100 rounded-pill mt-4" onClick={() => handleSubmit()}>Kirim </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Wrapper>
                )
            }
        </div>
        :
        <>SEBENTAR SEDANG MENGAMBIL DATA</>

        }
        </>

    );
}

export default DetailProduct;
