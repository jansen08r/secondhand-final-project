import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';
import Navbar from '../components/Navbar';

import imageClock from '../images/gambarJam1.png'
import imagePerson from '../images/person.png'

import '../assets/css/detail-product.style.css'
import axios from 'axios';
import { updateInformasiProdukService } from '../services/updateInformasiProduk'


const PreviewProduct = () => {
    const navigate = useNavigate();

    const previewProduct = useSelector((state) => state.previewProduct)
    console.log(previewProduct[0].data, "ini previewProduct");
    
    const [login, setLogin] = useState(false)
    const [namaPenjual, setNamaPenjual] = useState('')
    const [kotaPenjual, setKotaPenjual] = useState('')
    const [barang, setBarang] = useState('')
    const [gambarProfil, setGambarProfil] = useState('')
    const [kategori, setKategori] = useState('')
 
    const handleSubmit = async() => {

        let formData = new FormData()
        let formData1 = new FormData()
        let formData2 = new FormData();
        let formData3 = new FormData();

        formData.append('name', previewProduct[0].data.name)
        formData.append('price', previewProduct[0].data.price)
        formData.append('id_category', previewProduct[0].data.id_category)
        formData.append('description', previewProduct[0].data.description)
        formData.append('image', previewProduct[0].data.fileIMG[0])
        if(previewProduct[0].data.fileIMG[1]){
            formData1.append('image', previewProduct[0].data.fileIMG[1])
        }
        if(previewProduct[0].data.fileIMG[2]){
            formData2.append('image', previewProduct[0].data.fileIMG[2])
        }
        if(previewProduct[0].data.fileIMG[3]){
            formData3.append('image', previewProduct[0].data.fileIMG[3])
        }
        
        let token = localStorage.getItem('token');
        let res = await axios.post('http://localhost:8000/api/products/create',formData,{
            headers:{
                'Authorization':`${token}`
            }
        })
        console.log(res,'ini hasil dari post pertama');

        toast.success('Produk berhasil diterbitkan!', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        if(previewProduct[0].data.fileIMG[1]){
            let resp1 = await axios.post(`http://localhost:8000/api/products/${res.data.productImage.id_products}/uploadImage`,formData1, {
                headers:{
                    'Authorization':`${token}`
                }
            })
            console.log(resp1, "ini balikan dari upload gambar kedua ");

        }

        if(previewProduct[0].data.fileIMG[2]){
            let resp2 = await axios.post(`http://localhost:8000/api/products/${res.data.productImage.id_products}/uploadImage`,formData2, {
                headers:{
                    'Authorization':`${token}`
                }
            })
            console.log(resp2, "ini balikan dari upload gambar ketiga ");

        }

        if(previewProduct[0].data.fileIMG[3]){
            let resp3 = await axios.post(`http://localhost:8000/api/products/${res.data.productImage.id_products}/uploadImage`,formData3, {
                headers:{
                    'Authorization':`${token}`
                }
            })
            console.log(resp3, "ini balikan dari upload gambar keempat ");
        }
        
    }

    useEffect(() => {
        async function fetchMyApi() {
            let token = localStorage.getItem('token');
            if (token) {
                setLogin(true)
                let {data} = await axios.get(`http://localhost:8000/api/users/profile`,{
                    headers: {
                        Authorization: token
                    }
                })
                console.log(data.data.cityId, "ini data data buat provinsi dan kota");
                if(data.data.cityId){
                    let res = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${data.data.cityId}`)
                    setKotaPenjual(res.data.nama)
                    // balikkan res nya coba aja paste di gugel nanti, kenapa pake nama
                }
                setNamaPenjual(data.data.username)
                setGambarProfil(data.data.profileImage_url)
            }   
        }
        fetchMyApi();
    }, [])

    useEffect(() => {
        const fetchMyApi = async() => {
          // const arrKosong1 = []
    
          let res = await axios.get(`http://localhost:8000/api/categories/${previewProduct[0].data.id_category}`,{
            headers: {
                Authorization: `${localStorage.getItem('token')}`
            }
          })
          setKategori(res.data.data.category)
          // arrKosong1.push(res.data.data.category)
        }
      
        fetchMyApi()
      }, [previewProduct])

    return (
        <div>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <div className="d-none d-lg-block">
                <Navbar login={login} setLogin={setLogin} />
            </div>
            <div className="page-detailproduct">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="row">
                                <div className="col-12 px-0">
                                    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="true">
                                        <div class="carousel-indicators">
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                            <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                        </div>
                                        {/* <img src={previewProduct[0].data.image[0]} class="d-block w-100" alt="..." />
                                        {previewProduct[0].data.image.map((item, index) => {
                                                return  <div class="carousel-item">
                                                            <img src={item[index]} class="d-block w-100" alt="..." />
                                                        </div>
                                        }) } */}

                                        <div class="carousel-inner">
                                            <div class="carousel-item active">
                                                <img src={previewProduct[0].data.image[0]} class="d-block w-100" alt="..." style={{ width:"600px", height:"436px"}}/>
                                            </div>

                                            {previewProduct[0].data.image.length > 0 ?
                                            <>
                                                {previewProduct[0].data.image.slice(1).map((item) => {
                                                return  <div class="carousel-item">
                                                            <img src={item} class="d-block w-100" alt="..." style={{ width:"600px", height:"436px"}}/>
                                                        </div>
                                            }) }
                                            </>
                                            :
                                            <></>
                                            
                                            }
                                            
                                        </div>
                                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Previous</span>
                                        </button>
                                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="visually-hidden">Next</span>
                                        </button>
                                    </div>
                                </div>
                                <div className="col-12 d-block d-md-none">
                                    <div className="card mt-3" style={{borderRadius: "16px"}}>
                                        <div className="card-body">
                                        <h3 className="prduct-name fw-bold" style={{ fontSize: "18px"}}>{previewProduct[0].data.name} </h3>
                                        <p className="product-category" style={{ fontSize: "14px"}}>{kategori}</p>
                                        <p className="product-price" style={{ fontSize: "18px"}}>Rp {previewProduct[0].data.price}</p>
                                        </div>
                                    </div>
                                    <div className="card mt-3" style={{borderRadius: "16px"}}> 
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-2" style={{marginRight: "10px"}}>
                                                    <img src={gambarProfil} alt="" style={{width: "48px", height: "48px", borderRadius: "12px"}}/>
                                                </div>
                                                <div className="col-9">
                                                    <h5 className="seller-name fw-bold" style={{fontSize: "14px"}}>{namaPenjual}</h5>
                                                    <p className="city" style={{fontSize: "12px"} }>{kotaPenjual}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-12">
                                    <div className="card mt-3 mt-md-5 mb-4 rounded-5" style={{ borderRadius: "16px" }}>
                                        <div className="card-body">
                                            <div className="detailproduct-title fw-bold">Deskripsi</div>
                                            <div className="detailproduct-description mt-3">
                                                {previewProduct[0].data.description}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='d-block d-md-none'>
                                        <button type="button" className="d-blok w-100 rounded-pill" onClick={() => handleSubmit()}style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", fontSize: "14px", border: "none" }}>Terbitkan</button>
                                        <button className="btn btn-outline-success d-blok w-100 rounded-pill mt-3" onClick={() => navigate(-1)} style={{fontSize: "14px"}}>Edit Kembali</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-4 d-none d-md-block">
                            <div className="card shadow" style={{ borderRadius: "16px" }}>
                                <div className="card-body">
                                    <h3 className="prduct-name fw-bold" style={{ fontSize: "18px"}}>{previewProduct[0].data.name} </h3>
                                    <p className="product-category" style={{ fontSize: "14px"}}>{kategori}</p>
                                    <p className="product-price" style={{ fontSize: "18px"}}>Rp {previewProduct[0].data.price}</p>
                                    <button type="button" className="d-blok w-100 rounded-pill" onClick={() => handleSubmit()}style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", fontSize: "14px", border: "none" }}>Terbitkan</button>
                                    <button className="btn btn-outline-success d-blok w-100 rounded-pill mt-3" onClick={() => navigate(-1)} style={{fontSize: "14px"}}>Edit Kembali</button>
                                </div>
                            </div>
                            <div className="card mt-4" style={{borderRadius: "16px"}}>
                                <div className="card-body" >
                                    <div className="row">
                                        <div className="col-2">
                                            <img src={gambarProfil} alt="" style={{width: "48px", height: "48px", borderRadius: "12px"}}/>
                                        </div>
                                        <div className="col-9">
                                            <h5 className="seller-name fw-bold" style={{fontSize: "14px"}}>{namaPenjual}</h5>
                                            <p className="city" style={{fontSize: "12px"} }>{kotaPenjual}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Mobile Button */}
                    {/* <button className="btn btn-primary rounded-pill d-block d-md-none w-100 btn-publish">Terbitkan</button> */}
                </div>
            </div>
        </div>
    );
}

export default PreviewProduct;
