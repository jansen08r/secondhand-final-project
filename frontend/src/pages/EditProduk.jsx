import React,{useState} from 'react'
import logoKotak from '../images/logoKotak.png';
import fi_arrowleft from '../images/fi_arrowleft.png';
import { Link } from 'react-router-dom';
import { useParams, useNavigate } from "react-router-dom";
import axios from 'axios';

export default function EditProduk() {

    const navigate = useNavigate();
    const { id } = useParams();

    const [name, setName] = useState('');
    const [price, setPrice] = useState();
    const [kategori, setKategori] = useState();
    const [description, setDescription] = useState('');
    // const [id, setId] = useState('')
    let formData = new FormData();

    const handleSubmit = async (e) => {
        e.preventDefault()
        // formData.append('name', name)
        // formData.append('price', +price)
        // formData.append('description', description)
        // formData.append('id_category', +kategori)
        // console.log(...formData, "ini formDataa");

        const formData1 = {
            name:name,
            price:+price,
            description:description,
            id_category:kategori
        }

        let token = localStorage.getItem('token');
        let res = await axios.put(`http://localhost:8000/api/products/${id}/update`,formData1,{
            headers:{
                'Authorization':`${token}`
            }
        })
        console.log(res.data.data, "ini balikkan update");
        alert(`Produk dengan id : ${id} berhasil diupdate`)
    }

  return (
    <div class='container-fluid px-0'>
                <nav class="navbar navbar-expand d-none d-md-block navbar-light bg-light shadow-sm">
                        <div className="d-flex w-100 justify-content-between">
                            <Link to='/' style={{textDecoration: "none", color: "purple"}}> 
                                    <img src={logoKotak} alt="" className='ps-5 d-none d-md-block'/>
                            </Link> 
                            <div class=" position-absolute top-50 start-50 translate-middle d-none d-md-block h6" >
                                    Edit Produk
                            </div>
                        </div>
                </nav>
                

                <div className='d-flex justify-content-center' >
                    <form class='w-100'>
                        <div className='d-flex align-items-start  position-relative mt-5 mb-5 pb-4'>

                            <Link to='/' className='ps-3' style={{ textDecoration: "none", color: "purple" }}>
                                <img src={fi_arrowleft} alt="" class='' />
                            </Link>

                            <div className=' position-absolute top-0 start-50 translate-middle-x w-75 px-3 '>
                                <div class="d-flex justify-content-center pb-4 h6 d-block d-md-none" >
                                    Edit Produk
                                </div>
                                <div className=" mx-auto">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Nama Produk</label>
                                        <input type="text" onChange={(e) => setName(e.target.value)} class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Harga Produk</label>
                                        <input type="text" onChange={(e) => setPrice(e.target.value)} class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Kategori</label>
                                        <select class="form-select" onChange={(e) => setKategori(Number(e.target.value))} aria-label="Default select example">
                                            <option disabled selected hidden>Pilih Kategori</option>
                                            <option value="1">Hobi</option>
                                            <option value="2">Kendaraan</option>
                                            <option value="3">Baju</option>
                                            <option value="4">Elektronik</option>
                                            <option value="5">Kesehatan</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Deskripsi</label>
                                        <textarea class="form-control" onChange={(e) => setDescription(e.target.value)} id="exampleFormControlTextarea1" rows="3" placeholder='Contoh: Jalan Ikan Hiu 33'></textarea>
                                    </div>
                                    <div className="row">
                                        {/* <div className="col-md-6 py-3 py-md-0 ">
                                            <button type="button" class="btn btn-outline-primary w-100"
                                            onClick={(e) => handlePreview(e)}
                                            >Preview</button>
                                        </div> */}
                                        <div className="btn-group-vertical pb-5">
                                            <button type="submit" class="w-100" onClick={(e) => handleSubmit(e)} style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", border: "none"}}>Terbitkan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
  )
}
