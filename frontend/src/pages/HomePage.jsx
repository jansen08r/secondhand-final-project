import React, { useState, useEffect } from 'react';
import Navbar from '../components/Navbar';
import Carousel1 from '../components/Carousel1';
import SwiperCarousel from '../components/SwiperCarousel';
import Kategori from '../components/Kategori';
import ListCard from '../components/ListCard';
import SwiperCarousel1 from '../components/SwiperCarousel1';
import { fetchApi } from '../services/FetchCards';
import { search,  filterKategori } from '../services/searchHelper';
import CarouselTelusuri from '../components/CarouselTelusuri';
import TombolJualHomepage from '../components/TombolJualHomepage';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';


const HomePage = () => {

    const navigate = useNavigate()

    const [login, setLogin] = useState(false)
    const [filtered, setFiltered] = useState([]);
    const [keyword, setKeyword] = useState("");
    const [products, setProducts] = useState([])
    const [kategori, setKategori] = useState(0)
    const [gambar, setGambar] = useState('')
    console.log(products, "ini productss");
    console.log(gambar, "ini gambarrrrrrr");
    console.log(filtered, "ini filteredddddddd")

    useEffect(() => {
        async function fetchMyApi() {
            let {data} = await axios.get('http://localhost:8000/api/products/');
            console.log(data.data);
            setProducts(...products, data.data);
        }
        fetchMyApi();
    }, []); // kenapa kalo diisi dependencies products malah error ? 

    useEffect(() => {

        const getWithForOf = async() => {
            const arrKosong = []
            for (const product of products) {
                let dataUser = await axios.get(`http://localhost:8000/api/products/${product.id}/detail/`,{
                        headers: {
                            Authorization: `${localStorage.getItem('token')}`
                        }
                    })
                // if(dataUser.images.length !== 0){
                    arrKosong.push(dataUser.data);
                // }
            }
            setGambar(...gambar, arrKosong)
         }
        getWithForOf();
    }, [products]); 

    useEffect(() => {
        const filtered = search(products, keyword, kategori);
        setFiltered(filtered);
    }, [keyword, kategori]); 

    useEffect(() => {
        let token = localStorage.getItem('token');
        if (token) {
            console.log(token, "ini token, berhasil login");
            setLogin(true);
        }
    }, [login])

    return (
        <div class="position-relative" >
            <Navbar keyword={keyword} handleSearch={(e) => setKeyword(e)} login={login} setLogin={setLogin} />
            <SwiperCarousel />
            <CarouselTelusuri setKategori={setKategori} kategori={kategori} />
            {
                filtered.length > 0 ?
                    <ListCard products={filtered} gambar={gambar}/>
                    :
                    keyword.length > 0 ?
                        <ListCard products={filtered} gambar={gambar}/>
                        :
                        kategori === 0 ? 
                        <ListCard products={products} gambar={gambar}/>
                        :
                        <ListCard products={filtered} gambar={gambar}/>
            }

            <TombolJualHomepage  />
        </div>
    );
}

export default HomePage;
