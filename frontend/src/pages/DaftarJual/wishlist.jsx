import React,{useEffect, useState} from 'react';
import imagempty from '../../images/empty.png'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux'
import { seeDetailProduct } from "../../store/actions/ProductDetailActions";
import { ClickTransactions } from '../../store/actions/ClickTransactions';
import './wishlist1.css'

import empty from "../../images/empty.png"

const Wishlist = (props) => {

    const {products ,productsJual, gambarJual, category} = props
    console.log(productsJual, "ini productsJual di wishlist") // kok kosong ya
    console.log(products, "ini products !");
    // console.log(gambarJual, "ini gambar jual");

    // let ditolak = productsJual.filter((item) => item.sellerResponse === true && item.isBargainAccepted === true)
    // console.log(ditolak, "ini ditolak euy");

    // let belumSelesai = productsJual.filter((item) => item.isCompleted === false)
    // console.log(belumSelesai, "ini belumSelesai");

    // let diterima = productsJual.filter((item) => )


    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [wishlist, setWishlist] = useState([])
    const [barang, setBarang] = useState([])
    const [barangKlik, setBarangKlik] = useState('')
    const [namaPenawar, setNamaPenawar] = useState('')
    // console.log(barangKlik, "ini barangKlik =======================");
    console.log(barang, "ini barang di wishlist, detail barangnya dari get product detail")
    // console.log(productsJual, "ini get all transaction ");

    const handleHapus = async(id) => {
        let token = localStorage.getItem('token');
        let {data} = await axios.delete(`http://localhost:8000/api/products/${id}/delete`,{
            headers:{
                Authorization: token
            }
        });
        alert('barang berhasil dihapus')
        window.location.reload(); 
    }

    const handleEdit = async(id) => {
        navigate(`editproduk/${id}`)
    }

    const handleClick = (id, idTransaksi) => {
        dispatch(seeDetailProduct(id))
        navigate(`/infopenawar/${idTransaksi}`)
    }

    const handleClick1 = async(id) => {
        // console.log(id, "hehe");
        let token = localStorage.getItem('token');
        const {data} = await axios.get(`http://localhost:8000/api/transactions/${id}`,{
            headers:{
                Authorization: token
            }
        });
        dispatch(ClickTransactions(data))
        
    }

    // useEffect(() => {
    //     async function fetchMyApi() {
    //         let token = localStorage.getItem('token');
    //         let {data} = axios.get('http://localhost:8000/api/transactions/',{
    //             headers:{
    //                 Authorization: token
    //             }
    //         })
    //     }
    //     fetchMyApi()
    // },[])
    

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {

            const arrKosong = []
            for (const productJual of productsJual) {
                let dataUser = await axios.get(`http://localhost:8000/api/products/${productJual.id_product}/detail/`,{
                        headers: {
                            Authorization: token
                        }
                    })
                // console.log(dataUser.data.data[0].isSold, "wawawawawawa");
                // if(dataUser.data.data[0].isSold !== true){
                //     arrKosong.push(dataUser.data);
                // }
                arrKosong.push(dataUser.data);

            }
            setBarang(...barang, arrKosong)
         }
         getWithForOf();
    },[productsJual])

    // useEffect(() => {
    //     const fetchMyApi = async() => {
    //       // const arrKosong1 = []
    
    //       let res = await axios.get(`http://localhost:8000/api/users/${productsJual[0].id_buyer}/profile`,{
    //         headers: {
    //             Authorization: `${localStorage.getItem('token')}`
    //         }
    //       })
    //       setNamaPenawar(res.data.data.username)
    //       // arrKosong1.push(res.data.data.category)
    //     }
      
    //     fetchMyApi()
    //   }, [productsJual])

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {

            const arrKosong = []
            for (const productJual of productsJual) {
                let dataUser = await axios.get(`http://localhost:8000/api/users/${productJual.id_buyer}/profile`,{
                    headers: {
                        Authorization: token
                    }
                })
                arrKosong.push(dataUser.data);
            }
            setNamaPenawar(...namaPenawar, arrKosong)
         }
         getWithForOf();
    },[productsJual])

    return (
        <>
            {productsJual.length !== 0 && barang.length !== 0  ? 
                <>
                    {barang.map((item, index) => {
                     return  <>
                        {
                            item.data[0].isSold !== true && productsJual[index].isCompleted != true && productsJual[index].bargainPrice !== null ?
                            <>
                                <div className="col-6 col-md-4">
                                    <div class="card" onClick={() => {handleClick(item.data[0].id, productsJual[index].id); handleClick1(productsJual[index].id);}} 
                                    // className={productsJual[index].isBargainAccepted === true ? 'background-green' : ''}
                                    style={{cursor: "pointer"}}>
                                        {barang[index].images.length !== 0 ?
                                            <img src={barang[index].images[0].imageURL} class="card-img-top" alt="..." style={{
                                                width: "auto",
                                                height: "120px",
                                                objectFit: "cover",
                                            }}/>
                                            :
                                            <></>
                                        }
                                        <div class="card-body">
                                            {
                                                productsJual[index].isBargainAccepted === true ?
                                                <span class="badge rounded-pill bg-success" style={{fontSize: "10px"}}>Diterima</span>
                                                :
                                                <></>
                                            }
                                            {
                                                productsJual[index].isBargainAccepted !== true &&  productsJual[index].sellerResponse === true ?
                                                <span class="badge rounded-pill bg-danger" style={{fontSize: "10px"}}>Ditolak</span>
                                                :
                                                <></>
                                            }
                                            {/* <div>Transaksi ID : {productsJual[index].id}</div>
                                            <div>ID produk : {item.data[0].id}</div> */}
                                            <h5 class="card-title product-title mt-2" style={{fontSize: "16px"}}>{item.data[0].name}</h5>
                                            <p class="card-text product-subtitle" style={{fontSize: "12px"}}>{category[index]}</p>
                                            <p className="product-price" style={{fontSize: "16px"}}>Rp {item.data[0].price}</p>
                                            
                                            {
                                                productsJual[index].bargainPrice !== null ?
                                                    <div style={{fontSize: "16px"}}>Ditawar Rp {productsJual[index].bargainPrice}</div>
                                                :
                                                <></>
                                            }
                                               
                                            {
                                                namaPenawar[index].data.username !== '' ?
                                                <>
                                                    <div style={{fontSize: "16px"}}>Oleh {namaPenawar[index].data.username}</div>
                                                </>
                                                :
                                                <></>
                                            }
                                        </div>
                                        {/* <button onClick={() => handleEdit(item.id)}>Edit Barang</button>
                                        <button onClick={() => handleHapus(item.id)}>Hapus Barang</button> */}
                                    </div>
                                </div>
                            </> 
                            :
                            <></>
                        }
                        </>

                        
                                
                    })}
                </>
                :
                <><div className='d-flex justify-content-center'>
                <div className='flex-column mt-4'>
                    <img src={empty} alt="" style={{ width: "300px"}}></img>
                </div>
            </div></>

            }
        </>
    );
}

export default Wishlist;
