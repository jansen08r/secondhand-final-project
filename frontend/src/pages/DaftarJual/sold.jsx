import React,{useEffect, useState} from 'react';
import imagempty from '../../images/empty.png'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux'
import { seeDetailProduct } from "../../store/actions/ProductDetailActions";
import './wishlist1.css'



const Wishlist = (props) => {

    const {productsJual, gambarJual, category} = props
    // console.log(productsJual, "ini products di wishlist") // kok kosong ya
    // console.log(gambarJual, "ini gambar jual");

    const dispatch = useDispatch()
    const navigate = useNavigate()

    const [wishlist, setWishlist] = useState([])
    const [profil, setProfil] = useState('')
    const [barang, setBarang] = useState([])
    const [isCompleted, setIsCompleted] = useState('')
    const [isCompleted1, setIsCompleted1] = useState('')

    console.log(isCompleted1, "ini isCompleted1 !!!!!!!!")

    console.log(profil, "in profill");
    console.log(isCompleted, "ini isCompleted")
    console.log(barang, "ini barang di sold ---------") 
    // console.log(productsJual, "ini productsJual di wishlist, get all transactions ---------"); 

    const handleHapus = async(id) => {
        let token = localStorage.getItem('token');
        let {data} = await axios.delete(`http://localhost:8000/api/products/${id}/delete`,{
            headers:{
                Authorization: token
            }
        });
        alert('barang berhasil dihapus')
        window.location.reload(); 
    }

    const handleEdit = async(id) => {
        navigate(`editproduk/${id}`)
    }

    const handleClick = (id) => {
        // dispatch(seeDetailProduct(id))
        // navigate('/infopenawar')
    }

    useEffect(() => {
        async function fetchMyApi() {
            let token = localStorage.getItem('token');
            let {data} = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                    Authorization: token
                }
            })
            // console.log(data, "ini balikkan profil");
            setProfil(data.data)
        }
        fetchMyApi()
    },[])
    // Transaction
    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {

            let {data} = await axios.get(`http://localhost:8000/api/transactions/`,{
                headers: {
                    Authorization: token
                }
            })

            let arr = data.data.filter((item, index) => item.isCompleted == true )
            setIsCompleted(...isCompleted, arr)

            console.log(data.data, "data.data.id_product");
         }
         getWithForOf();
    },[])

    // Notification    
    useEffect(() => {
        const getWithForOf = async() => {

            let {data} = await axios.get(`http://localhost:8000/api/notifications/`,{
                headers: {
                    Authorization: `${localStorage.getItem('token')}`
                }
            })
            console.log(data, "ini balikkan notifikasi")
            let arr = data.data.filter((item) => item.status == "Transaksi Dibatalkan!" || item.status == "Produk Berhasil Dijual!" )
            setIsCompleted1(...isCompleted, arr)

            console.log(arr, "kenapa arr setelah filter ga muncul ya ?");
         }
         getWithForOf();
    },[])

    useEffect(() => {
       
            const getWithForOf = async() => {
                const arrKosong = []
                for (const item of isCompleted) {
                    let {data} = await axios.get(`http://localhost:8000/api/products/${item.id_product}/detail/`,{
                            headers: {
                                Authorization: `${localStorage.getItem('token')}`
                            }
                        })
                    arrKosong.push(data)
                }
            setBarang(arrKosong)
            }
            getWithForOf();
        
        
    },[isCompleted])

    const badge = (id) => {
        console.log(id, "wawa id");
        if(isCompleted.find(item => item.id_transaksi === id && item.status === "Transaksi Dibatalkan!" )){
            return <div class="badge rounded-pill bg-danger" style={{fontSize:"10px"}}>Transaksi Batal</div>
        }
        else{
            return <div class="badge rounded-pill bg-success" style={{fontSize:"10px"}}>Transaksi Berhasil</div>
        }
    }



    return (
        <>
            <div className="col-6 col-md-4" onClick={() => navigate('/infoproduk')} style={{cursor: "pointer"}}>
                <div className="btn-add-product" >
                    <div className="btn-add-product_icon">+</div>
                    <div className="btn-add-product_text">Tambah Produk</div>
                </div>
            </div>
            {isCompleted.length !== 0 && barang.length !== 0 ? 
                <>
                    {isCompleted.map((item, index) => {
                        return <div className="col-6 col-md-4">
                                    <div class="card" 
                                    
                                    >
                                        {barang[index].images.length !== 0 ?
                                            <img src={barang[index].images[0].imageURL} class="card-img-top" alt="..." style={{
                                                width: "auto",
                                                height: "120px",
                                                objectFit: "cover",
                                            }}/>
                                            :
                                            <></>
                                        }

                                        <div class="card-body">
                                            {badge (isCompleted[index].id)}
                                            {/* <div>ID produk : {barang[index].data[0].id}</div> */}
                                            <h5 class="card-title product-title mt-2" style={{fontSize:"16px"}}>{barang[index].data[0].name}</h5>
                                            <p class="card-text product-subtitle" style={{fontSize:"12px"}}>{category[index]}</p>
                                            <p className="product-price" style={{fontSize:"16px"}}>Rp {barang[index].data[0].price}</p>
                                            <div style={{fontSize:"14px"}}>Ditawar Rp {item.bargainPrice}</div>
                                            {
                                                profil !== '' ?
                                                <>
                                                {
                                                    profil.id == item.id_buyer ? 
                                                    <div style={{fontSize:"14px"}}>Bertransaksi Dengan Anda</div>
                                                    :
                                                    <div style={{fontSize:"14px"}}>Oleh Pengguna dengan ID {item.id_buyer}</div>
                                                }
                                                </>
                                                :
                                                <></>
                                            }
                                            
                                        </div>


                                        {/* <button onClick={() => handleEdit(item.id)}>Edit Barang</button>
                                        <button onClick={() => handleHapus(item.id)}>Hapus Barang</button> */}
                                    </div>
                                </div>
                    })}
                </>
                :
                <></>

            }
        </>
    );
}

export default Wishlist;
