/* eslint-disable jsx-a11y/img-redundant-alt */
import React,{useState, useEffect} from 'react';
import { useLocation } from 'react-router-dom';
import Navbar from '../../components/Navbar';
import HeadNavbar from '../../components/HeadNavbar';
import "../../assets/css/daftar-jual.style.css"
import Product from './product';
import Wishlist from './wishlist';
import Wishlist1 from './wishlist1';
import Sold from './sold';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import WishlistLocal from './WishlistLocal';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";

import { BsBox } from 'react-icons/bs';
import { MdFavoriteBorder } from 'react-icons/md';
import { BsCurrencyDollar } from 'react-icons/bs';
import { FaUndo } from 'react-icons/fa';
import { FaRedo } from 'react-icons/fa';
import { BsChevronRight } from 'react-icons/bs';

const DaftarJual = () => {
    let { hash } = useLocation()
    console.log(hash);

    const navigate = useNavigate()

    const [login, setLogin] = useState(false)
    const [products, setProducts] = useState([])
    const [productsJual, setProductsJual] = useState([])
    const [productsJual1, setProductsJual1] = useState([])
    const [gambarJual, setGambarJual] = useState([])
    const [gambarBarang, setGambarBarang] = useState([])
    const [namaPenjual, setNamaPenjual] = useState('')
    const [kotaPenjual, setKotaPenjual] = useState('')
    const [gambarProfil, setGambarProfil] = useState('')
    const [kategori, setKategori] = useState('')

    useEffect(() => {
        async function fetchMyApi() {
            let token = localStorage.getItem('token');
            if (token) {
                setLogin(true)
            }
            let res = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                    authorization : token 
                }
            })
            if(res.data.data.id){
                console.log(res.data.data.id, "ini id harusnya yang dapet dari user");
                let {data} = await axios.get('http://localhost:8000/api/products/');
                let barangSortir = data.data.filter((item) =>  item.id_owner === res.data.data.id)
 
                setProducts(...products, barangSortir);
            }
        }
        fetchMyApi();
    }, [])

    // barang yang diminati 
    useEffect(() => {
        async function fetchMyApis() {
            let token = localStorage.getItem('token');
            if (token) {
                setLogin(true)
            }
            let res = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                    authorization : token
                }
            })
            if(res.data.data.id){
                async function fetchMyApi() {
                    let {data} = await axios.get('http://localhost:8000/api/transactions/',{
                        headers:{
                            authorization : token
                        }
                    })
                    let barangSortir = data.data.filter((item) =>  item.id_seller === res.data.data.id)
                    let barangSortir1 = data.data.filter((item) =>  item.id_buyer === res.data.data.id)
                    setProductsJual1(...productsJual1, barangSortir1)
                    setProductsJual(...productsJual, barangSortir)
                }
                fetchMyApi();
            }
        }
        fetchMyApis()
    }, [])

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {
            const arrKosong = []
            for (const product of products) {
              let gambarBarang = await axios.get(`http://localhost:8000/api/products/${product.id}/detail/`,{
                        headers: {
                            Authorization: token
                        }
                    })
              arrKosong.push(gambarBarang.data);
            }
            setGambarBarang(...gambarBarang, arrKosong)
         }
         getWithForOf();
    },[products])

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {
            const arrKosong = []
            for (const productJual of productsJual) {
              let gambarBarang = await axios.get(`http://localhost:8000/api/products/${productJual.id}/detail/`,{
                        headers: {
                            Authorization: token
                        }
                    })
              arrKosong.push(gambarBarang.data);
            }
            setGambarJual(...gambarBarang, arrKosong)
         }
         getWithForOf();
    },[productsJual])

    useEffect(() => {
        async function fetchMyApi() {
            let token = localStorage.getItem('token');
            if (token) {
                let {data} = await axios.get(`http://localhost:8000/api/users/profile`,{
                    headers: {
                        Authorization: token
                    }
                })
                console.log(data.data.cityId, "ini data data buat provinsi dan kota");
                if(data.data.cityId){
                    let res = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${data.data.cityId}`)
                    setKotaPenjual(res.data.nama)
                    // balikkan res nya coba aja paste di gugel nanti, kenapa pake nama
                }
                setNamaPenjual(data.data.username)
                setGambarProfil(data.data.profileImage_url)
            }   
        }
        fetchMyApi();
    }, [])

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {
            const arrKosong = []
            for (const product of products) {
              let {data} = await axios.get(`http://localhost:8000/api/categories/${product.id_category}`,{
                        headers: {
                            Authorization: token
                        }
                    })
              arrKosong.push(data.data.category);
            }
            setKategori(...kategori, arrKosong)
         }
         getWithForOf();
    },[products])

    return (
        <div>
            <div className='d-lg-none'>
                <HeadNavbar login={login} setLogin={setLogin} title={'Daftar Jual Saya'}/>
            </div>

            <div className='d-none d-lg-block'>
                <Navbar login={login} setLogin={setLogin}/>
                <h1 className="product-title mt-4" style={{ fontSize: "20px", marginLeft: "120px"}}>Daftar Jual Saya</h1> <br />
            </div>

            <div className="page-product">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="row">
                                <div className="col-12">
                                    <div className="card shadow" style={{ borderRadius: "16px"}}>
                                        <div className="card-body">
                                            <div className="row align-items-center">
                                                <div className="col-9 col-md-4">
                                                    <div className="row">
                                                        <div className="col-3 col-md-3">
                                                            {
                                                                gambarProfil !== '' ?
                                                                <div className="product-profile">
                                                                    <img src={gambarProfil} className="profile-img" alt="Image Person" style={{width: "48px", height:"48px", borderRadius: "12px"}} />
                                                                </div>
                                                                :
                                                                <></>
                                                            }
                                                        </div>
                                                        <div className="col-9 col-md-9">
                                                            <div className="product-profile_user">
                                                                {
                                                                    namaPenjual !== '' && kotaPenjual !== '' ?
                                                                    <>
                                                                        <h1 className="profile-name" style={{ fontSize: "16px "}}>{namaPenjual}</h1>
                                                                        <p className="profile-city text-muted" style={{ fontSize: "12px "}}>{kotaPenjual}</p>
                                                                    </>
                                                                    :
                                                                    <>
                                                                        sedang mengambil info penjual
                                                                    </>
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-3 col-md-8">
                                                    <div className="d-flex w-100 justify-content-end">
                                                        <button className="btn btn-edit rounded-pill px-3" onClick={() => navigate('/infoprofil')} style={{outlineStyle: "solid", outlineColor: "#7126B5", outlineWidth: "1px", fontSize: "12px" }}>Edit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="row mt-5">
                                <div className="col-12 col-md-3">
                                    <div className="card shadow d-none d-md-block" style={{ borderRadius: "16px"}}>
                                        <div className="card-body">
                                            <div className="product-filter-title ms-3 fw-bold">
                                                Kategori
                                            </div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item mt-2">
                                                    <a href="#product" class="btn-category"> <BsBox /> Semua Produk <BsChevronRight style={{float: "right"}}/></a>
                                                </li>
                                                <li class="list-group-item mt-2">
                                                    <a href="#wishlist" class="btn-category"><FaUndo /> Barang Dinego <BsChevronRight style={{float: "right"}}/></a>
                                                </li>
                                                <li class="list-group-item mt-2">
                                                    <a href="#wishlist1" class="btn-category"><FaRedo /> Nego Barang <BsChevronRight style={{float: "right"}}/></a>
                                                </li>
                                                <li class="list-group-item mt-2">
                                                    <a href="#wishlistlocal" class="btn-category"><MdFavoriteBorder /> Wishlist Barang <BsChevronRight style={{float: "right"}}/></a>
                                                </li>
                                                <li class="list-group-item mt-2">
                                                    <a href="#sold" class="btn-category"><BsCurrencyDollar /> Transaksi Selesai <BsChevronRight style={{float: "right"}}/></a>
                                                </li>
                                                
                                            </ul>
                                            
                                        </div>
                                    </div>
                                    <div class="d-block d-md-none" style={{ paddingBottom: "20px", marginTop: "-20px"}}>
                                        <Swiper
                                        slidesPerView={"auto"}
                                        spaceBetween={15}
                                        // pagination={{
                                        //   clickable: true,
                                        // }}
                                        grabCursor={true}
                                        modules={[Pagination]}
                                        className="mySwiper pou"
                                        >
                                        <SwiperSlide className="pou">
                                            <a href="#product" class="btn btnClass d-flex justify-content-center px-3 py-2"> <BsBox style={{marginTop: "4px", marginRight: "4px"}}/> Produk</a>
                                        </SwiperSlide>
                                        <SwiperSlide className="pou">
                                            <a href="#wishlist" class="btn btnClass d-flex justify-content-center px-3 py-2"><FaUndo style={{marginTop: "4px", marginRight: "4px"}}/> Dinego</a>
                                        </SwiperSlide>
                                        <SwiperSlide className=" pou">
                                            <a href="#wishlist1" class="btn btnClass d-flex justify-content-center px-3 py-2"><FaRedo style={{marginTop: "4px", marginRight: "4px"}}/> Nego</a>
                                        </SwiperSlide>
                                        <SwiperSlide className="pou">
                                            <a href="#wishlistlocal" class="btn btnClass d-flex justify-content-center px-3 py-2" ><MdFavoriteBorder style={{marginTop: "4px", marginRight: "4px"}}/> Wishlist</a>
                                        </SwiperSlide>
                                        <SwiperSlide className="pou">
                                            <a href="#sold" class="btn btnClass d-flex justify-content-center px-3 py-2" ><BsCurrencyDollar style={{marginTop: "4px", marginRight: "4px"}}/> Terjual</a>
                                        </SwiperSlide>
                                        </Swiper>
                                    </div>
                                </div>


                                <div className="col-12 col-md-9">
                                    <div className="row g-3">
                                        {
                                            // DITAHAN DULU NANTI GA KERENDER !
                                            productsJual !== null && productsJual1 !== null ?
                                            <>
                                                {hash === "#product" ? <Product products={products} category={kategori} productsJual={productsJual}  gambarBarang={gambarBarang}/> : <></>}
                                                {hash === "#wishlist" ? <Wishlist products={products} category={kategori} productsJual={productsJual} gambarJual={gambarJual}/> : <></>}
                                                {hash === "#wishlist1" ? <Wishlist1 category={kategori} productsJual={productsJual1}/> : <></>}
                                                {hash === "#wishlistlocal" ? <WishlistLocal category={kategori}/> : <></>}
                                                {hash === "#sold" ? <Sold category={kategori} productsJual={productsJual} gambarJual={gambarJual}/> : <></>}
                                            </>
                                            :
                                            <>
                                            </>
                                        }
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default DaftarJual;