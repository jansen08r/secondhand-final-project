import React, {useState, useEffect} from "react";
import axios from 'axios';
import "../../assets/css/daftar-jual.style.css"
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';

import { useNavigate } from 'react-router-dom';

const Product = (props) => {

    const {products, gambarBarang, productsJual, category} = props
    const navigate = useNavigate()
    console.log(products, 'ini products')
    console.log(gambarBarang, "ini gambarbarnga");
    // console.log(productsJual, 'ini products jual')

    const handleHapus = async(id) => {
        try{
            let token = localStorage.getItem('token');
            let {data} = await axios.delete(`http://localhost:8000/api/products/${id}/delete`,{
                headers:{
                    Authorization: token
                }
            });
            // alert('barang berhasil dihapus')
            toast.success('Barang berhasil dihapus', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                });
            window.location.reload(); 
        }
        catch(err){
            toast.success('Barang berhasil dihapus', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                });
            // window.location.reload(); 
        }
    }
    
    const handleEdit = async(id) => {
        navigate(`editproduk/${id}`)
    }

    return (
        <>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <div className="col-6 col-md-4" onClick={() => navigate('/infoproduk')} style={{cursor: "pointer"}}>
                <div className="btn-add-product" >
                    <div className="btn-add-product_icon">+</div>
                    <div className="btn-add-product_text">Tambah Produk</div>
                </div>
            </div>
            
            {products.length !== 0 &&  gambarBarang.length !== 0 ? 
                <>
                    {products.map((item, index) => {

                        return  <>
                        {
                            item.isSold !== true ?
                            <>
                                            {gambarBarang[index].images.length !== 0 ?
                                                <>
                                                <div className="col-6 col-md-4">
                                                    <div class="card">
                                                    <img src={gambarBarang[index].images[0].imageURL} class="card-img-top" alt="..."  style={{
                                                        width: "auto",
                                                        height: "120px",
                                                        objectFit: "cover",
                                                    }}/>
                                                    <div class="card-body">
                                                        <h5 class="card-title product-title" style={{fontSize: "16px"}}>{item.name}</h5>
                                                        <p class="card-text product-subtitle" style={{fontSize: "12px"}}>{category[index]}</p>
                                                        <p className="product-price" style={{fontSize: "16px"}}>Rp {item.price}</p>
                                                    </div>
                                                    <div class="btn-group-vertical px-2 py-3" style={{marginTop: "-25px"}}>
                                                        <button class="btn-edit w-100 rounded-pill" onClick={() => handleEdit(item.id)} style={{outlineStyle: "solid", outlineColor: "#7126B5", outlineWidth: "1px", borderRadius: "16px", border: "none", marginRight: "10px", fontSize: "14px"}}>Edit Barang</button>
                                                        <button class="btn-delete w-100 mt-2 rounded-pill"onClick={() => handleHapus(item.id)} style={{ backgroundColor: "#F94449", borderRadius: "16px", border: "none", fontSize: "14px"}}>Hapus Barang</button>
                                                    </div>

                                                    </div>
                                                 </div>
                                                </>
                                            
                                                :
                                                <></>
                                            }

                                        
                                        
                                    
                            </>
                            :
                            <></>
                        }
                                
                        </>
                        
                    })}
                    
                </>
                :
                <></>

            }
        </>
    );
}

export default Product;
