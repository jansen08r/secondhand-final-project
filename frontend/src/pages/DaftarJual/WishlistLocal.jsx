import axios from 'axios';
import React, { useEffect , useState} from 'react'
import { useNavigate } from 'react-router-dom';

const WishlistLocal = (props) => {

    const navigate = useNavigate();
    const { category } = props;
    // let wishlist = JSON.parse(localStorage.getItem('wishlist'));
    // console.log(wishlist);

    // const myBlogs = [...wishlist, {tes:"datax"}];
    // localStorage.setItem('wishlist', JSON.stringify(myBlogs))

    const [barang, setBarang] = useState([])
    const [barang1, setBarang1] = useState([])
    console.log(barang, "barang ---------");
    // console.log(barang1, "barang1 +++++++++");

    const [wishlist, setWishlist] = useState([])
    // console.log(barang, 'ini barang di wishlist local');

    const handleHapusWishlist = async (id) => {
        // let wishlist = JSON.parse(localStorage.getItem('wishlist'));
        // let arrKosong = []
        // wishlist.filter((item,index) => item == id)

        let filter0 = barang.filter((item) => item.data[0].id != id)
        // let arrKosong = [...barang];
        // arrKosong.pop() // DIGANTI DULU BIAR ILANG, JANGAN PAKE POP, TERUS LOCAL STORAGE NYA JUGA JANGAN LUPA DIUPDATE 
        // setBarang(arrKosong)
        setBarang(filter0)

        let {data} = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                  Authorization : `${localStorage.getItem("token")}`
                }
              })
        
        let wilis = JSON.parse(localStorage.getItem(`wishlist${data.data.id}`));
        console.log(wilis, "wilis di delete");

        let wilisFilter = wilis.filter((item) => item != id)

        localStorage.removeItem(`wishlist${data.data.id}`)
        localStorage.setItem(`wishlist${data.data.id}`, JSON.stringify(wilisFilter))

        // let filter1 = filter0.filter((item) => item.data[0].id == )
        // filter.map((item) => )  
        
    }

    useEffect(() => {
        async function fetchMyApi(){
            let {data} = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                  Authorization : `${localStorage.getItem("token")}`
                }
              })

            let wilis = JSON.parse(localStorage.getItem(`wishlist${data.data.id}`));
            setWishlist(wilis)
            let arrKosong = []

            for (const wil of wilis) {
                let dataUser = await axios.get(`http://localhost:8000/api/products/${wil}/detail/`,{
                        headers: {
                            Authorization: `${localStorage.getItem('token')}`
                        }
                    })
                    arrKosong.push(dataUser.data);
            }
            setBarang(...barang, arrKosong)
        }

        fetchMyApi()
    },[])   

    // useEffect(() => {
    //     async function fetchMyApi(){
    //         let arrKosong = []
    //         for (const wish of wishlist) {
    //             let dataUser = await axios.get(`http://localhost:8000/api/products/${wish}/detail/`,{
    //                     headers: {
    //                         Authorization: `${localStorage.getItem('token')}`
    //                     }
    //                 })
    //                 arrKosong.push(dataUser.data);
    //         }
    //         setBarang1(arrKosong)
    //     }

    //     fetchMyApi()
    // }, [wishlist])

    return (
        <>
            <div className="col-6 col-md-4" onClick={() => navigate('/')} style={{cursor: "pointer"}}>
                <div className="btn-add-product" >
                    <div className="btn-add-product_icon">+</div>
                    <div className="btn-add-product_text">Cari Produk di Dashboard</div>
                </div>
            </div>
            
            {barang.length !== 0  ? 
                <>
                    {barang.map((item, index) => {
                        return <div className="col-6 col-md-4">
                                    <div class="card" 
                                    // onClick={() => navigate(`/detailproduk/${item.data[0].id}`) }

                                    // onClick={() => {handleClick(item.data[0].id, productsJual[index].id); handleClick1(productsJual[index].id);}} 
                                    // className={productsJual[index].isBargainAccepted === true ? 'background-green' : ''}
                                    >
                                        {barang[index].images.length !== 0 ?
                                            <img src={barang[index].images[0].imageURL} class="card-img-top" alt="..." style={{
                                                width: "auto",
                                                height: "120px",
                                                objectFit: "cover",
                                            }}/>
                                            :
                                            <></>
                                        }
                                        <div class="card-body">
                                            {/* <div>Transaksi ID : {item.data[0].id}</div> */}
                                            {/* <div>ID produk : {item.data[0].id}</div> */}
                                            <h5 class="card-title product-title" style={{fontSize:"16px"}}>{item.data[0].name}</h5>
                                            <p class="card-text product-subtitle" style={{fontSize:"12px"}}>{category[index]}</p>
                                            <p className="product-price" style={{fontSize:"16px"}}>Rp {item.data[0].price}</p>
                                            <button class="w-100 rounded-pill" onClick={() => handleHapusWishlist(item.data[0].id)}style={{ backgroundColor: "#F94449", color: "white", borderRadius: "16px", border: "none", fontSize: "14px"}}>Hapus Dari Wishlist</button>
                                            {/* <div>Ditawar Rp {productsJual[index].bargainPrice}</div> */}
                                            {/* <div>Oleh Pengguna dengan ID {productsJual[index].id_buyer}</div> */}
                                        </div>
                                        {/* <button onClick={() => handleEdit(item.id)}>Edit Barang</button>
                                        <button onClick={() => handleHapus(item.id)}>Hapus Barang</button> */}
                                    </div>
                                </div>
                    })}
                </>
                :
                <></>

            }
        </>
        
    )
}

export default WishlistLocal