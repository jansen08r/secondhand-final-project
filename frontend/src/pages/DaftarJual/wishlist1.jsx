import React,{useEffect, useState} from 'react';
import imagempty from '../../images/empty.png'
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './wishlist1.css'


const Wishlist1 = (props) => {

    const {productsJual, gambarJual, category} = props
    console.log(productsJual, "ini products di wishlist1 +++++++++++++++++++") // kok kosong ya
    // console.log(gambarJual, "ini gambar jual");

    const navigate = useNavigate()

    let ditolak = productsJual.filter((item) => item.sellerResponse === true && item.isCompleted !== true ) // && item.isBargainAccepted !== true , gatau deng yang bener harusnya gimana 
    console.log(ditolak, "ditolaaaaak");

    const [namaPenjual, setNamaPenjual] = useState('')
    const [tawar, setTawar] = useState('')
    const [wishlist, setWishlist] = useState([])
    const [barang, setBarang] = useState([])
    console.log(barang, "ini barang di wishlist1")
    console.log(productsJual, "ini productsJual di wishlist1");

    const handleTawar = (e) => {
        setTawar(e.target.value)
        // console.log(e.target.value);
    }

    const handleSubmit = async(idTransaksi) => {
        
        const bargainPrice = {
            bargainPrice : tawar
        }

        let {data} = axios.put(`http://localhost:8000/api/transactions/${idTransaksi}/update/buyer`,bargainPrice,{
            headers:{
                Authorization: `${localStorage.getItem('token')}`
            }
        })
        alert('Harga sudah diupdate')
        
    }

    useEffect(() => {
        async function fetchMyApi() {
            let token = localStorage.getItem('token');
            let {data} = axios.get('http://localhost:8000/api/transactions/',{
                headers:{
                    Authorization: token
                }
            })
        }
        fetchMyApi()
    },[])

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {

            const arrKosong = []
            for (const productJual of productsJual) {
                // ini yang belum selesai 
                if(productJual.isCompleted === false){
                    let dataUser = await axios.get(`http://localhost:8000/api/products/${productJual.id_product}/detail/`,{
                        headers: {
                            Authorization: token
                        }
                    })
                    arrKosong.push(dataUser.data);
                }
            }
            setBarang(...barang, arrKosong)
         }
         getWithForOf();
    },[productsJual])

    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {

            const arrKosong = []
            for (const productJual of productsJual) {
                let dataUser = await axios.get(`http://localhost:8000/api/users/${productJual.id_seller}/profile`,{
                    headers: {
                        Authorization: token
                    }
                })
                arrKosong.push(dataUser.data);
            }
            setNamaPenjual(...namaPenjual, arrKosong)
         }
         getWithForOf();
    },[productsJual])

    return (
        <>
            <div className="col-6 col-md-4" onClick={() => navigate('/')} style={{cursor: "pointer"}}>
                <div className="btn-add-product" >
                    <div className="btn-add-product_icon">+</div>
                    <div className="btn-add-product_text">Cari Produk di Dashboard</div>
                </div>
            </div>
            {barang.length !== 0 ? 
                <>
                    {barang.map((item, index) => {
                        return <div className="col-6 col-md-4">
                                    <div class="card">
                                        {barang.length !== 0 && barang[index].images.length !== 0 ?
                                            <img src={barang[index].images[0].imageURL} class="card-img-top" alt="..." style={{
                                                width: "auto",
                                                height: "120px",
                                                objectFit: "cover",
                                            }}/>
                                            :
                                            <></>
                                        }
                                        <div class="card-body">
                                            {
                                                productsJual[index].isBargainAccepted === true ?
                                                <>
                                                    <span class="badge rounded-pill bg-success" style={{fontSize: "10px"}}>Diterima Oleh Penjual</span>
                                                    <div class="mt-2" style={{fontSize: "14px"}}>Kamu akan segera dihubungi penjual via whatsapp</div>
                                                </>
                                                :
                                                <>
                                                        <button type="button" class="btn btn-success w-100 rounded-pill" data-bs-toggle="modal" data-bs-target="#exampleModal" style={{fontSize: "14px"}}>Update Harga</button>
                                                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content" style={{borderRadius: "16px"}}>
                                                                <div class="modal-header border-0">
                                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                </div>
                                                                <div class="modal-body" style={{marginTop: "-20px"}}>
                                                                    <h6 style={{fontSize: "14px"}}>Silahkan ubah harga tawaranmu!</h6>
                                                                    <label for="exampleInputEmail1" class="form-label" style={{fontSize: "14px", marginBottom: "10px"}}>Harga Tawar</label>
                                                                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Rp.' onChange={(e) => handleTawar(e)} style={{fontSize: "14px"}}/>
                                                                </div>
                                                                <div class="modal-footer border-0">
                                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" style={{ borderRadius: "14px", border: "none", fontSize: "14px"}}>Tutup</button>
                                                                    <button type="button" class="btn btn-success rounded-pill" onClick={() => handleSubmit(productsJual[index].id)} style={{fontSize: "14px"}}>Update Harga</button>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                </>
                                            }
                                            {
                                                productsJual[index].isBargainAccepted !== true &&  productsJual[index].sellerResponse === true ?
                                                <>
                                                    <div class="badge rounded-pill bg-danger" style={{fontSize: "10px"}}>Ditolak oleh Penjual</div>
                                                    
                                                    <button type="button" class="btn btn-success w-100 rounded-pill" data-bs-toggle="modal" data-bs-target="#exampleModal" style={{fontSize: "14px"}}>Update Harga</button>
                                                    
                                                    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <label for="exampleInputEmail1" class="form-label"><b>Harga Tawar</b></label>
                                                                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Rp.' onChange={(e) => handleTawar(e)}/>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                                <button type="button" class="btn btn-success rounded-pill" onClick={() => handleSubmit(productsJual[index].id)} style={{fontSize: "14px"}}>Update Harga</button>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                :
                                                <></>
                                            }
                                            <div class="mt-2">
                                                <h5 class="card-title product-title" style={{fontSize: "16px"}}>{item.data[0].name}</h5>
                                                <p class="card-text product-subtitle" style={{fontSize: "12px"}}>{category[index]}</p>
                                                <p className="product-price" style={{fontSize: "16px"}}>Rp {item.data[0].price}</p>
                                                <div style={{fontSize: "14px"}}>Kamu tawar senilai Rp. {productsJual[index].bargainPrice}</div>
                                                <div style={{fontSize: "14px"}}>Dijual oleh {namaPenjual[index].data.username}</div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                    })}
                    
                </>
                :
                <></>

            }
            {/* <div>Ditolak</div> */}
            
        </>
    );
}

export default Wishlist1;
