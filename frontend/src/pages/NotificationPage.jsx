import React, {useState, useEffect} from 'react';
import { Link, useNavigate } from "react-router-dom";
import HeadNavbar from '../../src/components/HeadNavbar';
import axios from 'axios';
import moment from 'moment'

import logoKotak from '../images/logoKotak.png';
import fi_arrowleft from '../images/fi_arrowleft.png';

import { IoIosLogIn } from 'react-icons/io';
import { IoIosNotificationsOutline } from 'react-icons/io';
import { IoIosList } from 'react-icons/io';
import { FiUser } from 'react-icons/fi';
import { FcInfo } from "react-icons/fc";

import io from 'socket.io-client';

const socket = io.connect('http://localhost:8000')

const Notification = () => {

    const navigate = useNavigate();
    // const { handleSearch, login, setLogin} = props;

    const [transactions, setTransactions] = useState([])
    const [barang, setBarang] = useState([])
    const [idUser, setIdUser] = useState('')
    const [notif, setNotif] = useState([])
    const [notif1, setNotif1] = useState('')
    const [ login, setLogin ] = useState('')
    const [barangNotif, setBarangNotif] = useState('')
    const [penjualBarangNotif, setPenjualBarangNotif] = useState('')
    const [messages, setMessages] = useState([])
 
    useEffect(() => {
        socket.on("notification", (data) => {
            setMessages([...messages, data]);
            console.log(data, "WAWA ++++++");
        })
    }, [socket, messages]);
    
    
    console.log(transactions, "ini transactions")
    console.log(notif1, "ini notif1 di navbar !");
    console.log(barang, "ini barang");

    const handleLogout = () => {
        localStorage.removeItem("token")
        setLogin(false)
        navigate('/')
    }

    useEffect(() => {
        async function fetchMyApi(){
            let token = localStorage.getItem('token');
            let {data} = await axios.get(`http://localhost:8000/api/transactions/`,{
                headers: {
                    Authorization: token
                }
            })

            setTransactions(...transactions, data.data)

            let res = await axios.get(`http://localhost:8000/api/notifications/`,{
                headers: {
                    Authorization: token
                }
            })

            console.log(res.data.data,"=============");
            setNotif(...notif, res.data.data)
        }
        fetchMyApi()
    },[])

    useEffect(() => {
            let arrSementara = transactions.map((i) => notif.filter((item) => item.id_transaksi === i.id ))
            console.log(arrSementara,"AKAAAAAAAAAAA");
            setNotif1(arrSementara)
    },[transactions, notif])
    
    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {
           

            const arrKosong = []
            for (const transaction of transactions) {
                let dataUser = await axios.get(`http://localhost:8000/api/products/${transaction.id_product}/detail/`,{
                        headers: {
                            Authorization: token
                        }
                    })
                arrKosong.push(dataUser.data);
            }
            setBarang(...barang, arrKosong)
         }
         getWithForOf();
    },[transactions])

    useEffect(() => {
        const fetchMyApi = async() => {
            let token = localStorage.getItem('token');
            let {data} = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                    Authorization: token
                }
            })
            setIdUser(data.data.id)
            setLogin(true)
        }
        fetchMyApi()
    }, [])

    useEffect(() => {
        const getWithForOf = async() => {

            const arrKosong = []
            const arrKosong1 = []
            for (const message of messages) {
                let barangUser = await axios.get(`http://localhost:8000/api/products/${message.id_product}/detail/`,{
                        headers: {
                            Authorization: `${localStorage.getItem('token')}`
                        }
                    }) 
                arrKosong.push(barangUser.data);
                let dataUser = await axios.get(`http://localhost:8000/api/users/${message.id_seller}/profile`,{
                    headers: {
                        Authorization: `${localStorage.getItem('token')}`
                    }
                })
                arrKosong1.push(dataUser.data.data)
            }
            setPenjualBarangNotif(...penjualBarangNotif, arrKosong1)
            setBarangNotif(...barangNotif, arrKosong)
         }
         getWithForOf();
    }, [messages])
    

    console.log(messages, "++!!!");
    console.log(penjualBarangNotif, "+++++++++[[[[[[");
    console.log(barangNotif, "-------------[[[[[");

    return (
        <>
            
            <nav class="navbar navbar-expand-md py-0  navbar-light ">
                <div class="container-fluid">
                    <a class="navbar-brand d-none d-md-block" onClick={() => navigate('/')} style={{ cursor: "pointer" }}>SecondHand</a>
                    <div className="d-flex w-100 justify-content-between">
                            {/* gabisa w-md-50, jadi trick nya dibuat dalam satu col-md-6, dalemnya w-100, kekurung sama col-6 gt maksudnya, kalo ms-auto supaya ke kanan */}
                                <div class="d-flex w-100 justify-content-between py-4 py-md-3">
                                    <button class="navbar-toggler bg-light border-radius-13px p-2 me-3" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample" >
                                        <span class="navbar-toggler-icon"></span>
                                    </button>
                                    <div className="col-md-6 col d-flex align-items-center">
                                        <h3>Notifikasi</h3>
                                    </div>
                                    {login ? 
                                    <>
                                    {/* Notification */}

                                        <div id='btn-group' className='btn-group'>
                                            <button class="btn button btn-list" type="button" onClick={() => navigate('/product-sell')}>
                                                <IoIosList size={28}/>
                                            </button>
                                            <div class="dropdown">
                                                <button className='btn button btn-notification dropdown-toggle' type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <IoIosNotificationsOutline size={28}/>
                                                </button>
                                                {
                                                    messages.length !== 0 && penjualBarangNotif.length !== 0 && barangNotif.length !== 0 ? 
                                                    <>
                                                            {messages.map((item, index) => {
                                                                return  <div class="dropdown-menu container py-2" onClick={() => navigate('/product-sell')}>
                                                                                <div className='row'>
                                                                                    <div className="col-12 pb-3">
                                                                                        Status : {item.status}
                                                                                    </div>
                                                                                    <div className='col-4 d-flex justify-content-center align-items-center' >
                                                                                    {
                                                                                        barangNotif[index].images.length !== 0 ?
                                                                                        <img src={barangNotif[index].images[0].imageURL} alt="" style={{width: "48px", height: "48px", borderRadius: "16px", marginTop: "8px"}}/>
                                                                                        :
                                                                                        <></>
                                                                                    }
                                                                                    </div>
                                                                                    <div className='col-8'>
                                                                                        <div class="text-muted" style={{fontSize: "10px"}}>{moment(item.createdAt).format("dddd, MMMM Do YYYY, h:mm:ss a")}</div>
                                                                                        {idUser !== '' ? 
                                                                                        <>
                                                                                            {item.id_seller === idUser ? 
                                                                                                <h6 style={{fontSize: "16px"}}>
                                                                                                    Barang Milik Anda
                                                                                                </h6>
                                                                                                :
                                                                                                <h6 style={{fontSize: "16px"}}>
                                                                                                    Barang Milik {penjualBarangNotif[index].username}
                                                                                                </h6>
                                                                                            }
                                                                                        </>
                                                                                        :
                                                                                        <></>
                                                                                        }
                                                                                        <div class="fw-normal" style={{fontSize:"14px"}}>{barangNotif[index].data[0].name}</div>
                                                                                        <div class="fw-normal" style={{fontSize:"14px"}}>Rp {barangNotif[index].data[0].price}</div>

                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                                <hr />
                                                                            </div>
                                                                        
                                                            })}

                                                    </>
                                                    :
                                                    <>
                                                        
                                                    </>
                                                }
                                                                                </div>
                                            <button class="btn button btn-user" type="button" onClick={() => navigate('/informasiakun')} style={{ marginRight: "20px" }}>
                                                <FiUser size={28}/>
                                            </button>
                                        </div>
                                    </>
                                        
                                        :
                                        <button class="btn btnClass rounded-pill d-none d-md-block" style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px" }} type="button" onClick={() => navigate('/login')}> <IoIosLogIn /> Masuk</button>
                                    }
                                </div>
                                <div class="offcanvas offcanvas-start  d-md-none w-50" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                                    <div class="offcanvas-header">
                                        <h5 id="offcanvasTopLabel" onClick={() => navigate('/')}>SecondHand</h5>
                                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                    </div>
                                    <div class="offcanvas-body d-md-none">
                                        {login ?
                                        <>
                                            <div className='pb-3' onClick={() => navigate('/notifikasi')}>Notifikasi</div>
                                            <div className='pb-3' onClick={() => navigate('/product-sell')}>Daftar Jual</div>
                                            <div onClick={() => navigate('/informasiakun')} className='pb-3'>Akun Saya</div>
                                            <div onClick={() => handleLogout()} className='pb-3'>
                                                Logout
                                            </div> 
                                        </>
                                        :
                                        <div onClick={() => navigate('/login')}>
                                            Login 
                                        </div>
                                        }
                                    </div>
                                </div>
                    </div>

                </div>
            </nav>
            <>
            {
                messages.length !== 0 && penjualBarangNotif.length !== 0 && barangNotif.length !== 0 ? 
                <>
                        {messages.map((item, index) => {
                            return  <div class="container py-2" onClick={() => navigate('/product-sell')}>
                                            <div className='row'>
                                                <div className="col-8 pb-3">
                                                    <div className=''>Status : {item.status} </div>
                                                </div>
                                                <div className="col-3">
                                                    <FcInfo size={22}/> 
                                                </div>
                                                
                                                <div className='col-4 d-flex justify-content-center align-items-center' >
                                                {
                                                    barangNotif[index].images.length !== 0 ?
                                                    <img src={barangNotif[index].images[0].imageURL} alt="" style={{width: "48px", height: "48px", borderRadius: "16px", marginTop: "8px"}}/>
                                                    :
                                                    <></>
                                                }
                                                </div>
                                                <div className='col-8'>
                                                    <div class="text-muted" style={{fontSize: "10px"}}>{moment(item.createdAt).format("dddd, MMMM Do YYYY, h:mm:ss a")}</div>
                                                    
                                                    <h6 class="fw-normal" style={{fontSize:"14px"}}>{barangNotif[index].data[0].name}</h6>
                                                    <h6 class="fw-normal" style={{fontSize:"14px"}}>Rp {barangNotif[index].data[0].price}</h6>
                                                    {idUser !== '' ? 
                                                    <>
                                                        {item.id_seller === idUser ? 
                                                            <div style={{fontSize: "16px"}}>
                                                                Barang Milik Anda
                                                            </div>
                                                            :
                                                            <div style={{fontSize: "16px"}}>
                                                                Barang Milik {penjualBarangNotif[index].username}
                                                            </div>
                                                        }
                                                    </>
                                                    :
                                                    <></>
                                                    }
                                                    
                                                </div>
                                            </div>
                                            <hr />
                                        </div>
                                    
                        })}

                </>
                :
                <>
                    
                </>
            }
            </>
        </>
    );
    



};

export default Notification;
