import React, {useState, useEffect} from 'react';
import loginAtauRegister from '../images/loginAtauRegister.png';
import "./styles.css"
import { Link, useNavigate } from "react-router-dom";
import { registerService } from '../services/loginRegister';
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';

import fi_eye from '../images/fi_eye.png'
import fi_arrowleft from '../images/fi_arrowleft.png'


const RegisterPage = () => {
    const navigate = useNavigate();

    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [login, setLogin] = useState(false)
    const [mata, setMata] = useState(false)

    const handleNameInput = (event) => {
        setName(event.target.value);
        console.log(event.target.value);
    }

    const handleEmailInput = (event) => {
        setEmail(event.target.value)
        console.log(event.target.value);
    }

    const handlePasswordInput = (event) => {
        setPassword(event.target.value)
        console.log(event.target.value);
    }

    const handleMata = () => {
        mata ? setMata(false) : setMata(true)
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        let registerData = {
            username: name,
            email: email,
            password: password
        }

        registerService(registerData).then((res) => {
            // localStorage.setItem('token', res.data.token)
            // setLogin(true)
            toast.success('Pendaftaran akun berhasil!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            navigate('/login')
        }).catch((err) => {
            console.log(err);
        })
    }

    useEffect(() => {
        let token = localStorage.getItem('token')
        if(token){
            navigate('/')
        }
    },[])

    return (
        <div>
            <div className="container-fluid">
                <div className="row">
                    <div className='col-md-6 d-none d-sm-block px-0 mx-0'>
                            <img src={loginAtauRegister} className="" alt="loginAtauRegister" style={{width : '100%', height:'100vh', objectFit: "cover"}} />
                    </div>
                    <div className='col-md-6 col-12 px-0 mx-0 d-flex justify-content-center align-items-md-center min-vh-100  coba-Rel'>
                        <div className="w-75">
                            <button onClick={() => navigate(-1)} className='coba-Abs1 d-block d-md-none border-0 px-0'>
                                <img src={fi_arrowleft} alt=""/>
                            </button>
                            <h1>Daftar</h1>
                            <form >
                                <div class="mb-3">
                                    <label for="exampleInputName1" class="form-label" >Nama</label>
                                    <input type="text" class="form-control" id="exampleInputName1" placeholder="Nama Lengkap" onChange={(e) => handleNameInput(e)}/>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputEmail1" class="form-label" >Email</label>
                                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Contoh: johndee@gmail.com" onChange={(e) => handleEmailInput(e)}/>
                                </div>
                                <div class="mb-3">
                                    <label for="exampleInputPassword1" class="form-label" >Password</label>
                                    <div className="position-relative">
                                        <img src={fi_eye} alt="" className='position-absolute top-50 end-0 translate-middle-y pe-3' onClick={() => handleMata()}/>
                                        {mata ? 
                                        <input type='text' class="form-control" id="exampleInputPassword1" placeholder="Masukkan password" onChange={(e) => handlePasswordInput(e)} />
                                        : 
                                        <input type='password' class="form-control" id="exampleInputPassword1" placeholder="Masukkan password" onChange={(e) => handlePasswordInput(e)} />
                                    }
                                    </div>
                                </div>
                                    <button type="submit" class="btn btn-primary w-100 border-0"  onClick={(e) => handleSubmit(e)} style={{backgroundColor: '#7126B5', color:'white', borderRadius: "16px"}}>Daftar</button>
                                </form>
                                <div className='coba-Abs mb-4 mb-md-0 w-100'>
                                    <div className='mt-4 text-center'>
                                        Sudah punya akun? <Link to="/login" style={{textDecoration: "none", color: "#7126B5"}}><b>Masuk di sini</b></Link>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    );
}

export default RegisterPage;
