import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate, useParams } from 'react-router-dom';
import axios from 'axios'
import fi_arrowleft from '../images/fi_arrowleft.png';
import imagePerson from '../images/person.png'
import gambarJam1 from '../images/gambarJam1.png';
import logoKotak from '../images/logoKotak.png';

import '../assets/css/info-penawar.style.css'
import ModalInfoPenawar from '../components/Modal/ModalInfoPenawar';

const InfoPenawar = () => {

    // const [nungguProduct, setNungguProduct] = useState()
    const [namaPembeli, setNamaPembeli] = useState('')
    const [kotaPembeli, setKotaPembeli] = useState('')
    const [gambarProfil, setGambarProfil] = useState('')
    const [idPembeli, setIdPembeli] = useState('')
    const [nomor, setNomor] = useState('')
    const [barangClick, setBarangClick ] = useState('')
    const [barangFilter, setBarangFilter] = useState('') 

    const productDetail = useSelector((state) => state.productDetail); 
    const ClickTransactions = useSelector((state) => state.ClickTransactions)

    const navigate = useNavigate()
    const {id} = useParams()

    useEffect(() => {
        async function fetchMyApi() {
        let token = localStorage.getItem('token')
        
        if(ClickTransactions.length !== 0){
                let {data} = await axios.get(`http://localhost:8000/api/users/${ClickTransactions[0].data.transaksi.id_buyer}/profile`,{
                headers:{
                    Authorization: token
                }
            })

            setIdPembeli(data.data.id)
            setNamaPembeli(data.data.username);
            setGambarProfil(data.data.profileImage_url)
            setNomor(data.data.phoneNumber)
            
            if(data.data.cityId){
                let res = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${data.data.cityId}`)
                setKotaPembeli(res.data.nama)
            }
        }

        }
        fetchMyApi()

    }, [ClickTransactions])

    return (
        <div class='container-fluid px-0'>
            <nav class="navbar navbar-expand d-none d-md-block navbar-light bg-light shadow-sm">
                    <div className="d-flex w-100 justify-content-between">
                        <Link to='/' style={{textDecoration: "none", color: "purple"}}> 
                                <img src={logoKotak} alt="" className='ps-5 d-none d-md-block'/>
                        </Link> 
                        <div class=" position-absolute top-50 start-50 translate-middle d-none d-md-block h6" >
                                Info Penawar
                        </div>
                    </div>
            </nav>

            <div className='mt-5' style={{marginLeft: "15px", overflow: "hidden"}}>
                <div className='row'>
                    <div className='col-4'>
                    <Link to='/product-sell#wishlist'> 
                        <img src={fi_arrowleft} alt="" />
                    </Link>
                    </div>
                    <div className='col-8'>
                    <div class="h6 d-block d-md-none" >
                        Info Penawar
                    </div>
                    </div>
                </div>
            </div>
            
            <div className='d-flex justify-content-center'>
                <form class="w-70 mb-5 pb-5 px-3">
                    <div className=''>
                        <div class="content-page">
                                    <div className="card mt-3" style={{ borderRadius: "16px"}}>
                                        <div className="card-body">
                                            <div className="row">
                                                <div className="col-2">
                                                    {
                                                        gambarProfil !== '' ?
                                                        <div className="product-profile">
                                                            <img src={gambarProfil} className="profile-img" alt="" style={{width: "48px", height:"48px", borderRadius: "12px"}} />
                                                        </div>
                                                        :
                                                        <></>
                                                        }
                                                </div>
                                                <div className="col-6" style={{marginLeft: "10px"}}>
                                                    {
                                                        namaPembeli !== '' && kotaPembeli !== '' ?
                                                        <>
                                                            <h1 style={{ fontSize: "16px "}}>{namaPembeli}</h1>
                                                            <p className="profile-city text-muted" style={{ fontSize: "12px "}}>{kotaPembeli}</p>
                                                        </>
                                                        :
                                                        <></>
                                                    }
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mt-4">
                                        <h6>Daftar Produkmu yang Ditawar</h6>
                                    </div>
                                    <div className='mt-3'>
                                        <div class="detail-card">
                                            <div class="row">
                                                <div class="col-2 mt-2">
                                                    {
                                                        productDetail.length !== 0 ?
                                                        <>
                                                            {
                                                                productDetail[0].images.length !== 0 ?
                                                                <img src={productDetail[0].images[0].imageURL} alt=""  style={{
                                                                    width: "54px",
                                                                    height: "54px",
                                                                    borderRadius: "12px",
                                                                    marginLeft: "-15px"
                                                                }}/>
                                                                :
                                                                <></>
                                                            }
                                                        </>
                                                        :
                                                        <></>
                                                    }
                                                </div>
                                                <div class="col-10">
                                                    <div class="detail-card-desc">
                                                        <div class="detail-desc text-muted" style={{fontSize: "10px"}}>
                                                            Penawaran produk
                                                            <span class="detail-desc-right">20 Apr, 14:04</span>
                                                        </div>
                                                        {
                                                            // si useSelector harus di tunggu bentar kalo ga bakal error karena pas ngeprint beluma ada datanya
                                                            ClickTransactions.length !== 0 ?
                                                            <>
                                                                <div class="fw-normal" style={{fontSize:"14px"}}>{ClickTransactions[0].data.product.name}</div>
                                                                <div class="fw-normal" style={{fontSize:"14px"}}>Rp. {ClickTransactions[0].data.product.price}</div>
                                                                <div class="fw-normal" style={{fontSize:"14px"}}>Ditawar Rp. {ClickTransactions[0].data.transaksi.bargainPrice}</div>

                                                            </>
                                                            :
                                                            <></>
                                                        }
                                                    </div>
                                                </div>
                                                <div className='mt-3 mb-3'>
                                                    {
                                                        productDetail.length !== 0 ? 
                                                        <ModalInfoPenawar 
                                                        gambarProfil={gambarProfil}
                                                        namaPembeli={namaPembeli} 
                                                        kotaPembeli={kotaPembeli} 
                                                        idPembeli ={idPembeli}
                                                        nomor={nomor}
                                                        gambarProduk={productDetail[0].images[0].imageURL}
                                                        idProduk={productDetail[0].data[0].id}
                                                        idTransaksi={id}
                                                        // produk={barangFilter}
                                                        namaProduk={productDetail[0].data[0].name} 
                                                        hargaProduk={productDetail[0].data[0].price}
                                                        // tawarProduk={barangFilter[0].bargainPrice}
                                                        ClickTransactions={ClickTransactions}
                                                        />
                                                        :
                                                        <></>
                                                    }
                                                </div>
                                                <hr />
                                            </div>
                                        </div>
                                </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    );
}

export default InfoPenawar;
