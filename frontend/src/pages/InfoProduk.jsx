import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import fi_arrowleft from '../images/fi_arrowleft.png';
import { updateInformasiProdukService } from '../services/updateInformasiProduk'
import logoKotak from '../images/logoKotak.png';
import { previewProduct } from "../store/actions/PreviewProdukActions";
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';

import jwt_decode from 'jwt-decode'
import axios from 'axios';

const InfoProduk = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    // const productDetail = useSelector((state) => state.productDetail); 
    // const WAWA = useSelector((state) => state.previewProduct)

    const [name, setName] = useState('');
    const [price, setPrice] = useState();
    const [kategori, setKategori] = useState();
    const [description, setDescription] = useState('');
    const [id, setId] = useState('')

    const [file, setFile] = useState();
    const [file1, setFile1] = useState();
    const [file2, setFile2] = useState();
    const [file3, setFile3] = useState();

    console.log(file,file1, file2, file3, "ini fileee");

    const [fileUrl, setFileUrl] = useState([]);
    const [jaga, setJaga] = useState(false) // misal ada gambar lebih dari 1, 2 misalnya, salah satu didelete
    // nanti gambar satu lagi bakal kena useEffect jadi terdetek sama seperti klik gambar
    // jadinya pas di delete, gambar yang kesisa bakal jadi ada 2 

    const handleGambar = (e) => {
        e.preventDefault()
        setJaga(true)
        if (e.target.files[0].type === "image/jpeg" || e.target.files[0].type === "image/png") {

            // setFile([...file, e.target.files[0]]) // jangan lupa tetep dikurungin kalo ga, spread operator nya gakan bekerja
            
            console.log(e.target.files, 'ini e target filess');
            setFile(e.target.files[0]);
            // formData.append('image', e.target.files[0])
            // console.log(...formData, "ini formData di handleGambar");

        } else {
            setFileUrl('');
            alert("File type is not supported");
        }
    }

    const handleGambar1 = (e) => {
        e.preventDefault()
        if (e.target.files[0].type === "image/jpeg" || e.target.files[0].type === "image/png") {
            console.log(e.target.files, 'ini e target filess');
            setFile1(e.target.files[0]);
        } else {
            setFileUrl('');
            alert("File type is not supported");
        }
    }

    const handleGambar2 = (e) => {
        e.preventDefault()
        if (e.target.files[0].type === "image/jpeg" || e.target.files[0].type === "image/png") {
            console.log(e.target.files, 'ini e target filess');
            setFile2(e.target.files[0]);
        } else {
            setFileUrl('');
            alert("File type is not supported");
        }
    }

    const handleGambar3 = (e) => {
        e.preventDefault()
        if (e.target.files[0].type === "image/jpeg" || e.target.files[0].type === "image/png") {
            console.log(e.target.files, 'ini e target filess');
            setFile3(e.target.files[0]);
        } else {
            setFileUrl('');
            alert("File type is not supported");
        }
    }

    const handleHapusGambar = (e, index) => {
        // e.preventDefault()
        // setJaga(false)

        // console.log(index, "ini index sebagai paramater");
        // let arrKosongFile = [...file]
        // arrKosongFile.splice(index, 1)
        // let arrKosongFileUrl = [...fileUrl]
        // arrKosongFileUrl.splice(index, 1)
        // console.log(arrKosongFile, arrKosongFileUrl, index, "ini arrKosongFile, arrKosongFileUrl dan index");
        // setFile(arrKosongFile)
        // setFileUrl(arrKosongFileUrl)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        let formData = new FormData();

        formData.append('name', name)
        formData.append('price', price)
        formData.append('id_category', kategori)
        formData.append('description', description)
        formData.append('image', file)

        let formData1 = new FormData()
        formData1.append('image',file1)

        let formData2 = new FormData();
        formData2.append('image',file2)

        let formData3 = new FormData();
        formData3.append('image',file3)


        let res = await axios.post('http://localhost:8000/api/products/create', formData, {
            headers:{
                'Authorization':`${localStorage.getItem('token')}`
            }
        })
        setId(res.data.productImage.id_products)

        if(res.status === 201){
            toast.success('Gambar Pertama Berhasil Diunggah !', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }

        let resp1 = await axios.post(`http://localhost:8000/api/products/${res.data.productImage.id_products}/uploadImage`,formData1, {
            headers:{
                'Authorization':`${localStorage.getItem('token')}`
            }
        })
        if(resp1.status === 201){
            toast.success('Gambar Kedua Berhasil Diunggah !', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        

        console.log(resp1, "ini balikkan dari gambar kedua / 2");

        let resp2 = await axios.post(`http://localhost:8000/api/products/${res.data.productImage.id_products}/uploadImage`,formData2, {
            headers:{
                'Authorization':`${localStorage.getItem('token')}`

            }
        })
        if(resp2.status === 201){
            toast.success('Gambar Ketiga Berhasil Diunggah !', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        console.log(resp2, "ini balikkan dari gambar ketiga / 3");


        let resp3 = await axios.post(`http://localhost:8000/api/products/${res.data.productImage.id_products}/uploadImage`,formData3, {
            headers:{
                'Authorization':`${localStorage.getItem('token')}`
            }
        })
        if(resp3.status === 201){
            toast.success('Gambar Keempat Berhasil Diunggah !', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        console.log(resp3, "ini balikkan dari gambar keempat / 4");

        toast.success('Produk Berhasil Dibuat ! Silahkan Kembali Ke Halaman Utama Untuk Melihat Produk Anda ', {
            position: "top-center",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });
        


        // alert('Produk berhasil diterbitkan!')
    }

    const handlePreview = async (e) => {
        e.preventDefault()

        let wawa = {
            name: name,
            price: price,
            id_category: kategori,
            description: description,
            image: fileUrl,
            fileIMG: [file, file1, file2, file3]
        }
        console.log(wawa, "ini informasiProduk sebelum di dispatch");
        dispatch(previewProduct(wawa))
        navigate('/previewproduct')

    }

    useEffect(() => {
        let fileData
        fileData = new FileReader()

        if (file && jaga === true) {
            fileData.readAsDataURL(file) 
            fileData.onload = (e) => {  // kalau udah selesai, akan ada event load, makanya diliat dari onload 
                if (e.target.result) {
                    setFileUrl([...fileUrl, e.target.result])
                    // setFileUrl(e.target.result)
                }
            }
        }
    }, [file]);

    useEffect(() => {
        let fileData1
        fileData1 = new FileReader()

        if (file1 && jaga === true) {
            fileData1.readAsDataURL(file1) 
            fileData1.onload = (e) => {  // kalau udah selesai, akan ada event load, makanya diliat dari onload 
                if (e.target.result) {
                    setFileUrl([...fileUrl, e.target.result])
                    // setFileUrl(e.target.result)
                }
            }
        }
    }, [file1]);

    useEffect(() => {
        let fileData2
        fileData2 = new FileReader()

        if (file2 && jaga === true) {
            fileData2.readAsDataURL(file2) 
            fileData2.onload = (e) => {  // kalau udah selesai, akan ada event load, makanya diliat dari onload 
                if (e.target.result) {
                    setFileUrl([...fileUrl, e.target.result])
                    // setFileUrl(e.target.result)
                }
            }
        }
    }, [file2]);

    useEffect(() => {
        let fileData3
        fileData3 = new FileReader()

        if (file3 && jaga === true) {
            fileData3.readAsDataURL(file3) 
            fileData3.onload = (e) => {  // kalau udah selesai, akan ada event load, makanya diliat dari onload 
                if (e.target.result) {
                    setFileUrl([...fileUrl, e.target.result])
                    // setFileUrl(e.target.result)
                }
            }
        }
    }, [file3]);

    useEffect(() => {
        async function fetchMyApi(){
            let {data} = await axios.get(`http://localhost:8000/api/users/profile`,{
                headers:{
                    Authorization: `${localStorage.getItem('token')}`
                }
            })
            if(data.data.profileImage_url == null || data.data.address == null || data.data.phoneNumber == null || data.data.provinceId == null || data.data.cityId == null ){
                alert('Harap Melengkapi Informasi Akun Anda Terlebih Dahulu') // Warning: StrictMode will render the components twice only on the development mode not production.
                navigate('/infoprofil')
            }
        }
        fetchMyApi()
    }, [])   

    

    return (
        <div class='container-fluid px-0'>
                <nav class="navbar navbar-expand d-none d-md-block navbar-light bg-light shadow-sm">
                        <div className="d-flex w-100 justify-content-between">
                            <Link to='/' style={{textDecoration: "none", color: "purple"}}> 
                                    <img src={logoKotak} alt="" className='ps-5 d-none d-md-block'/>
                            </Link> 
                            <div class=" position-absolute top-50 start-50 translate-middle d-none d-md-block h6" >
                                    Lengkapi Info Produk
                            </div>
                        </div>
                </nav>
                

                <div className='d-flex justify-content-center' >
                    <form class='w-100'>
                        <div className='d-flex align-items-start  position-relative mt-5 mb-5 pb-4'>

                            <Link to='/' className='ps-3' style={{ textDecoration: "none", color: "purple" }}>
                                <img src={fi_arrowleft} alt="" class='' />
                            </Link>

                            <div className=' position-absolute top-0 start-50 translate-middle-x w-75 px-3 '>
                                <div class="d-flex justify-content-center pb-4 h6 d-block d-md-none" >
                                    Lengkapi Info Produk
                                </div>
                                <div className=" mx-auto">
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Nama Produk</label>
                                        <input type="text" onChange={(e) => setName(e.target.value)} class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputEmail1" class="form-label">Harga Produk</label>
                                        <input type="text" onChange={(e) => setPrice(e.target.value)} class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" />
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleInputPassword1" class="form-label">Kategori</label>
                                        <select class="form-select" onChange={(e) => setKategori(Number(e.target.value))} aria-label="Default select example">
                                            <option disabled selected hidden>Pilih Kategori</option>
                                            <option value="1">Hobi</option>
                                            <option value="2">Kendaraan</option>
                                            <option value="3">Baju</option>
                                            <option value="4">Elektronik</option>
                                            <option value="5">Kesehatan</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Deskripsi</label>
                                        <textarea class="form-control" onChange={(e) => setDescription(e.target.value)} id="exampleFormControlTextarea1" rows="3" placeholder='Contoh: Jalan Ikan Hiu 33'></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label for="exampleFormControlTextarea1" class="form-label">Foto Produk</label>

                                        {/* {file.length !== 0 ?
                                            <>
                                                {fileUrl.map((item, index) => {
                                                    return <div class="border-radius-13px d-flex align-items-center" style={{}}>
                                                        <img src={fileUrl[index]} alt="" class='profil_picture' />
                                                        <button onClick={(e) => handleHapusGambar(e, index)}>Hilangkan</button>
                                                    </div>
                                                })}
                                            </>
                                            :
                                            <div className='d-flex flex-column align-items-center'>
                                                😀
                                            </div>
                                        }
                                        {file.length < 4 ?
                                            <div className='w-100'>
                                                <input className='w-100' accept='image/*' type="file" name="upload_gambar" onChange={(e) => handleGambar(e)} />
                                            </div>
                                            :
                                            <></>
                                        } */}
                                        <input className='w-100' accept='image/*' type="file" name="upload_gambar" onChange={(e) => handleGambar(e)} style={{outlineStyle: "solid", outlineColor: "#D0D0D0", outlineWidth: "1px", padding: "7px", borderRadius: "3px" }}/>
                                        <input className='w-100 mt-3' accept='image/*' type="file" name="upload_gambar" onChange={(e) => handleGambar1(e)} style={{outlineStyle: "solid", outlineColor: "#D0D0D0", outlineWidth: "1px", padding: "7px", borderRadius: "3px" }}/>
                                        <input className='w-100 mt-3' accept='image/*' type="file" name="upload_gambar" onChange={(e) => handleGambar2(e)} style={{outlineStyle: "solid", outlineColor: "#D0D0D0", outlineWidth: "1px", padding: "7px", borderRadius: "3px" }} />
                                        <input className='w-100 mt-3' accept='image/*' type="file" name="upload_gambar" onChange={(e) => handleGambar3(e)} style={{outlineStyle: "solid", outlineColor: "#D0D0D0", outlineWidth: "1px", padding: "7px", borderRadius: "3px" }}/>
                                        

                                    </div>
                                    <div className="row">
                                        <div className="col-md-6 py-3 py-md-0 ">
                                            {/* type nya jangan submit, harus di prevent Default nanti, bikin pusing gilak dari tadi kenapa gagal mulu T_T */}
                                            <button type="button" class="btn w-100"
                                            onClick={(e) => handlePreview(e)} style={{outlineStyle: "solid", outlineColor: "#7126B5", outlineWidth: "1px", borderRadius: "16px" }}
                                            >Preview</button>
                                        </div>
                                        <div className="col-md-6 pb-5">
                                            <button type="submit" class="btn w-100" onClick={(e) => handleSubmit(e)} style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px"}}>Terbitkan</button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    );
}

export default InfoProduk;
