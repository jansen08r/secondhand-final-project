import React, {useState, useEffect} from 'react';
import { Link, useNavigate } from "react-router-dom";
import fi_camera from '../images/fi_camera.png';
import fi_arrowleft from '../images/fi_arrowleft.png';
import logoKotak from '../images/logoKotak.png';
import { loginService } from '../services/loginRegister';
import { updateInformasiAkunService } from '../services/updateInformasiAkun';
import { fetchKota, fetchProvinsi } from '../services/fetchProvinsiKota';
import './styles.css'
import axios from 'axios';

import { FiLogOut } from 'react-icons/fi';

const InformasiAkun = () => {
    const navigate = useNavigate();

    const [username, setUsername] = useState('');
    const [cityId, setCityId] = useState('');
    const [provinceId, setProvinceId] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [listProvince, setListProvince] = useState('');
    const [listCity, setListCity] = useState('');
    const [nama, setNama] = useState('')
    const [email, setEmail] = useState('')
    const [provinsi, setProvinsi] = useState('')
    const [listProvinsi, setListProvinsi] = useState([])
    const [kota, setKota] = useState('')
    const [listKota, setListKota] = useState([])
    const [alamat, setAlamat] = useState()
    const [nomor, setNomor] = useState()
    const [login, setLogin] = useState(false)
    const [gambar, setGambar] = useState('')

    const handleLogout = () => {
        // localStorage.clear()
        localStorage.removeItem("token")
        setLogin(false)
        navigate('/')
    }

    // List Provinsi
    useEffect(() => {
        async function fetchMyApi() {
            let res = await fetchProvinsi();
            setListProvince(...listProvince, res.data);
        }

        fetchMyApi();
    }, []);

    // List Kota ( biasa disebut callback )
    // dia langsung jalanin fungsi yang callbacknya didengerin sama useEffect, keren sih, meskipun lebih panjang kalo pake functional component , sumber : On Ben Hare's answer, If someone wants to achieve the same using React Hooks I have added sample code below.
    useEffect(() => {  // this hook will get called everytime when myArr has changed
        // perform some action which will get fired everytime when myArr gets updated

        async function fetchMyApi() {
            let res = await fetchKota(provinceId); // isi provinsi itu id
            setListCity(res.data);
        }
        fetchMyApi();
    }, [provinceId]);

    // Buat cari informasi akun 
    useEffect(() => {
        async function fetchMyApi() {
            let {data} = await axios.get(`http://localhost:8000/api/users/profile`,{
                headers:{
                    Authorization : `${localStorage.getItem("token")}`
                }
            });
            setNama(data.data.username);
            setEmail(data.data.email)
            setGambar(data.data.profileImage_url)
            setAddress(data.data.address)
            setPhoneNumber(data.data.phoneNumber)
            setProvinceId(data.data.provinceId)
            setCityId(data.data.cityId)
        }

        fetchMyApi();
    
    }, [])

    // niatnya buat nyari kota
    useEffect(() => {
        async function fetchMyApi() {
            let kabUser = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/provinsi/${provinceId}`);
    
            setProvinsi(kabUser.data.nama)
            let kotaUser = await axios.get(`https://dev.farizdotid.com/api/daerahindonesia/kota/${cityId}`)
          
            setKota(kotaUser.data.nama)
            
        }

        fetchMyApi();
    
    }, [provinceId, cityId])
    


    return (
        <div class='container-fluid px-0'>
            <nav class="navbar navbar-expand d-none d-md-block navbar-light bg-light shadow-sm">
                    <div className="d-flex w-100 justify-content-between">
                        <Link to='/' style={{textDecoration: "none", color: "purple"}}> 
                                <img src={logoKotak} alt="" className='ps-5 d-none d-md-block'/>
                        </Link> 
                        <div class=" position-absolute top-50 start-50 translate-middle d-none d-md-block h6" >
                                Informasi Akun Saya
                        </div>
                    </div>
            </nav>

            <div className='mt-5' style={{marginLeft: "15px", overflow: "hidden"}}>
                <div className='row'>
                    <div className='col-4'>
                    <Link to='/product-sell#wishlist'> 
                        <img src={fi_arrowleft} alt="" />
                    </Link>
                    </div>
                    <div className='col-8'>
                    <div class="h6 d-block d-md-none" >
                        Akun Saya
                    </div>
                    </div>
                </div>
            </div>
            
            <div className='d-flex justify-content-center'>
                    <div className='d-flex align-items-start position-relative mt-3 mb-5 pb-4'> 
                            <div className='px-3'>
                                <div class="d-flex flex-column align-items-center">
                                    <img src={gambar} alt="" class='profil_picture' style={{ borderRadius: "16px"}}/>
                                </div>
                                
                                <div className=" mx-auto pt-3">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                        <th scope="row">Nama</th>
                                        <td>{nama}</td>
                                        </tr>
                                        <tr>
                                        <th scope="row">Email</th>
                                        <td>{email}</td>
                                        </tr>
                                        <tr>
                                        <th scope="row">Alamat</th>
                                        <td>{address}</td>
                                        </tr>
                                        <tr>
                                        <th scope="row">No. Hp</th>
                                        <td>{phoneNumber}</td>
                                        </tr>
                                        <tr>
                                        <th scope="row">Provinsi</th>
                                        <td>{provinsi}</td>
                                        </tr>
                                        <tr>
                                        <th scope="row">Kota</th>
                                        <td>{kota}</td>
                                        </tr>
                                    </tbody>
                                    </table>
                                    </div>
                                    
                                    <div className='btn-group mb-5'>
                                        <button class="btn-edit rounded-pill" onClick={() => navigate('/infoprofil')} style={{outlineStyle: "solid", outlineColor: "#7126B5", outlineWidth: "1px", borderRadius: "16px", border: "none", fontSize: "14px"}}>Ubah Akun</button>
                                        <button class="btn-logout rounded-pill" onClick={() => handleLogout()} style={{ backgroundColor: "#7126B5", borderRadius: "16px", marginLeft: "12px", border: "none", fontSize: "14px"}}>Logout <FiLogOut style={{marginLeft: "4px"}}/></button>
                                    </div>
                                </div>
                            </div>
                    </div>
            </div>
    );
}

export default InformasiAkun;
