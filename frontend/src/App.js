import LoginPage from './pages/LoginPage';
import './App.css';
import { Routes, Route } from "react-router-dom";
import RegisterPage from './pages/RegisterPage';
import HomePage from './pages/HomePage';
import InfoProfil from './pages/InfoProfil';
import InfoProduk from './pages/InfoProduk';
import DetailProduct from './pages/DetailProduct';
import DaftarJual from './pages/DaftarJual/DaftarJual';
import PrivateRoute from "./routes/PrivateRoute";
import PreviewProduct from './pages/PreviewProduct'
import InfoPenawar from './pages/InfoPenawar';
import EditProduk from './pages/EditProduk';
import InformasiAkun from './pages/InformasiAkun';
import Notification from './pages/NotificationPage';
 
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css'; // kenapa ditaruh di app.js ? sumber : https://stackoverflow.com/questions/69478209/react-toastify-is-not-working-when-page-is-redirect-to-another-page-but-it-is


function App() {
  return (
    <div className="App"> 
    <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route path="/infoprofil" element={
            <PrivateRoute>
              <InfoProfil/>
            </PrivateRoute>}/>
        <Route path="/infoproduk" element={
            <PrivateRoute>        
              <InfoProduk/>
            </PrivateRoute>} />
        <Route path="/detailproduk/:id" element={
            <PrivateRoute>        
              <DetailProduct/>
            </PrivateRoute>} />
        <Route path="product-sell/editproduk/:id" element={
            <PrivateRoute>        
              <EditProduk />
            </PrivateRoute>} />
        <Route path="/informasiakun" element={<InformasiAkun/>} />
        <Route path="/previewproduct" element={<PreviewProduct />} />
        <Route path="/product-sell" element={<DaftarJual />} />
        <Route path="/infopenawar/:id" element={<InfoPenawar />} />
        <Route path="/notifikasi" element={<Notification />} />
    </Routes>
    <ToastContainer
                    position="top-center"
                    autoClose={5000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
    </div>
  );
}

export default App;
