import React from 'react';
import kacaPembesar from '../images/kacaPembesar.png';
import kacaPembesarHitam from '../images/kacaPembesarHitam.png';

const Kategori = () => {
    return (
        <div class="container">
            <h5>Telusuri Kategori</h5>
            <div className='d-flex flex-wrap'>
                <button type="button" class="btn ms-2 me-2" style={{backgroundColor : "#7126B5",color:'white'}}>
                    <img src={kacaPembesar} alt="" />
                    Semua
                </button>
                <button type="button" class="btn mx-2" style={{backgroundColor : "#E2D4F0"}}>
                    <img src={kacaPembesar} alt="" />
                    Hobi
                </button>
                <button type="button" class="btn mx-2" style={{backgroundColor : "#E2D4F0"}}>
                    <img src={kacaPembesar} alt="" />
                    Kendaraan
                </button>
                <button type="button" class="btn mx-2" style={{backgroundColor : "#E2D4F0"}}>
                    <img src={kacaPembesar} alt="" />
                    Baju
                </button>
                <button type="button" class="btn mx-2" style={{backgroundColor : "#E2D4F0"}}>
                    <img src={kacaPembesar} alt="" />
                    Elektronik
                </button>
                <button type="button" class="btn mx-2" style={{backgroundColor : "#E2D4F0"}}>
                    <img src={kacaPembesar} alt="" />
                    Kesehatan 
                </button>
            </div>
        </div>
        
    );
}

export default Kategori;
// #7126B5
