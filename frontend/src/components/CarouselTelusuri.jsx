import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import "./SwiperCarousel.css";

import { FaSearch } from 'react-icons/fa';

const CarouselTelusuri = (props) => {
  const { kategori, setKategori } = props;

  return (
    <div className="pb-4 pt-4">
      <h5 className="ps-3 pb-3">Telurusi Kategori</h5>
      <div className="d-none d-md-block">
        <div className="btn-group px-2">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center px-3 py-2 mx-2"
              onClick={() => {
                  setKategori(0)
              }}
              // style={{ backgroundColor: "#7126B5", color: "white"}}
            >
              <span class="px-2"><FaSearch /></span> Semua
            </button>
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2 mx-2"
              onClick={() => {
                  setKategori(1)
              }}
            >
              <span class="px-2"><FaSearch /></span> Hobi
            </button>
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2 mx-2"
              onClick={() => {
                  setKategori(2)
              }}
            >
              <span class="px-2"><FaSearch /></span> Kendaraan
            </button>
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2 mx-2"
              onClick={() => {
                  setKategori(3)
              }}
            >
              <span class="px-2"><FaSearch /></span> Baju
            </button>
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2 mx-2"
              onClick={() => {
                  setKategori(4)
              }}
            >
              <span class="px-2"><FaSearch /></span> Elektronik
            </button>
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2 mx-2"
              onClick={() => {
                  setKategori(5)
              }}
            >
              <span class="px-2"><FaSearch /></span>  Kesehatan
            </button>
        </div>
      </div>

      <div class="d-block d-md-none px-3">
        <Swiper
          slidesPerView={"auto"}
          spaceBetween={50}
          // pagination={{
          //   clickable: true,
          // }}
          grabCursor={true}
          modules={[Pagination]}
          className="mySwiper pou"
        >
          <SwiperSlide className="pou">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2"
              onClick={() => {
                  setKategori(0)
              }}
              style={{ marginLeft: "20px"}}
            >
              <span class="px-2"><FaSearch /></span> Semua
            </button>
          </SwiperSlide>
          <SwiperSlide className="pou">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2"
              onClick={() => {
                  setKategori(1)
              }}
              style={{ marginLeft: "-25px" }}
            >
              <span class="px-2"><FaSearch /></span> Hobi
            </button>
          </SwiperSlide>
          <SwiperSlide className=" pou">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2"
              onClick={() => {
                  setKategori(2)
              }}
              style={{ marginLeft: "-40px" }}
            >
              <span class="px-2"><FaSearch /></span> Kendaraan
            </button>
          </SwiperSlide>
          <SwiperSlide className="pou">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2"
              onClick={() => {
                  setKategori(3)
              }}
              style={{ marginLeft: "-55px" }}
            >
             <span class="px-2"><FaSearch /></span> Baju
            </button>
          </SwiperSlide>
          <SwiperSlide className="pou">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2"
              onClick={() => {
                  setKategori(4)
              }}
              style={{ marginLeft: "-75px" }}
            >
              <span class="px-2"><FaSearch /></span> Elektronik
            </button>
          </SwiperSlide>
          <SwiperSlide className="pou">
            <button
              type="button"
              class="btn btnClass d-flex justify-content-center border-radius-13px px-3 py-2"
              onClick={() => {
                  setKategori(5)
              }}
              style={{ marginLeft: "-55px" }}
            >

             <span class="px-2"><FaSearch /></span> Kesehatan
            </button>
          </SwiperSlide>
        </Swiper>
      </div>
    </div>
  );
};

export default CarouselTelusuri;
