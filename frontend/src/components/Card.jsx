import React from 'react';
import gambarJam1 from '../images/gambarJam1.png'

const Card = (props) => {
    return (
        <div>
            <div class="card" style={{width: "18rem"}}>
                <img src={gambarJam1} class="card-img-top" alt="..." />
                <div class="card-body">
                    <h5 class="card-title">Jam Tangan Casio</h5>
                    <h6 class="card-text text-muted">Aksesoris</h6>
                    <h5 class="card-title">Rp 250.000</h5>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
    );
}

export default Card;
    