import React, { useRef, useState } from 'react';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/free-mode";
import { FreeMode, Pagination } from "swiper";
// import bannerUtama from '../images/bannerUtama.png';
import kacaPembesar from '../images/kacaPembesar.png';
// import kacaPembesarHitam from '../images/kacaPembesarHitam.png';

const SwiperCarousel1 = () => {
    return (
        <div className="container pt-5 pb-4 mb-3">
            <div className='row '>
                <div className='px-0 '>Telusuri Kategori</div>
                <Swiper
                    freeMode={true}
                    slidesPerView={1}
                    // pagination={{
                    //     clickable: true,
                    // }}
                    modules={[FreeMode, Pagination]}
                    className="mySwiper"
                >
                    <SwiperSlide className='w-auto me-4 ' >
                        <div className=' d-flex justify-content-center' style={{}}>
                            <button type="button" class="btn d-flex" style={{backgroundColor : "#7126B5",color:'white'}}>
                                <img src={kacaPembesar} alt="" />
                                Semua
                            </button>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide className='w-auto me-4 '>
                        <div className=' d-flex justify-content-center' style={{}}>
                            <button type="button" class="btn d-flex" style={{backgroundColor : "#E2D4F0"}}>
                                <img src={kacaPembesar} alt="" />
                                Hobi
                            </button>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide className='w-auto me-4'>
                        <div className=' d-flex justify-content-center' style={{}}>
                            <button type="button" class="btn d-flex" style={{backgroundColor : "#E2D4F0"}}>
                                <img src={kacaPembesar} alt="" />
                                Kendaraan
                            </button>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide className='w-auto me-4'>
                        <div className=' d-flex justify-content-center' style={{}}>
                            <button type="button" class="btn d-flex" style={{backgroundColor : "#E2D4F0"}}>
                                <img src={kacaPembesar} alt="" />
                                Baju
                            </button>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide className='w-auto me-4'>
                        <div className=' d-flex justify-content-center' style={{}}>
                            <button type="button" class="btn d-flex" style={{backgroundColor : "#E2D4F0"}}>
                                <img src={kacaPembesar} alt="" />
                                Elektronik
                            </button>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide className='w-auto me-4'>
                        <div className=' d-flex justify-content-center' style={{}}>
                            <button type="button" class="btn d-flex" style={{backgroundColor : "#E2D4F0"}}>
                                <img src={kacaPembesar} alt="" />
                                Kesehatan 
                            </button>
                        </div>
                    </SwiperSlide>
                
                </Swiper>
            </div>
            
        </div>
        
    );
}

export default SwiperCarousel1;
