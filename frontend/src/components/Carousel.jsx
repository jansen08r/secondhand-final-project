import React from 'react';

const Carousel = () => {
    return (
        <div>
            <div
                id="carouselExampleControls"
                class="row carousel slide"
                data-bs-ride="carousel"
            >
                <div class="carousel-inner">
                    <div class="carousel-item active">

                        <div class="row">
                            <div
                                class="col-lg-4 d-flex justify-content-center align-items-center"
                            >
                                <div class="card shadow" style={{width: "30rem"}}>
                                    <div class="row">
                                        <div
                                            class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                        >
                                            {/* <img
                                                src="./assests/img/foto2.png "
                                                style="width: 80px; height: 80px"
                                                class=""
                                                alt="..."
                                            /> */}
                                        </div>
                                        <div class="col-lg-8 col-12">
                                            <div class="card-body d-flex flex-column  py-4">
                                                <div class="d-flex justify-content-center justify-content-lg-start">
                                                    {/* <img
                                                        src="./assests/img/rate_kuning.png"
                                                        alt=""
                                                        class="pb-2 "
                                                    /> */}
                                                </div>

                                                <p class="card-text">
                                                    Some quick example text to build on the card title
                                                    and make up the bulk of the card's content.
                                                </p>
                                                <h6>John Dee 32, Bromo</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div
                                class="col-lg-4 d-flex justify-content-center align-items-center"
                            >
                                <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                    <div class="row">
                                        <div
                                            class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                        >
                                            {/* <img
                                                src="./assests/img/foto2.png "
                                                style="width: 80px; height: 80px"
                                                class=""
                                                alt="..."
                                            /> */}
                                        </div>
                                        <div class="col-lg-8 col-12">
                                            <div class="card-body d-flex flex-column  py-4">
                                                <div class="d-flex justify-content-center justify-content-lg-start">
                                                    {/* <img
                                                        src="./assests/img/rate_kuning.png"
                                                        alt=""
                                                        class="pb-2 w-25 "
                                                    /> */}
                                                </div>

                                                <p class="card-text">
                                                    Some quick example text to build on the card title
                                                    and make up the bulk of the card's content.
                                                </p>
                                                <h6>John Dee 32, Bromo</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div
                                class="col-lg-4 d-flex justify-content-center align-items-center"
                            >
                                <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                    <div class="row">
                                        <div
                                            class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                        >
                                            {/* <img
                                                src="./assests/img/foto2.png "
                                                style="width: 80px; height: 80px"
                                                class=""
                                                alt="..."
                                            /> */}
                                        </div>
                                        <div class="col-lg-8 col-12">
                                            <div class="card-body d-flex flex-column  py-4">
                                                <div class="d-flex justify-content-center justify-content-lg-start">
                                                    {/* <img
                                                        src="./assests/img/rate_kuning.png"
                                                        alt=""
                                                        class="pb-2 w-25 "
                                                    /> */}
                                                </div>

                                                <p class="card-text">
                                                    Some quick example text to build on the card title
                                                    and make up the bulk of the card's content.
                                                </p>
                                                <h6>John Dee 32, Bromo</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="carousel-item ">

                        <div class="row">
                            <div
                                class="col-lg-4 d-flex justify-content-center align-items-center"
                            >
                                <div class="card shadow" style={{width: "30rem"}}>
                                    <div class="row">
                                        <div
                                            class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                        >
                                            {/* <img
                                                src="./assests/img/foto2.png "
                                                style="width: 80px; height: 80px"
                                                class=""
                                                alt="..."
                                            /> */}
                                        </div>
                                        <div class="col-lg-8 col-12">
                                            <div class="card-body d-flex flex-column  py-4">
                                                <div class="d-flex justify-content-center justify-content-lg-start">
                                                    {/* <img
                                                        src="./assests/img/rate_kuning.png"
                                                        alt=""
                                                        class="pb-2 "
                                                    /> */}
                                                </div>

                                                <p class="card-text">
                                                    Some quick example text to build on the card title
                                                    and make up the bulk of the card's content.
                                                </p>
                                                <h6>John Dee 32, Bromo</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div
                                class="col-lg-4 d-flex justify-content-center align-items-center"
                            >
                                <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                    <div class="row">
                                        <div
                                            class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                        >
                                            {/* <img
                                                src="./assests/img/foto2.png "
                                                style="width: 80px; height: 80px"
                                                class=""
                                                alt="..."
                                            /> */}
                                        </div>
                                        <div class="col-lg-8 col-12">
                                            <div class="card-body d-flex flex-column  py-4">
                                                <div class="d-flex justify-content-center justify-content-lg-start">
                                                    {/* <img
                                                        src="./assests/img/rate_kuning.png"
                                                        alt=""
                                                        class="pb-2 w-25 "
                                                    /> */}
                                                </div>

                                                <p class="card-text">
                                                    Some quick example text to build on the card title
                                                    and make up the bulk of the card's content.
                                                </p>
                                                <h6>John Dee 32, Bromo</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div
                                class="col-lg-4 d-flex justify-content-center align-items-center"
                            >
                                <div class="card shadow d-none d-lg-block" style={{width: "30rem"}}>
                                    <div class="row">
                                        <div
                                            class="col-lg-4 col-12 d-flex justify-content-center align-items-center ps-lg-5 pt-5 pt-lg-0"
                                        >
                                            {/* <img
                                                src="./assests/img/foto2.png "
                                                style="width: 80px; height: 80px"
                                                class=""
                                                alt="..."
                                            /> */}
                                        </div>
                                        <div class="col-lg-8 col-12">
                                            <div class="card-body d-flex flex-column  py-4">
                                                <div class="d-flex justify-content-center justify-content-lg-start">
                                                    {/* <img
                                                        src="./assests/img/rate_kuning.png"
                                                        alt=""
                                                        class="pb-2 w-25 "
                                                    /> */}
                                                </div>

                                                <p class="card-text">
                                                    Some quick example text to build on the card title
                                                    and make up the bulk of the card's content.
                                                </p>
                                                <h6>John Dee 32, Bromo</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-karosel pt-0">
                <button
                    class="carousel-control"
                    type="button"
                    data-bs-target="#carouselExampleControls"
                    data-bs-slide="prev"
                >
                    PREV
                    {/* <img src="./assests/img/Left button.png" alt="" /> */}
                </button>
                <button
                    class="carousel-control"
                    type="button"
                    data-bs-target="#carouselExampleControls"
                    data-bs-slide="next"
                >
                    NEXT
                    {/* <img src="./assests/img/Right button.png" alt="" /> */}
                </button>
            </div>

        </div>

    );
}

export default Carousel;
