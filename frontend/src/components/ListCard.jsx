import React,{useEffect, useState} from 'react';
import CardBulet from './CardBulet';
import axios from 'axios';

import gifLoading from '../images/loading.gif'

const ListCard = (props) => {

    const {products} = props;
    // console.log(products, "ini products di listcard"); // ini kalo di console.log() kacau Pak clg terus gatau kenapa
    const produk = products.filter((item) => item.isSold != true )
    console.log(produk, "ini produk yang belum laku terjual");

    const [gambar, setGambar] = useState('')
    // console.log(gambar, "ini gambar di listacrd ++++++++++++++");

    useEffect(() => {

        const getWithForOf = async() => {
            const arrKosong = []

            for (const product of produk) {
                let {data} = await axios.get(`http://localhost:8000/api/products/${product.id}/detail/`,{
                        headers: {
                            Authorization: `${localStorage.getItem('token')}`
                        }
                    })
                arrKosong.push(data.images)
            }
            setGambar(arrKosong)
         }
        getWithForOf();
    }, [products]);  // nge render terus kalau dimasukkin produk, kenapa ? gatau jujur, apa jangan jangan gara gara dia bukan props atau apa gitu jadi dia ngerender terus kan, terus kedetect sebagai pergerakkan di useEffect nya
    // kalo dikosongin malah nanti keburu jalan duluan sebelum si passing props gambar ready

    return (
        <>
            {produk.length === 0 && gambar.length !== 0 ? 
                <div className='d-flex justify-content-center'>
                    <div className='flex-column'>
                        <img src={gifLoading} alt="" style={{ width: "300px"}}></img>
                        <div className='pb-5'>Jika barang belum muncul, kemungkinan barang kosong...</div>
                    </div>
                </div>
                :
                <div className='container-fluid'>
                    <div className='row g-3 pb-5'>
                        {produk.map((card, index) => {
                            return <CardBulet 
                            id={card.id}
                            title={card.name} 
                            description={card.description} 
                            fileUrl={card.fileUrl} 
                            price={card.price} 
                            id_category={card.id_category}
                            gambar={gambar[index]}
                            />
                        })}
                    </div>
                </div>
            }
        </>
        
    );
}

export default ListCard;
