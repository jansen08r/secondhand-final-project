import React from 'react';
import './Modal.css';
import gambarJam1 from '../../images/gambarJam1.png';

const ModalDetailProduct = () => {
    return (
        <div>
            <button type="button" className="btn btn-primary d-blok w-100 rounded-pill" data-bs-toggle="modal" data-bs-target="#exampleModal" style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "13px" }}>Saya tertarik dan ingin nego </button>

            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Masukkan Harga Tawarmu</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan segera dihubungi penjual.</p>
                        <div class="modal-card">
                            <div class="row">
                                <div class="col-2">
                                    <img src={gambarJam1} class="modal-card-img" alt="..." />
                                </div>
                                <div class="col-6">
                                    <div class="modal-card-desc">
                                        <b>Jam Tangan Casio</b>
                                        <p>Rp. 250.000</p>
                                    </div>
                                </div>
                            </div>
                        </div> <br />
                        <label for="exampleInputEmail1" class="form-label"><b>Harga Tawar</b></label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Rp.'/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" className="btn btn-primary d-blok w-100 rounded-pill">Kirim </button>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ModalDetailProduct;
