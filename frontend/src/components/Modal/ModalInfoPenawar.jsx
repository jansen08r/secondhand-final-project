import React, {useState} from 'react';
import './Modal.css';
import gambarJam1 from '../../images/gambarJam1.png';
import imagePerson from '../../images/person.png';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify'; // toast 
import 'react-toastify/dist/ReactToastify.css';
import { BsWhatsapp } from 'react-icons/bs';
import { useEffect } from 'react';

const ModalInfoPenawar = (props) => {

    const navigate = useNavigate()

    const {namaPembeli, kotaPembeli, gambarProfil, gambarProduk, idTransaksi, ClickTransactions, idPembeli, nomor} = props;
    console.log(ClickTransactions, "ini ClickTransactions ============================");
    // console.log(produk, "ini produk di modalinfopenawar");
    // console.log(idTransaksi, "ini idTransaksi di modalinfopenawar");

    // let arr = produk.filter((item) => item.id_transaksi === idTransaksi.toString())
    // console.log(arr, "ini arr yang khusus setelah filter dengan id transaksi ");


    const [showToggle, setShowToggle] = useState(false);
    const [showToggle1, setShowToggle1] = useState(false);
    const [radio, setRadio] = useState('')
    console.log(radio,typeof(radio), "ini nilai radio");

    const toggle = () => {
        setShowToggle(true);
        
    }

    const toggle1 = () => {
        setShowToggle1(true);
    }

    const handleTerima = async(id) => {
        try{
            console.log("okeh terima ! barang dengan id transaksi", id);
            let token = localStorage.getItem('token');
            let {data} = await axios.put(`http://localhost:8000/api/transactions/${id}/update/seller-accept`,{},{
                headers: {
                    Authorization : token
                }
            })
            toast.success('Penawaran berhasil diterima!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                });
            // isi data yang dikirim harus ada, kasih ad objek kosong, kalau ga error karena gada body nya
            // let {data} = await axios.put(`http://localhost:8000/api/transactions/${id}/update/seller-accept`,{},{
            //     headers: {
            //         Authorization : token
            //     }
            // })
            // console.log(data, "ini hasil ketika barang di acc");
            // alert("Penawaran barang berhasil diterima !")
            
        }
        catch(err){
            console.log(err.message);
        }
    }

    const handleTolak = async(id) => {
        try{
            console.log("okeh ditolak ! barang dengan id transaksi", id);
            let token = localStorage.getItem('token');
            let {data} = await axios.put(`http://localhost:8000/api/transactions/${id}/update/seller-reject`,{},{
                headers: {
                    Authorization : token
                }
            })
            console.log(data, "ini hasil ketika barang berhasil ditolak");
            // alert("Penawaran barang berhasil ditolak !")
            toast.error('Penawaran dibatalkan!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            
        }
        catch(err){
            console.log(err.message);
        }
    }

    const handleWA = () => {
        window.open(`https://wa.me/${nomor}`)
    }

    const handleRadioButton = (e) => {
        setRadio(e)
    }

    const handleSubmit = async() => {
        if(+radio === 1 ){
            let dataKirim = {
                transaksi : "accepted"
            }
            let token = localStorage.getItem('token');
            let {data} = await axios.put(`http://localhost:8000/api/transactions/${idTransaksi}/complete`,dataKirim, {
                headers: {
                    Authorization : token
                }
            })
            toast.success('Berhasil terjual!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            }); 
        }
        if(+radio === 2){
            let dataKirim = {
                transaksi : "rejected"
            }
            let token = localStorage.getItem('token');
            let {data} = await axios.put(`http://localhost:8000/api/transactions/${idTransaksi}/complete`,dataKirim, {
                headers: {
                    Authorization : token
                }
            })
            toast.error('Transaksi dibatalkan!', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            }); 
        }
    }

    // useEffect(() => {
    //     const fetchMyApi = async() => {

    //     }
    //     fetchMyApi()
    // },[])

    return (
        <>
            <ToastContainer
                position="top-center"
                autoClose={5000}
                hideProgressBar
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        {
            ClickTransactions.length !== 0 ? 
            <>
            {
                ClickTransactions[0].data.transaksi.isBargainAccepted != true ?
                <>
                    {/* Accept Buton */}
                    <div class="modal-btn">
                        <button type="button" class="btn-confirm" data-bs-toggle="modal" data-bs-target="#modalAccept" onClick={() => handleTerima(idTransaksi)} style={{backgroundColor: "#7126B5", borderRadius: "16px", marginLeft: "12px", border: "none", fontSize: "14px", width: "80px"}}>Terima</button>
                    </div>

                    {/* Reject Button */}
                    <div class="modal-btn">
                        <button type="button" class="btn-status" onClick={() => handleTolak(idTransaksi)} style={{display: showToggle ? 'none' : 'block', outlineStyle: "solid", outlineColor: "#7126B5", outlineWidth: "1px", borderRadius: "16px", border: "none", fontSize: "14px", width: "80px"}}>Tolak</button> 
                    </div>
                </>
                :
                <>
                     {/* Status Button */}
                    <div class="modal-btn">
                        <button type="button" class="btn-status" data-bs-toggle="modal" data-bs-target="#modalStatus" style={{outlineStyle: "solid", outlineColor: "#7126B5", outlineWidth: "1px", borderRadius: "16px", border: "none", fontSize: "14px"}}>Status</button>
                        <button type="button" class="btn-confirm" data-bs-toggle="modal" data-bs-target="#modalAccept" style={{ backgroundColor: "#7126B5", borderRadius: "16px", marginLeft: "12px", border: "none", fontSize: "14px"}}>Hubungi <BsWhatsapp size={14} style={{margin: "4px 0 0 6px", float: "right"}}/></button>
                    </div>
                </>
            }
              
           

            {/* Produk Match Modals */}
            <div class="modal fade" id="modalAccept" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style={{borderRadius: "16px"}}>
                <div class="modal-header border-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h6 class="modal-title" id="exampleModalLabel" style={{ fontSize: "14px"}}>Yeay kamu berhasil mendapat harga yang sesuai</h6>
                    <p class="text-muted" style={{ fontSize: "14px"}}>Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya</p>
                    <div class="modal-card">
                        <h6 class="title-center" style={{ fontSize: "14px"}}>Product Match</h6>
                        <div class="row">
                            <div class="col-lg-2 col-3">
                                <img src={gambarProfil} class="modal-card-img" alt="..." />
                            </div>
                            <div class="col-8">
                                <div class="modal-card-desc">
                                    <h6 style={{ fontSize: "14px"}}>{namaPembeli}</h6>
                                    <p class="text-muted" style={{ fontSize: "14px"}}>{kotaPembeli}</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-3">
                                <img src={gambarProduk} class="modal-card-img" alt="..." />
                            </div>
                            <div class="col-8">
                                <div class="modal-card-desc">
                                    <div>{ClickTransactions[0].data.product.name}</div>
                                    <div><s>Rp. {ClickTransactions[0].data.product.price}</s></div>
                                    <div>Ditawar Rp. {ClickTransactions[0].data.transaksi.bargainPrice}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" onClick={() => handleWA()} data-bs-dismiss="modal" class="w-100"style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", marginLeft: "12px", border: "none"}}>Hubungi Via Whatsapp <BsWhatsapp size={20} style={{marginTop: "2px", float: "right"}}/> </button>
                </div>
                </div>
            </div>
            </div>

            {/* Status Modals */}
            <div class="modal fade" id="modalStatus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style={{borderRadius: "16px"}}>
                <div class="modal-header border-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <h6 class="modal-title" id="exampleModalLabel" style={{fontSize: "14px"}}>Perbarui status penjualan produkmu</h6>
                    <div class="form-check mt-4">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1" 
                    value="1"
                    onChange={(e) => handleRadioButton(e.target.value)}
                    style={{outlineColor: "#7126B5"}}
                    />
                        <label class="form-check-label" for="flexRadioDefault1">
                            <h6 style={{fontSize: "14px"}}>Berhasil terjual</h6> 
                            <p class="text-muted" style={{fontSize: "14px"}}>Kamu telah sepakat menjual produk ini kepada pembeli</p>
                        </label>
                    </div>
                    <div class="form-check">
                    <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" 
                    value="2"
                    onChange={(e) => handleRadioButton(e.target.value)}
                    />
                        <label class="form-check-label" for="flexRadioDefault2"> 
                            <h6 style={{fontSize: "14px"}}>Batalkan transaksi </h6> 
                            <p class="text-muted" style={{fontSize: "14px"}}>Kamu membatalkan transaksi produk ini dengan pembeli</p>
                        </label>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" className="w-100" style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", marginLeft: "12px", border: "none"}} onClick={() => handleSubmit()}>Kirim </button>
                </div>
                </div>
            </div>
            </div>
            </>
            :
            <></>
        }
            
        </>
    );
}

export default ModalInfoPenawar;
