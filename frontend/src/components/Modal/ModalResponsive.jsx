import React from 'react';
import { useState } from 'react';
import './Modal.css';

import gambarJam1 from '../../images/gambarJam1.png';

const ModalsResponsive = (props) => {

    const {name, price, image, onChange, onClick} = props;
 
    return (
        <div>
            <button type="button" className="d-blok w-100 rounded-pill" data-bs-toggle="modal" data-bs-target="#exampleModal1" style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", fontSize: "14px", border: "none"}}>Saya tertarik dan ingin nego </button>
            
            <div class="modal fade" id="exampleModal1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content" style={{borderRadius: "16px"}}>
                <div class="modal-header border-0">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" style={{marginTop: "-10px"}}>
                    <h6 class="modal-title" id="exampleModalLabel" style={{fontSize: "14px"}}>Masukkan Harga Tawarmu</h6>
                    <p style={{fontSize: "14px"}}>Harga tawaranmu akan diketahui penjual, jika penjual cocok kamu akan segera dihubungi penjual.</p>
                    <div class="modal-card">
                        <div class="row">
                            <div class="col-3">
                                <img src={image} class="modal-card-img" alt="..." />
                            </div>
                            <div class="col-8">
                                <div class="modal-card-desc">
                                    <b style={{fontSize: "14px"}}>{name}</b>
                                    <p style={{fontSize: "14px"}}>Rp. {price}</p>
                                </div>
                            </div>
                        </div>
                    </div> <br />
                    <label for="exampleInputEmail1" class="form-label" style={{fontSize: "14px"}}><b>Harga Tawar</b></label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder='Rp.' onChange={onChange} style={{fontSize: "14px"}}/>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" className="d-blok w-100 rounded-pill" onClick={onClick} style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", fontSize: "14px", border: "none"}}>Kirim </button>
                </div>
                </div>
            </div>
            </div>
        </div>
    );
}

export default ModalsResponsive;
