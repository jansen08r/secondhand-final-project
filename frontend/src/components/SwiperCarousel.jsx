import React, { useRef, useState } from 'react';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "./SwiperCarousel.css";
import { Pagination } from "swiper";
import bannerUtama from '../images/bannerUtama.png';
import hadiah from '../images/hadiah.png';


const SwiperCarousel = () => {
    return (
        <div className=''>
             <Swiper
                slidesPerView={"auto"}
                centeredSlides={true}
                // spaceBetween={0}
                grabCursor={true}
                modules={[Pagination]}
                className="mySwiper d-none d-md-block peh"
            >

                <SwiperSlide className='w-50 peh '>
                    <div className='d-flex justify-content-center' style={{}}>
                        {/* Slide 1 */}
                        <img src={bannerUtama} alt="" />
                    </div>
                </SwiperSlide>
                <SwiperSlide  className='w-50 peh'>
                    <div className='d-flex justify-content-center' style={{}}>
                        {/* Slide 1.1 */}
                        <img src={bannerUtama} alt=""  />
                    </div>
                </SwiperSlide>
                <SwiperSlide  className='w-50 '>
                    <div className='d-flex justify-content-center' style={{}}>
                        {/* Slide 1.2 */}
                        <img src={bannerUtama} alt=""  />
                    </div>
                </SwiperSlide>
                <SwiperSlide  className='w-50 '>
                    <div className='d-flex justify-content-center' style={{}}>
                        {/* Slide 1.3 */}
                        <img src={bannerUtama} alt=""  />
                    </div>
                </SwiperSlide>
                <SwiperSlide  className='w-50 d-flex justify-content-center'>
                    <div className='d-flex justify-content-center' style={{}}>
                        {/* Slide 1.4 */}
                        <img src={bannerUtama} alt=""  />
                    </div>
                </SwiperSlide>
                <SwiperSlide  className='w-50 d-flex justify-content-center'>
                    <div className='d-flex justify-content-center' style={{}}>
                        {/* Slide 1.5 */}
                        <img src={bannerUtama} alt=""  />
                    </div>
                </SwiperSlide>
            
            </Swiper>
            
            <div className="container-fluid  d-block d-md-none pb-5 pt-3" style={{backgroundImage : "linear-gradient(rgba(255, 233, 201,1), rgba(0,0,0,0)"}}>
                <div className="row">
                    <div className="col-8">
                        <h1 className='fw-bold pb-1'>Bulan Ramadhan Banyak diskon!</h1>
                        <div>Diskon Hingga</div>
                        <h2 className='text-danger'>60%</h2>
                    </div>
                    <div className="col-4 px-0 pt-3">
                        <img src={hadiah} alt="" className='w-100 pe-3'/>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default SwiperCarousel;
