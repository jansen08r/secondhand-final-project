import React, {useState, useEffect} from "react";
import { useDispatch, useSelector } from "react-redux";
import { seeDetailProduct } from "../store/actions/ProductDetailActions";
import { useNavigate } from 'react-router-dom';
import axios from "axios";

const CardBulet = (props) => {
  
  const { id, title, description, fileUrl, price, id_category, gambar } = props;
  console.log(gambar, "ini gambar di  ");

  const navigate = useNavigate();
  const dispatch = useDispatch();
  
  const data = useSelector((state) => state.productDetail); 
  // console.log(data, "ini data dari useSelector");

  const [kategori, setKategori] = useState('')

  const handleSeeDetailProduct = (id) => {
    
    // console.log(id, "ini index kartu ketika diclick");
    // dispatch(seeDetailProduct(id));
    navigate(`/detailproduk/${id}`)
  };

  useEffect(() => {
    const fetchMyApi = async() => {
      // const arrKosong1 = []

      let res = await axios.get(`http://localhost:8000/api/categories/${id_category}`,{
        headers: {
            Authorization: `${localStorage.getItem('token')}`
        }
      })
      setKategori(res.data.data.category)
      // arrKosong1.push(res.data.data.category)
    }
  
    fetchMyApi()
  }, [id_category]) // dikasih dependency, kalau ga nanti ke render duluan sebelum id_category rede
  

  return (
    <>
    {
      gambar !== undefined && gambar.length != 0 ? // harus dicek dulu undefined atau ga kalau ga nanti ke render duluan alhasil malah jadi error T_T 
      <>
      <div class="col-lg-2 col-6 d-flex justify-content-center" key={id}>
            <div class="card rounded-3 shadow-sm h-100 " 
              style={{ width: "500px" }}>
                  <>
                    {
                      gambar.length != 0 ?
                      <>
                        <img
                          src={gambar[0].imageURL} 
                          className='card-img-top p-3 rounded-3 img-fluid'
                          alt="..."
                          style={{
                            width: "100%",
                            height: "200px",
                            objectFit: "cover"
                          }}
                        />
                      </>
                      :
                      <></>
                    }
                    
                  </>
                  
                <div class="card-body" style={{marginTop: "-20px", cursor: "pointer"}} onClick={() => handleSeeDetailProduct(id)}>
                <p class="card-title" style={{ fontSize: "16px"}}>{title}</p>
                {/* <p class="card-text text-muted" style={{ fontSize: "12px"}}>{description}</p> */}
                {
                  kategori != '' ? 
                  <>
                    <p class="card-title text-muted" style={{ fontSize: "14px"}}>{kategori}</p>
                  </>
                  :
                  <></>
                }
                {/* <p class="card-title fs-5">{id_category}</p> */}
                <p class="card-title" style={{ fontSize: "16px"}}>Rp. {price}</p>
                {/* <button className='d-flex justify-content-center w-100' onClick={() => handleSeeDetailProduct(id)} style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "13px", fontSize: "12px", border: "none" }}>Detail Produk</button> */}
              </div>
            </div>
          </div>
      </>
      :
      <></>
    }
      
    </>
  );
};


export default CardBulet;
