/* eslint-disable jsx-a11y/anchor-is-valid */
import React, {useEffect, useState} from 'react';
import { Link, useNavigate } from 'react-router-dom';
import './Navbar.css'
import axios from 'axios';
import moment from 'moment'

import { IoIosLogIn } from 'react-icons/io';
import { IoIosNotificationsOutline } from 'react-icons/io';
import { IoIosList } from 'react-icons/io';
import { FiUser } from 'react-icons/fi';

const HeadNavbar = (props) => {

    const navigate = useNavigate();
    const { handleSearch, login, setLogin, title} = props;

    const [transactions, setTransactions] = useState([])
    const [barang, setBarang] = useState([])
    const [idUser, setIdUser] = useState('')
    const [notif, setNotif] = useState([])

    const handleLogout = () => {
        localStorage.removeItem("token")
        setLogin(false)
        navigate('/')
    }

    useEffect(() => {
        async function fetchMyApi(){
            let token = localStorage.getItem('token');
            let {data} = await axios.get(`http://localhost:8000/api/transactions/`,{
                headers: {
                    Authorization: token
                }
            })

            setTransactions(...transactions, data.data)

            let res = await axios.get(`http://localhost:8000/api/notifications/`,{
                headers: {
                    Authorization: token
                }
            })

            setNotif(...notif, res.data.data)
        }
        fetchMyApi()
    },[])
    
    useEffect(() => {
        let token = localStorage.getItem('token');
        const getWithForOf = async() => {
           

            const arrKosong = []
            for (const transaction of transactions) {
                let dataUser = await axios.get(`http://localhost:8000/api/products/${transaction.id_product}/detail/`,{
                        headers: {
                            Authorization: token
                        }
                    })
                arrKosong.push(dataUser.data);
            }
            setBarang(...barang, arrKosong)
         }
         getWithForOf();
    },[transactions])

    useEffect(() => {
        const fetchMyApi = async() => {
            let token = localStorage.getItem('token');
            let {data} = await axios.get('http://localhost:8000/api/users/profile',{
                headers:{
                    Authorization: token
                }
            })
            setIdUser(data.data.id)
        }
        fetchMyApi()
    }, [])

    return (
        <>
            <nav class="navbar navbar-expand-md py-0  navbar-light">
                <div class="container-fluid">
                    <a class="navbar-brand d-none d-md-block" onClick={() => navigate('/')} style={{ cursor: "pointer" }}>SecondHand</a>
                    <div className="d-flex w-100 justify-content-between">
                            {/* gabisa w-md-50, jadi trick nya dibuat dalam satu col-md-6, dalemnya w-100, kekurung sama col-6 gt maksudnya, kalo ms-auto supaya ke kanan */}
                                <div class="d-flex w-100 justify-content-between py-4 py-md-3" >
                                    <button class="navbar-toggler bg-light border-radius-13px p-2 me-3" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample" >
                                        <span class="navbar-toggler-icon"></span>
                                    </button>
                                    <div className="col-md-6 col">
                                        <h2 class="mt-2">{title}</h2>
                                    </div>
                                    {login ? 
                                    <>
                                        <div id='btn-group' className='btn-group'>
                                            <button class="btn button" type="button" onClick={() => navigate('/product-sell')}>
                                                <IoIosList size={28}/>
                                            </button>
                                            <div class="dropdown">
                                                <button className='btn button dropdown-toggle' type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                    <IoIosNotificationsOutline size={28}/>
                                                </button>
                                                {
                                                    transactions.length !== 0  && barang.length !== 0 && notif.length !== 0 ? 
                                                    <>
                                                        <ul className='dropdown-menu dropdown-menu-end' aria-labelledby="dropdownMenuButton1" style={{ width: "400px", borderRadius: "16px"}}>
                                                            {transactions.map((item, index) => {
                                                                return  <li>
                                                                            <div class="dropdown-item">
                                                                                {idUser !== '' ? 
                                                                                <>
                                                                                    {item.id_seller === idUser ? 
                                                                                        <div>
                                                                                            Barang yang Ditawar oleh Orang Lain
                                                                                        </div>
                                                                                        :
                                                                                        <div>
                                                                                            Menawar Barang Milik Orang Lain
                                                                                        </div>
                                                                                    }
                                                                                </>
                                                                                :
                                                                                <></>
                                                                                }
                                                                                {
                                                                                    barang[index].images.length !== 0 ?
                                                                                    <img src={barang[index].images[0].imageURL} alt="" style={{width: "100px", height: "100px"}}/>
                                                                                    :
                                                                                    <></>
                                                                                }
                                                                                
                                                                                <div>{barang[index].data[0].name}</div>
                                                                                {/* <div>{item.id_product}</div> */}
                                                                                {
                                                                                    notif[index].status === "Penawaran Produk Diterima!" ?
                                                                                    <>
                                                                                        <s><div>Rp. {barang[index].data[0].price}</div></s>
                                                                                        <div>Berhasil Ditawar Rp. {notif[index].bargainPrice}</div>
                                                                                        {
                                                                                            item.id_seller === idUser ? 
                                                                                            
                                                                                            <div className='pb-4'>Segera Hubungi Pembeli via whatsapp</div>
                                                                                            :
                                                                                            <div className='pb-4'>{notif[index].message}</div>
                                                                                        }
                                                                                    </>
                                                                                    :
                                                                                    <>
                                                                                        <div>Rp. {barang[index].data[0].price}</div>
                                                                                        <div className='pb-4'>Ditawar Rp. {item.bargainPrice}</div>
                                                                                    </>
                                                                                }
                                                                                {/* <div>{moment(item.createdAt).format("dddd, MMMM Do YYYY, h:mm:ss a")}</div> */}
                                                                                {/* <div>{moment(moment().format().split('T')[0].replaceAll('-', ''), "YYYYMMDD").fromNow()}</div> */}
                                                                            </div>
                                                                        </li>
                                                            })}
                                                        </ul>
                                                    </>
                                                    :
                                                    <>
                                                    </>
                                                }
                                                {/* <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                    <li><a class="dropdown-item" href="#">Action</a></li>
                                                    <li><a class="dropdown-item" href="#">Another action</a></li>
                                                    <li><a class="dropdown-item" href="#">Something else here</a></li>
                                                </ul> */}
                                            </div>
                                            <button class="btn button" type="button" onClick={() => navigate('/informasiakun')} style={{ marginRight: "20px" }}>
                                                <FiUser size={28}/>
                                            </button>
                                        </div>
                                    </>
                                        
                                        :
                                        <button class="btn btnClass d-none d-md-block " style={{ backgroundColor: "#7126B5", color: "white" }} type="button" onClick={() => navigate('/login')}> <IoIosLogIn /> Masuk</button>
                                    }
                                </div>
                                <div class="offcanvas offcanvas-start  d-md-none w-50" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
                                    <div class="offcanvas-header">
                                        <h5 id="offcanvasTopLabel" onClick={() => navigate('/')}>SecondHand</h5>
                                        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                    </div>
                                    <div class="offcanvas-body d-md-none">
                                        {login ?
                                        <>
                                            <div className='pb-3'>Notifikasi</div>
                                            <div className='pb-3'>Daftar Jual</div>
                                            <div onClick={() => navigate('/infoprofil')} className='pb-3'>Akun Saya</div>
                                            <div onClick={() => handleLogout()} className='pb-3'>
                                                Logout
                                            </div> 
                                        </>
                                        :
                                        <div onClick={() => navigate('/login')}>
                                            Login 
                                        </div>
                                        }
                                    </div>
                                </div>
                    </div>

                </div>
            </nav>
        </>
    );
}

export default HeadNavbar;
