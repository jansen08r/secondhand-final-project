import PropsTypes from "prop-types";

function Search(props) {
  const { keyword, handleSearch } = props;
  console.log(keyword);

  return (
    // kalo pake CSS Module gini, kalo vanilla CSS kayak yg dibawahnya
    // perbedaan pake yg module itu cuma bisa buat 1 style aja, mksdnya gabisa styles.container dan styles.input berbarengan dalam satu komponen
    // beda sama vanilla CSS mau taruh sebanyak apapun, ditambah Bootstrap sebanyak apapun bisa
    <div > 
      <div >
        <input
          type="text"
          name="input"
          placeholder="Search by title..."
          className="form-control w-100"
          id="search-input"
          value={keyword}
          // onChange={(e) => handleSearch(e.target.value)} // sama aja gini boleh yang dibawah juga boleh
          onChange={(e) => props.handleSearch(e.target.value)}
        />
      </div>
    </div>
  );
}