import React,{useEffect} from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

import { FaPlus } from 'react-icons/fa';

const TombolJualHomepage = () => {

    const navigate = useNavigate()

   

    return (
        <>
            <button className="btn position-fixed bottom-0 start-50 translate-middle-x mb-4 w-10 rounded-pill"
            onClick={() => navigate('/infoproduk')} style={{ backgroundColor: "#7126B5", color: "white", borderRadius: "16px", boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.15)", width: "115px", fontSize: "16px" }}
            >
                 <FaPlus size={14} style={{marginBottom: "4px"}}/> Jual
            </button>
        </>
    );
}

export default TombolJualHomepage;
